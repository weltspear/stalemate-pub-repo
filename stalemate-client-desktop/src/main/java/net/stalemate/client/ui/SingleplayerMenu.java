/*
 * Stalemate Game
 * Copyright (C) 2022 Weltspear
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package net.stalemate.client.ui;

import net.stalemate.client.AssetManager;
import net.stalemate.client.meta.MetaLoader;
import net.stgl.MainFrame;
import net.stgl.STGLGraphics;
import net.stgl.Utils;
import net.stgl.color.STGLColors;
import net.stgl.event.WindowResizeListener;
import net.stgl.font.FontManager;
import net.stgl.font.STGLFont;
import net.stgl.image.STGLImage;
import net.stgl.texture.Texture;
import net.stgl.ui.ComponentManager;
import net.stgl.ui.STGLButton;
import net.stgl.ui.STGLMultiList;
import org.joml.Vector4f;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.locks.ReentrantLock;

import static net.stalemate.client.Utils.setXY;

public class SingleplayerMenu {
    private final MainFrame frame;
    private STGLMultiList multiList;
    private final int title_width_img;
    private final int title_height_img;
    private final Texture background;
    private final Texture title;

    private final Texture panel;

    private final ReentrantLock lock = new ReentrantLock();

    private int width;
    private int height;
    private final ComponentManager componentManager;

    private STGLButton play;
    private STGLButton close;

    private int status = 0;

    public int getStatus() {
        try {
            lock.lock();
            return status;
        }
        finally {
            lock.unlock();
        }
    }

    private final STGLFont monogram;
    private final STGLFont monogram20;

    public void dispose(){
        background.dispose();
        title.dispose();
        monogram.dispose();
    }

    private final WindowResizeListener resizeListener = (nwidth, nheight) -> {
        width = nwidth;
        height = nheight;
        int x_l = (width-400-50)/2;
        int y_l = (height-300)/2;
        setXY(multiList,x_l, y_l);
        setXY(play, multiList.getX()+375+25-10,multiList.getY()+275);
        setXY(close,multiList.getX()+375+25-150-25-25-5-5,multiList.getY()+275);
    };

    public SingleplayerMenu(MainFrame frame, ComponentManager componentManager, FontManager fontManager){
        this.frame = frame;
        frame.addWindowResizeListener(resizeListener);
        width = frame.getWidth();
        height = frame.getHeight();
        this.componentManager = componentManager;

        panel =new Texture( AssetManager.loadImage("assets/panel.png"));

        monogram = fontManager.createFont("font/monogram-extended.ttf", 12, false, false);
        monogram20 = fontManager.createFont("font/monogram-extended.ttf", 20, false, false);

        background = new Texture(AssetManager.loadImage("assets/background.png"));
        BufferedImage title_img = AssetManager.loadImage("assets/stalemate.png");
        title = new Texture(title_img.getWidth(), title_img.getHeight()+2, new Texture.TextureOptions().setMagFiltering(Texture.Filtering.NEAREST).setMinFiltering(Texture.Filtering.NEAREST));
        title.writeImage(0,0, title_img);
        title_width_img = title_img.getWidth();
        title_height_img = title_img.getHeight();

        int x_l = (width-400-50)/2;
        int y_l = (height-300)/2;


        multiList = new STGLMultiList(
                new STGLMultiList.DefaultColumn("Map", List.of()),
                x_l, y_l, 180, 300, monogram.getMaxHeight()+4, 30);
        multiList.setForeground(Utils.colorToVec(new Color(198, 130, 77)));
        multiList.setBackground(Utils.colorToVec(new Color(60, 38, 22)));
        multiList.setSelectedBackground(Utils.colorToVec(new Color(51, 39, 31)));
        multiList.setSelectedForeground(Utils.colorToVec(Color.BLACK));
        multiList.setFont(monogram);
        componentManager.addComponent(multiList);


        play = new STGLButton(150, 25, multiList.getX()+375+25-10,multiList.getY()+275, "Play");
        StalemateStyle.makeButton(play);
        play.setFont(monogram);
        play.setActionListener(()-> status = 1);
        componentManager.addComponent(play);

        close = new STGLButton(150, 25, multiList.getX()+375+25-150-25-25-5-5,multiList.getY()+275, "Close");
        StalemateStyle.makeButton(close);
        close.setFont(monogram);
        close.setActionListener(()-> status = 2);
        componentManager.addComponent(close);
    }

    private static int lastIndex = -1;
    private STGLImage storedPreview = null;

    public void paint(STGLGraphics g) {
        g.drawTextureScaled(0, 0, background, new Vector4f(1, 1, 1, 1), width, height);
        g.fillRect(multiList.getX()+375+25-220, multiList.getY()+75-25-50, 150+220, 225+25+50, Utils.colorToVec(new Color(60, 38, 22)));
        g.drawRect(multiList.getX()+375+25-220, multiList.getY()+75-25-50, 150+220, 225+25+50, Utils.colorToVec(new Color(198, 130, 77)));
        componentManager.paint();
        if (maps != null) {
            g.fillRect(multiList.getX()+375+25-220+255, multiList.getY()+75-25-50+10,
                    96, 96,
                    new Vector4f(0,0,0,1));
            if (lastIndex != multiList.getSelectedIdx()) {
                lastIndex = multiList.getSelectedIdx();
                if (storedPreview != null)
                    storedPreview.drop();
                storedPreview = new STGLImage(Objects.requireNonNull(MetaLoader.loadPreview("maps/"
                        + maps.get(multiList.getSelectedIdx()))), new Texture.TextureOptions().setMinFiltering(Texture.Filtering.NEAREST).setMagFiltering(Texture.Filtering.NEAREST));
            }

            int st_x = multiList.getX()+375+25-220+255;
            int st_y = multiList.getY()+75-25-50+1+10-1;

            float sc_w = 1;
            float sc_h = 1;

            if (storedPreview != null) {
                if (storedPreview.getWidth() > 96) {
                    sc_w = 1f / (storedPreview.getWidth() / 96f);
                }
                if (storedPreview.getHeight() > 96) {
                    sc_h = 1f / (storedPreview.getHeight() / 96f);
                    if (sc_h < sc_w) {
                        sc_w = sc_h;
                    } else {
                        sc_h = sc_w;
                    }
                }

                if (storedPreview.getWidth() < 96) {
                    sc_w = 96f / storedPreview.getWidth();
                }
                if (storedPreview.getWidth() < 96) {
                    sc_h = 96f / storedPreview.getHeight();
                    if (sc_h < sc_w) {
                        sc_w = sc_h;
                    } else {
                        sc_h = sc_w;
                    }
                }

                int scaled_width = (int) (storedPreview.getWidth() * sc_w);
                int scaled_height = (int) (storedPreview.getHeight() * sc_h);

                g.drawImageScaled(st_x + (96 - scaled_width) / 2,
                        st_y + (96 - scaled_height) / 2, storedPreview, scaled_width, scaled_height);
            }

            try{
                MetaLoader.Metadata meta = MetaLoader.loadMetadata("maps/"
                        + maps.get(multiList.getSelectedIdx()));
                if (meta != null)
                {
                    int title_width = st_x-close.getX();
                    g.renderText(monogram20, close.getX()
                            +(title_width-(monogram20.getFontMetrics().getStringWidth(meta.name())))/2, st_y+10, meta.name(), STGLColors.WHITE);
                    g.renderText(monogram, close.getX(), st_y+32, "Author: " + Objects.requireNonNullElse(meta.author(), "<unknown>"), STGLColors.WHITE);
                    g.renderText(monogram, close.getX(), st_y+46, "Gamemode: " + meta.mode(), STGLColors.WHITE);
                    g.renderFormattedText(monogram, monogram, monogram, monogram, "Description: " + Objects.requireNonNullElse(meta.description(), "<no description>"), close.getX(), st_y+60);
                }
            } catch (IOException ignored) {
                // everything is fine
            }
        }

        if (ertext != null)
            g.renderText(monogram, multiList.getX(), multiList.getY()+300+monogram.getMaxHeight(), ertext, new Vector4f(1,0,0,1));

        if (title != null)
            g.drawTextureRegionScaled((width - 364) / 2, (int) (0.43f * height) - 194, title.getSubTexture(0, 0, title_width_img, title_height_img), new Vector4f(1, 1, 1, 1), 364, 64);
    }

    private List<String> maps = null;

    public void setMaps(List<String> maps){
        try {
            lock.lock();
            this.maps = maps;

            multiList.setColumns(
                    List.of(new STGLMultiList.DefaultColumn("Map", maps, 150))
            );
        }finally {
            lock.unlock();
        }
    }

    public void setStatus(int i) {
        try {
            lock.lock();
            status = i;
        }
        finally {
            lock.unlock();
        }
    }

    public int getIndex(){
        try {
            lock.lock();
            return multiList.getSelectedIdx();
        }
        finally {
            lock.unlock();
        }
    }

    private String ertext = null;

    public void setText(String s) {
        lock.lock();
        ertext = s;
        lock.unlock();
    }

    public void clFrame() {
        componentManager.removeComponent(play);
        componentManager.removeComponent(close);
        componentManager.removeComponent(multiList);
        frame.removeWindowResizeListener(resizeListener);
    }
}
