/*
 * Stalemate Game
 * Copyright (C) 2022 Weltspear
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package net.stalemate.server.core.buttons;

import net.stalemate.server.core.AirUnit;
import net.stalemate.server.core.Entity;
import net.stalemate.server.core.Unit;
import net.stalemate.server.core.buff.Camouflage;
import net.stalemate.server.core.buttons.util.Unflippable;
import net.stalemate.server.core.controller.Game;

import java.util.ArrayList;

import static net.stalemate.server.core.Unit.Layer.AIR;
import static net.stalemate.server.core.Unit.Layer.GROUND;

public class MoveButton implements Unit.ISelectorButton {
    private final int move_range;
    private final Unit.Layer l;

    private boolean dontConsumeSupply = false;

    public MoveButton(int move_range){
        this.move_range = move_range;
        l = GROUND;
    }

    public MoveButton(int move_range, Unit.Layer layer){
        this.move_range = move_range;
        l = layer;
    }

    public MoveButton setNoSupplyConsumption(){
        dontConsumeSupply = true;
        return this;
    }

    @Override
    public String bind() {
        return "M";
    }

    @Override
    public String texture() {
        return "assets/ui/buttons/move_button.png";
    }

    @Override
    public String identifier() {
        return "button_move";
    }

    @Override
    public void action(int x, int y, Unit unit, Game gameController) {
        if (!unit.hasTurnEnded() && (dontConsumeSupply||unit.unitStats().getSupply() - 1 > 0) && unit.getMoveAmount() > 0)
            if ((x != unit.getX()) || (y != unit.getY())) {
                if (gameController.getMapObject(x, y).isPassable()) {
                    boolean isPassable = true;
                    Entity notPasEntity = null;
                    for (Entity entity : gameController.getEntities(x, y)) {
                        if (!entity.isPassable()
                                && !(!(entity instanceof AirUnit) && unit instanceof AirUnit)
                                && !(entity instanceof AirUnit && !(unit instanceof AirUnit))) {
                            isPassable = false;
                            notPasEntity = entity;
                            break;
                        }
                    }
                    if (isPassable) {

                        if ((unit.getX() - x) > 0 && !(unit instanceof Unflippable)){
                            unit.flip();
                        }
                        if ((unit.getX() - x) < 0 && !(unit instanceof Unflippable)){
                            unit.unflip();
                        }

                        unit.setX(x);
                        unit.setY(y);
                        if (!dontConsumeSupply)
                            unit.consumeSupply(1);
                        unit.setMoveAmount(unit.getMoveAmount()-1);
                        unit.move();
                        unit.protectUnitWith(null);
                        if (!unit.getBuffs().isEmpty()) {
                            ArrayList<Unit.Buff> buffsToBeRemoved = new ArrayList<>();
                            for (Unit.Buff b : unit.getBuffs()) {
                                if (b instanceof Camouflage c) {
                                    buffsToBeRemoved.add(c);
                                    unit.mechanicMakeVisible(10);
                                }
                            }
                            unit.getBuffs().removeAll(buffsToBeRemoved);
                        }

                        if (unit.getMoveAmount() == 0){
                            unit.endTurn();
                        }
                    }
                    else{
                        if (notPasEntity instanceof Unit enemy){
                            if (enemy.isInvisible())
                                if (enemy.getTeam() != unit.getTeam()){
                                    AttackButton attackButton = new AttackButton(move_range, unit instanceof AirUnit ? AIR: GROUND);
                                    attackButton.action(unit, enemy, gameController);
                                    enemy.mechanicMakeVisible(5);
                                }
                        }
                    }
                }
            }
    }

    @Override
    public int selector_range() {
        return move_range;
    }

    @Override
    public String selector_texture() {
        return "assets/ui/selectors/ui_move.png";
    }

    @Override
    public Unit.Layer getLayer() {
        return l;
    }
}
