/*
 * Stalemate Game
 * Copyright (C) 2022 Weltspear
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package net.stalemate.client.meta;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.json.JsonMapper;
import com.fasterxml.jackson.databind.jsontype.BasicPolymorphicTypeValidator;
import com.fasterxml.jackson.databind.jsontype.PolymorphicTypeValidator;
import net.stalemate.utils.error.Expect;
import net.stalemate.client.AssetManager;
import net.stalemate.client.ClientMapLoader;
import net.stalemate.client.TileUtils;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

public class MetaLoader {

    @JsonIgnoreProperties(value = {
            "mpobjects",
            "entity_data"
    })
    private static class StMap{
        public StMap(){

        }
        public Metadata map_data;
    }

    @JsonIgnoreProperties(value = {
            "tileset_data",
            "team_data",
            "aparams"
    })
    public record Metadata(String author,
                           String description,
                           String name,
                           String mode){}

    private static HashMap<String, BufferedImage> path2preview = new HashMap<>();

    public static Metadata loadMetadata(String map_path) throws IOException {
        PolymorphicTypeValidator ptv = BasicPolymorphicTypeValidator.builder()
                .build();
        ObjectMapper objectMapper = JsonMapper.builder().polymorphicTypeValidator(ptv).build();

        StMap stMap = objectMapper.readValue(new File(map_path), StMap.class);
        return stMap.map_data;
    }

    public static BufferedImage loadPreview(String map){
        if (path2preview.containsKey(map))
        {
            return path2preview.get(map);
        }
        else {
            ClientMapLoader clMapLoader = new ClientMapLoader();
            Expect<?, ?> expect = clMapLoader.load(map);
            if (expect.isNone())
                return null;

            BufferedImage mpreview = new BufferedImage(clMapLoader.getWidth(),
                    clMapLoader.getHeight(), BufferedImage.TYPE_INT_ARGB);

            int _y = 0;
            for (ArrayList<String> row : clMapLoader.getEntireMap()) {
                int _x = 0;
                for (String tile : row) {
                    BufferedImage _tile = AssetManager.loadImage(tile);
                    mpreview.setRGB(_x, _y, TileUtils.calcAvgRGB(_tile));
                    _x++;
                }
                _y++;
            }

            path2preview.put(map, mpreview);
            return mpreview;
        }
    }

}
