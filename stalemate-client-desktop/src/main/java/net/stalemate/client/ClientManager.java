/*
 * Stalemate Game
 * Copyright (C) 2022 Weltspear
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package net.stalemate.client;

import net.stalemate.utils.error.Expect;
import net.stalemate.client.config.Grass32ConfigClient;
import net.stalemate.client.ui.*;
import net.stalemate.client.singleplayer.SingleplayerGame;
import net.stgl.MainFrame;
import net.stgl.font.FontManager;
import net.stgl.ui.ComponentManager;
import net.stgl.vidmode.Monitors;
import net.stgl.vidmode.VideoMode;

import java.io.File;
import java.util.ArrayList;

public class ClientManager {

    private MainFrame frame;
    private ComponentManager componentManager;

    public static class ClientPacketRunnable implements Runnable{
        public Client client;
        public Expect<Integer, ?> result = null;

        public ClientPacketRunnable(String ip){
            client = new Client(ip);
        }

        @Override
        public void run() {
            result = client.start_client();
        }
    }

    public ClientManager(){

    }

    public Expect<Integer, ?> startClientMenu(FontManager fontManager){
        ClientMenu clientMenu = new ClientMenu(frame, componentManager, fontManager);

        while (!frame.shouldClose()){
            long t1 = System.currentTimeMillis();
            frame.getGraphics().clear();
            clientMenu.paint(frame.getGraphics());

            frame.update();
            long t2 = System.currentTimeMillis();
            if (t2-t1 < 6) {
                try {
                    Thread.sleep(6-(t2-t1));
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
            }

            stage2:
            {
                if (clientMenu.getData() != null) {
                    clientMenu.cleanup();
                    ClientPacketRunnable runnable = new ClientPacketRunnable(clientMenu.getData());
                    Thread clientPacketThread = new Thread(runnable);
                    clientPacketThread.start();

                    while (!frame.shouldClose() && !runnable.client.hasConnectionStarted() && runnable.result == null) {
                        t1 = System.currentTimeMillis();
                        frame.getGraphics().clear();
                        clientMenu.paint(frame.getGraphics());

                        frame.update();
                        t2 = System.currentTimeMillis();
                        if (t2 - t1 < 6) {
                            try {
                                Thread.sleep(6 - (t2 - t1));
                            } catch (InterruptedException e) {
                                throw new RuntimeException(e);
                            }
                        }
                    }

                    if (!runnable.client.hasConnectionStarted() && runnable.result == null){
                        break stage2;
                    }

                    LobbySelectMenu lbSelectMenu = new LobbySelectMenu(frame, componentManager, fontManager);
                    runnable.client.setLobbySelectMenu(lbSelectMenu);

                    // select lobby

                    while (!frame.shouldClose()) {
                        long _t1 = System.currentTimeMillis();
                        frame.getGraphics().clear();
                        lbSelectMenu.paint(frame.getGraphics());

                        if (lbSelectMenu.getStatus() == 3) {
                            lbSelectMenu.clFrame();
                            return new Expect<>(1);
                        }
                        if (lbSelectMenu.getStatus() == 1 && runnable.client.isConnectedToLobby()) {
                            lbSelectMenu.clFrame();
                            break;
                        }
                        if (runnable.result != null) {
                            lbSelectMenu.clFrame();
                            return runnable.result;
                        }

                        frame.update();
                        long _t2 = System.currentTimeMillis();
                        if (_t2 - _t1 < 6) {
                            try {
                                Thread.sleep(6 - (_t2 - _t1));
                            } catch (InterruptedException e) {
                                throw new RuntimeException(e);
                            }
                        }
                    }
                    if (frame.shouldClose()) {
                        lbSelectMenu.clFrame();
                        return new Expect<>(1);
                    }

                    // wait

                    lbSelectMenu.dispose();
                    LobbyWaitMenu lbWaitMenu = new LobbyWaitMenu(frame, componentManager, "");
                    runnable.client.setLobbyWaitMenu(lbWaitMenu);
                    runnable.client.getwLobbyWaitMenu().release();
                    while (!frame.shouldClose()) {
                        long _t1 = System.currentTimeMillis();
                        frame.getGraphics().clear();
                        lbWaitMenu.paint(frame.getGraphics());
                        frame.update();

                        if (lbWaitMenu.getStatus() == 1) {
                            lbWaitMenu.clFrame();
                            return new Expect<>(1);
                        }

                        if (lbWaitMenu.getStatus() == 3) {
                            lbWaitMenu.clFrame();
                            break;
                        }

                        if (runnable.result != null) {
                            lbWaitMenu.clFrame();
                            return runnable.result;
                        }

                        long _t2 = System.currentTimeMillis();
                        if (_t2 - _t1 < 6) {
                            try {
                                Thread.sleep(6 - (_t2 - _t1));
                            } catch (InterruptedException e) {
                                throw new RuntimeException(e);
                            }
                        }
                    }

                    // ok start game
                    lbWaitMenu.dispose();

                    InGameUI inGameUI = new InGameUI(frame);
                    runnable.client.setInGameUI(inGameUI);
                    runnable.client.getwInGameUI().release();

                    while (!runnable.client.isFirstPacketDone()) {
                        Thread.onSpinWait();
                    }

                    while (!frame.shouldClose()) {
                        t1 = System.currentTimeMillis();

                        if (runnable.result != null) {
                            return runnable.result;
                        }

                        if (inGameUI.getStatus() == 1) {
                            break;
                        }

                        frame.getGraphics().clear();

                        inGameUI.inGameUIUpdate();

                        inGameUI.unsafeLock.lock();
                        Object[] ef = runnable.client.getCgame().buildView(inGameUI.cam_x, inGameUI.cam_y, inGameUI.scale, (int) Math.ceil((inGameUI.tr_width - 832f) / 64f), (int) Math.ceil((inGameUI.tr_height - 576f) / 64f));

                        inGameUI.getClDataManager().updateData(runnable.client.getCgame().getChat(),
                                (ClientGame.ClientEntity[][]) ef[0], (boolean[][]) ef[1], (boolean[][]) ef[2], runnable.client.getCgame().getSelectedUnit(), runnable.client.getCgame().getSteel(), runnable.client.getCgame().getAluminium(),
                                runnable.client.getCgame().isIsItYourTurn(), runnable.client.getCgame().getClMapLoader(), runnable.client.getCgame().drawMinimap(inGameUI.cam_x, inGameUI.cam_y), runnable.client.getCgame().getTeamDoingTurnColor(), runnable.client.getCgame().getTeamDoingTurnNick(),
                                runnable.client.getCgame().getGamemodeProperties(), runnable.client.getCgame().getManpower());
                        inGameUI.unsafeLock.unlock();

                        inGameUI.paint(frame.getGraphics());
                        frame.update();
                        t2 = System.currentTimeMillis();
                        if (t2 - t1 < 6) {
                            try {
                                Thread.sleep(6 - (t2 - t1));
                            } catch (InterruptedException e) {
                                throw new RuntimeException(e);
                            }
                        }
                    }
                    inGameUI.dispose();

                    return new Expect<>(1);
                }
            }
        }
        return new Expect<>(1);
    }

    public Expect<Integer, ?> startSingleplayerMenu(int width, int height, FontManager fontManager){
        SingleplayerMenu singleplayerMenu = new SingleplayerMenu(frame, componentManager, fontManager);

        File[] fs = new File("maps").listFiles();
        ArrayList<String> s = new ArrayList<>();
        for (File f: fs){
            s.add(f.getName());
        }
        singleplayerMenu.setMaps(s);

        while (!frame.shouldClose()) {
            long t1 = System.currentTimeMillis();
            frame.getGraphics().clear();
            singleplayerMenu.paint(frame.getGraphics());

            if (singleplayerMenu.getStatus() == 1){

                SingleplayerGame game = new SingleplayerGame("maps/"+s.get(singleplayerMenu.getIndex()), width, height, frame);
                Expect<Integer, ?> result = game.startGame();

                singleplayerMenu.clFrame();
                if (result.isNone())
                    return result;
                return new Expect<>(1);
            }

            if (singleplayerMenu.getStatus() == 2){
                break;
            }

            frame.update();
            long t2 = System.currentTimeMillis();
            if (t2 - t1 < 6) {
                try {
                    Thread.sleep(6 - (t2 - t1));
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
            }
        }

        singleplayerMenu.clFrame();

        return new Expect<>(1);
    }

    public void startSettingsMenu(MainFrame frame, FontManager fontManager){
        SettingsMenu settingsMenu = new SettingsMenu(frame, componentManager, frame.getWidth(), frame.getHeight(), fontManager);
        while (!frame.shouldClose()) {
            long t1 = System.currentTimeMillis();
            frame.getGraphics().clear();
            settingsMenu.paint(frame.getGraphics());
            frame.update();
            if (settingsMenu.isOkPressed()){
                settingsMenu.clFrame();
                return;
            }
            long t2 = System.currentTimeMillis();
            if (t2 - t1 < 6) {
                try {
                    Thread.sleep(6 - (t2 - t1));
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
            }
        }
    }

    public void start(){
        //1280x800
        //1440x1080

        Grass32ConfigClient.loadGrass32();
        String resolution = Grass32ConfigClient.getResolution();

        int width = Integer.parseInt(resolution.split("x")[0]);
        int height = Integer.parseInt(resolution.split("x")[1]);

        MainFrame.FrameConfig frameConfig = new MainFrame.FrameConfig().disableVSync()
                .setIcon(AssetManager.loadImage("assets/ui/selectors/ui_attack.png"));

        if (Grass32ConfigClient.isFullscreenEnabled()){
            resolution = Grass32ConfigClient.getFullscreenResolution();

            width = Integer.parseInt(resolution.split("x")[0]);
            height = Integer.parseInt(resolution.split("x")[1]);
        }

        if (Grass32ConfigClient.getMSAASamples() > 0){
            frameConfig = frameConfig.enableMSAA(Grass32ConfigClient.getMSAASamples());
        }

        frame = new MainFrame(width, height, "Stalemate", frameConfig);
        if (Grass32ConfigClient.isFullscreenEnabled()){
            frame.setFullscreen(new VideoMode(width, height, Monitors.getPrimaryVideoMode().refreshRate()), Monitors.getPrimaryMonitor());
        }
        componentManager = new ComponentManager(frame);
        componentManager.enableDirectDrawing();
        FontManager fontManager = new FontManager();

        frame.show();
        MainMenu mainMenu = new MainMenu(frame,componentManager, width, height, fontManager);

        while (!frame.shouldClose()){
            long t1 = System.currentTimeMillis();
            frame.getGraphics().clear();
            mainMenu.paint(frame.getGraphics());

            frame.update();
            long t2 = System.currentTimeMillis();
            if (t2-t1 < 6) {
                try {
                    Thread.sleep(6-(t2-t1));
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
            }
            if (mainMenu.isPlayPressed()){
                mainMenu.cleanup();
                Expect<Integer, ?> erres = startClientMenu(fontManager);
                mainMenu.rebuildFont(); // texture gets corrupted on intel gpu
                mainMenu.reinit();
                if (erres.isNone()){
                    mainMenu.setMessage(erres.getResult().message());
                }
            }
            if (mainMenu.isSingleplayerPressed()){
                mainMenu.cleanup();
                Expect<Integer, ?> erres = startSingleplayerMenu(width, height, fontManager);
                mainMenu.rebuildFont(); //
                mainMenu.reinit();
                if (erres.isNone()){
                    mainMenu.setMessage(erres.getResult().message());
                }
            }
            if (mainMenu.isSettingsPressed()){
                mainMenu.cleanup();
                startSettingsMenu(frame, fontManager);
                mainMenu.rebuildFont(); //
                mainMenu.reinit();
            }
        }

        frame.dispose();
        System.exit(0);
    }

}
