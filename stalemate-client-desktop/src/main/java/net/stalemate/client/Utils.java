package net.stalemate.client;

import net.stgl.ui.STGLComponent;

public class Utils {
    /***
     * Makes stuff less painful
     */
    public static void setXY(STGLComponent comp, int x, int y){
        comp.setX(x);
        comp.setY(y);
    }
}
