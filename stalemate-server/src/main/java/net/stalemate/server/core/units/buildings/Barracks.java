/*
 * Stalemate Game
 * Copyright (C) 2022 Weltspear
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package net.stalemate.server.core.units.buildings;

import net.stalemate.server.core.Unit;
import net.stalemate.server.core.animation.Animation;
import net.stalemate.server.core.animation.AnimationController;
import net.stalemate.server.core.buttons.Scrap;
import net.stalemate.server.core.controller.Game;
import net.stalemate.server.core.properties.Properties;
import net.stalemate.server.core.units.util.IBuilding;
import net.stalemate.server.core.units.util.IConstructableBuilding;

public class Barracks extends Unit implements IConstructableBuilding, IBuilding {

    private boolean isMobilizing = false;
    private int mobilizationTurns = 0;


    public class StartMobilizationButton implements IStandardButton{

        @Override
        public String bind() {
            return "M";
        }

        @Override
        public String texture() {
            return "assets/ui/buttons/train_infantry_button.png";
        }

        @Override
        public String identifier() {
            return "button_begin_mobilization";
        }

        @Override
        public void action(Unit unit, Game gameController) {
            isMobilizing = true;
        }
    }

    public class CancelMobilizationButton implements IStandardButton{

        @Override
        public String bind() {
            return "M";
        }

        @Override
        public String texture() {
            return "assets/ui/buttons/scrap_unit_queue.png";
        }

        @Override
        public String identifier() {
            return "button_cancel_mobilization";
        }

        @Override
        public void action(Unit unit, Game gameController) {
            isMobilizing = false;
        }
    }

    public Barracks(int x, int y, Game game) {
        super(x, y, game, new UnitStats(10, 10,
                0, 0, 0, 0, -1, -1,
                0, 0, 0), new AnimationController(), "Barracks");

        Animation idle = new Animation(1);
        idle.addFrame("assets/units/building_barracks.png");
        anim.addAnimation("idle", idle);
        anim.setCurrentAnimation("idle");

        move_amount = -1;
    }

    @Override
    public IButton[] getButtons() {
        IButton[] buttons = new IButton[9];
        if (!isMobilizing){
            buttons[0] = new StartMobilizationButton();
        }
        else buttons[0] = new CancelMobilizationButton();
        buttons[8] = new Scrap();
        return buttons;
    }

    @Override
    public Properties getProperties() {
        Properties p = super.getProperties();
        p.rm("ended_turn");
        p.put("mobilization_turns", ""+mobilizationTurns);
        return p;
    }

    @Override
    public AnimationController underConstructionAC() {
        AnimationController ac = new AnimationController();
        Animation idle = new Animation(1);
        idle.addFrame("assets/units/building_barracks.png");
        ac.addAnimation("idle", idle);
        ac.setCurrentAnimation("idle");
        return ac;
    }

    @Override
    public void turnUpdate() {
        super.turnUpdate();
        if (isMobilizing && mobilizationTurns == 0){
            if (getTeam().getSteel() > 0){
                getTeam().setSteel(getTeam().getSteel()-1);
                mobilizationTurns += 5;
            }
        }

        if (mobilizationTurns > 0 && getTeam().getManpower() < 30){
            getTeam().setManpower((getTeam().getManpower()*10 + 1)/10f);
            mobilizationTurns--;
        }
    }


}
