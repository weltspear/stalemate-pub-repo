# NOTICE

Source code of this game is licensed under GNU AGPLv3 (see [LICENSE](LICENSE)).
(content which is located under src/main/java directory, src/test/java directory and src/python/ directory)

All content which is located under src/main/resources directory is licensed under
CC-BY-SA except font monogram. (see https://creativecommons.org/licenses/by-sa/4.0/legalcode) and content which is located
there was made by Weltspear and SP7 except font monogram.

This game uses font monogram:
https://datagoblin.itch.io/monogram

## Also Stalemate uses following open source components (they are bundled with jar file and executables):

`jackson-core`: \
Project link: https://github.com/FasterXML/jackson-core \
Author: FasterXML, LLC (http://fasterxml.com/) \
License: https://github.com/FasterXML/jackson-core/blob/2.14/LICENSE (Apache License 2.0) 

`jackson-annotations`: \
Project link: https://github.com/FasterXML/jackson-annotations \
Author: FasterXML, LLC (http://fasterxml.com/) \
License: https://github.com/FasterXML/jackson-annotations/blob/2.14/LICENSE (Apache License 2.0) 

`jackson-dataformats-text`: \
Project link: https://github.com/FasterXML/jackson-dataformats-text \
Author: FasterXML, LLC \
License: https://www.apache.org/licenses/LICENSE-2.0.txt (Apache License 2.0) 

`snakeyaml`: \
Project link: https://bitbucket.org/snakeyaml/snakeyaml/src/master/ \
Author: Snakeyaml developers \
License: https://bitbucket.org/snakeyaml/snakeyaml/src/master/LICENSE.txt 

JetBrains Annotations: \
Project link: https://github.com/JetBrains/java-annotations/blob/master/ \
Author: JetBrains \
Copyright: 2000-2016 JetBrains s.r.o \
License: https://github.com/JetBrains/java-annotations/blob/master/LICENSE.txt 

Launch4J Executable Wrapper: \
Project link: http://launch4j.sourceforge.net/ \
Author: grzegok \
License: https://opensource.org/licenses/mit-license.html (MIT License) 

SLF4J: \
Project link: https://github.com/qos-ch/slf4j \
Author: QOS.ch Sarl \
License: https://github.com/qos-ch/slf4j/blob/master/LICENSE.txt 

JOML: \
Project link: https://github.com/JOML-CI/JOML \
Author: JOML \
License: https://github.com/qos-ch/slf4j/blob/master/LICENSE.txt 

LWJGL3: \
Project link: https://github.com/LWJGL/lwjgl3/tree/master \
Author: LWJGL \
License: https://github.com/LWJGL/lwjgl3/blob/master/LICENSE.md 

GLFW: \
Project link: https://github.com/glfw/glfw \
Author: GLFW \
License: https://github.com/glfw/glfw/blob/master/LICENSE.md

FreeType: \
Project link: https://freetype.org/ \
License: https://gitlab.freedesktop.org/freetype/freetype/-/blob/master/docs/FTL.TXT 

The following Gradle plugins were used to build this project: \
Gradle Shadow: https://github.com/johnrengelman/shadow \
Gradle Launch4J: https://github.com/TheBoegl/gradle-launch4j 

Of course Gradle was used to build the project: \
https://gradle.org/ 

