package net.stalemate.client.ui;

import net.stalemate.StVersion;
import net.stalemate.client.AssetManager;
import net.stalemate.client.config.Grass32ConfigClient;
import net.stgl.MainFrame;
import net.stgl.STGLGraphics;
import net.stgl.Utils;
import net.stgl.event.WindowResizeListener;
import net.stgl.font.FontManager;
import net.stgl.font.STGLFont;
import net.stgl.image.STGLImage;
import net.stgl.texture.Texture;
import net.stgl.texture.TextureManager;
import net.stgl.ui.ComponentManager;
import net.stgl.ui.STGLButton;
import net.stgl.ui.STGLCheckbox;
import net.stgl.ui.STGLEntry;
import net.stgl.vidmode.Monitors;
import net.stgl.vidmode.VideoMode;
import org.joml.Vector4f;

import java.awt.*;
import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.locks.ReentrantLock;

import static net.stalemate.client.Utils.setXY;

public class SettingsMenu {
    private final ReentrantLock lock = new ReentrantLock();
    private final ComponentManager componentManager;

    private final Texture background;
    private MainFrame frame;

    private int width;
    private int height;

    private final STGLFont monogram12;

    private final STGLImage information_logo;

    private static final int box_w = 500;
    private static final int box_h = 400;
    private STGLCheckbox msaa_ch;
    private STGLEntry sampleAmount;
    private STGLCheckbox fullscreen_ch;
    private STGLEntry window_resolution;
    private STGLButton res_btn_1;
    private STGLButton res_btn_2;
    private STGLEntry nickname_entry;
    private STGLButton apply;
    private STGLButton ok;

    private final ArrayList<String> videoModes = new ArrayList<>();
    private int vdmode = -1;

    private boolean isOkPressed = false;

    private final WindowResizeListener resizeListener = (nwidth, nheight) -> {
        width = nwidth;
        height = nheight;

        int x_m = (width - box_w) / 2;
        int y_m = (height - box_h) / 2;

        setXY(sampleAmount, x_m+33, y_m+87+10);
        setXY(msaa_ch, x_m+33, y_m+77);
        setXY(fullscreen_ch, x_m+33, (int) (y_m+87+10+13+23/1.5));
        setXY(window_resolution, x_m+33, (int) ((y_m+87+10+13+23/1.5)+32));
        setXY(res_btn_1, x_m+33, (int) ((y_m+87+10+13+23/1.5)+32+13+23/1.5+6+3));
        setXY(res_btn_2, x_m+33+110-23, (int) ((y_m+87+10+13+23/1.5)+32+13+23/1.5+6+3));
        setXY(nickname_entry, x_m+33, (int) ((y_m+87+10+13+23/1.5)+32+13+23/1.5+6+3)+45+14+7);
        setXY(apply, x_m+box_w-125-4, y_m+box_h-23);
        setXY(ok, x_m+box_w-125-4-125, y_m+box_h-23);

        if (!frame.isFullscreen())
            window_resolution.setText(new StringBuilder(nwidth + "x" + nheight));

    };

    public SettingsMenu(MainFrame frame, ComponentManager componentManager, int width, int height, FontManager fontManager){
        this.frame = frame;
        frame.addWindowResizeListener(resizeListener);
        Grass32ConfigClient.loadGrass32();
        this.componentManager = componentManager;

        monogram12 = fontManager.createFont("font/monogram-extended.ttf", 12, false, false);

        this.width = width;
        this.height = height;

        int i = 0;
        for (VideoMode vd : Monitors.getMonitors().get(0).videoModes()){
            String vdm = vd.width()+"x"+vd.height();
            if (vd.width() >= 1920){
                continue;
            }
            if (vd.width() <= 832 && vd.height() <= 576){
                continue;
            }
            if (!videoModes.contains(vdm)) {
                videoModes.add(vdm);

                if (vdm.equals(Grass32ConfigClient.getFullscreenResolution())){
                    vdmode = i;
                }

                i++;
            }
        }

        background = new Texture(AssetManager.loadImage("assets/background.png"));

        STGLImage checkbox_yes;
        STGLImage checkbox_no;
        try {
            information_logo = new STGLImage(TextureManager.loadBufferedImage("assets/information_logo.png"), new Texture.TextureOptions().setMinFiltering(Texture.Filtering.NEAREST).setMagFiltering(Texture.Filtering.NEAREST));
            checkbox_yes = new STGLImage(TextureManager.loadBufferedImage("assets/checkbox_yes.png"));
            checkbox_no = new STGLImage(TextureManager.loadBufferedImage("assets/checkbox_no.png"));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        int x_m = (width - box_w) / 2;
        int y_m = (height - box_h) / 2;

        sampleAmount = new STGLEntry(x_m+33, y_m+87+10, 110, 23, "Enter sample amount");
        StalemateStyle.makeEntry(sampleAmount);
        sampleAmount.setFont(monogram12);
        sampleAmount.partiallyDisable();
        componentManager.addComponent(sampleAmount);

        msaa_ch = new STGLCheckbox(x_m+33, y_m+77, "Enable MSAA?", checkbox_yes, checkbox_no);
        StalemateStyle.makeCheckbox(msaa_ch);
        msaa_ch.setFont(monogram12);
        msaa_ch.setCheckActionListener(() -> {
            if (msaa_ch.isChecked()){
                sampleAmount.partiallyEnable();
            }
            else{
                sampleAmount.partiallyDisable();
            }
        });
        componentManager.addComponent(msaa_ch);
        if (Grass32ConfigClient.getMSAASamples()>0){
            msaa_ch.setChecked(true);
            sampleAmount.setText(new StringBuilder(""+Grass32ConfigClient.getMSAASamples()));
            sampleAmount.partiallyEnable();
        }

        fullscreen_ch = new STGLCheckbox(x_m+33, (int) (y_m+87+10+13+23/1.5), "Enable Fullscreen?", checkbox_yes, checkbox_no);
        StalemateStyle.makeCheckbox(fullscreen_ch);
        fullscreen_ch.setFont(monogram12);
        componentManager.addComponent(fullscreen_ch);
        if (Grass32ConfigClient.isFullscreenEnabled()){
            fullscreen_ch.setChecked(true);
        }

        window_resolution = new STGLEntry(x_m+33, (int) ((y_m+87+10+13+23/1.5)+32), 110, 23, "Enter resolution:");
        window_resolution.partiallyDisable();
        StalemateStyle.makeEntry(window_resolution);
        window_resolution.setFont(monogram12);
        //window_resolution.partiallyDisable();
        componentManager.addComponent(window_resolution);
        window_resolution.setText(new StringBuilder(frame.getWidth() + "x" + frame.getHeight()));

        res_btn_1 = new STGLButton(23, 23, x_m+33, (int) ((y_m+87+10+13+23/1.5)+32+13+23/1.5+6+3), "<");
        StalemateStyle.makeButton(res_btn_1);
        res_btn_1.setFont(monogram12);
        res_btn_1.setBackground(bbcolor);
        componentManager.addComponent(res_btn_1);
        res_btn_1.setActionListener(() -> {
            vdmode--;
            if (vdmode < 0){
                vdmode++;
            }
        });

        res_btn_2 = new STGLButton(23, 23, x_m+33+110-23, (int) ((y_m+87+10+13+23/1.5)+32+13+23/1.5+6+3), ">");
        StalemateStyle.makeButton(res_btn_2);
        res_btn_2.setFont(monogram12);
        res_btn_2.setBackground(bbcolor);
        componentManager.addComponent(res_btn_2);
        res_btn_2.setActionListener(() -> {
            vdmode++;
            if (vdmode >= videoModes.size()){
                vdmode--;
            }
        });

        res_btn_1.partiallyDisable();
        res_btn_2.partiallyDisable();

        fullscreen_ch.setCheckActionListener(() ->{
            if (fullscreen_ch.isChecked()){
                res_btn_1.partiallyEnable();
                res_btn_2.partiallyEnable();
            }
            else{
                res_btn_1.partiallyDisable();
                res_btn_2.partiallyDisable();
            }
        });

        if (Grass32ConfigClient.isFullscreenEnabled()){
            res_btn_1.partiallyEnable();
            res_btn_2.partiallyEnable();
        }

        nickname_entry = new STGLEntry(x_m+33, (int) ((y_m+87+10+13+23/1.5)+32+13+23/1.5+6+3)+45+14+7, 110, 23, "Enter your nickname:");
        StalemateStyle.makeEntry(nickname_entry);
        nickname_entry.setFont(monogram12);
        componentManager.addComponent(nickname_entry);
        nickname_entry.setText(new StringBuilder(Grass32ConfigClient.getNickname()));

        apply = new STGLButton(125, 23, x_m+box_w-125-4, y_m+box_h-23, "Apply");
        StalemateStyle.makeButton(apply);
        apply.setFont(monogram12);
        componentManager.addComponent(apply);
        apply.setActionListener(this::applySettings);

        ok = new STGLButton(125, 23, x_m+box_w-125-4-125, y_m+box_h-23, "OK");
        StalemateStyle.makeButton(ok);
        ok.setFont(monogram12);
        ok.setActionListener(() ->{
            if (applySettings())
                isOkPressed = true;
        });
        componentManager.addComponent(ok);

    }

    private final Vector4f fcolor = Utils.colorToVec(new Color(198, 130, 77));
    private final Vector4f bbcolor = Utils.colorToVec(new Color(51, 39, 31));

    public void paint(STGLGraphics g) {
        lock.lock();
        g.drawTextureScaled(0, 0, background, new Vector4f(1, 1, 1, 1), width, height);

        g.renderText(monogram12, 3, height - 6, "Version " + StVersion.version, new Vector4f(1, 1, 1, 1));
        g.renderText(monogram12, width - monogram12.getFontMetrics().getStringWidth("See NOTICE.md and LICENSE.md for m"), height - 6 - 30, "Made by Weltspear and SP7", new Vector4f(1, 1, 1, 1));
        g.renderText(monogram12, width - monogram12.getFontMetrics().getStringWidth("See NOTICE.md and LICENSE.md for m"), height - 6 - 20, "Licensed under terms of GNU AGPLv3", new Vector4f(1, 1, 1, 1));
        g.renderText(monogram12, width - monogram12.getFontMetrics().getStringWidth("See NOTICE.md and LICENSE.md for m"), height - 6 - 10, "See NOTICE.md and LICENSE.md for m", new Vector4f(1, 1, 1, 1));
        g.renderText(monogram12, width - monogram12.getFontMetrics().getStringWidth("See NOTICE.md and LICENSE.md for m"), height - 6, "ore information", new Vector4f(1, 1, 1, 1));

        //

        int x_m = (width - box_w) / 2;
        int y_m = (height - box_h) / 2;

        g.fillRect(x_m, y_m, box_w, box_h, Utils.colorToVec(new Color(60, 38, 22)));
        g.fillRect(x_m, y_m, box_w, 45, Utils.colorToVec(new Color(131, 71, 37)));
        g.drawRect(x_m, y_m, box_w, box_h, 4, Utils.colorToVec(new Color(52, 33, 19)));
        g.drawRect(x_m, y_m, box_w, 45, 4, Utils.colorToVec(new Color(52, 33, 19)));
        g.renderText(monogram12,
                x_m + (box_w - monogram12.getFontMetrics().getStringWidth("Settings")) / 2,
                y_m + (45 - monogram12.getFontMetrics().getMaxCharHeight()) / 2 + monogram12.getFontMetrics().getMaxCharHeight(), "Settings", fcolor);
        g.drawImage(x_m + 5, y_m + (45 - 32) / 2 + 2, information_logo);

        // Video settings

        g.renderText(monogram12, x_m+33, y_m+65, "Video settings:", fcolor);
        g.renderText(monogram12, x_m+33, (int) ((y_m+87+10+13+23/1.5)+20+5), "Window mode resolution:", fcolor);

        g.renderText(monogram12, x_m+33, (int) ((y_m+87+10+13+23/1.5)+32+13+23/1.5+6), "Fullscreen mode resolution:", fcolor);

        g.fillRect(x_m+33, (int) ((y_m+87+10+13+23/1.5)+32+13+23/1.5+6+3), 110, 23, bbcolor);

        STGLFont.FontMetrics fontMetrics = monogram12.getFontMetrics();
        if (vdmode == -1){
            g.renderText(monogram12, x_m+33+(110-fontMetrics.getStringWidth("none"))/2 ,
                    (int) ((y_m+87+10+13+23/1.5)+32+13+23/1.5+6+3 + (23-fontMetrics.getMaxCharHeight())/2+fontMetrics.getMaxCharHeight()), "none", fcolor);
        }
        else{
            g.renderText(monogram12, x_m+33+(110-fontMetrics.getStringWidth(videoModes.get(vdmode)))/2 ,
                    (int) ((y_m+87+10+13+23/1.5)+32+13+23/1.5+6+3 +
                            (23-fontMetrics.getMaxCharHeight())/2+fontMetrics.getMaxCharHeight()),
                    videoModes.get(vdmode), fcolor);
        }

        // error

        if (errorText != null){
            g.renderText(monogram12, x_m+box_w-monogram12.getFontMetrics().getStringWidth(errorText)-15,y_m+65, errorText, new Vector4f(1,0,0,1));
        }
        if (showWarning){
            g.renderText(monogram12,
                    x_m+box_w-monogram12.getFontMetrics().getStringWidth("NOTE: Some changes to video settings will take effect after a restart")-15,
                    y_m+65+13+3, "NOTE: Some changes to video settings will take effect after a restart",
                    new Vector4f(1,1,1,1));
        }

        // Multiplayer settings

        g.renderText(monogram12, x_m+33, (int) ((y_m+87+10+13+23/1.5)+32+13+23/1.5+6+3)+45, "Multiplayer settings:", fcolor);
        g.renderText(monogram12, x_m+33, (int) ((y_m+87+10+13+23/1.5)+32+13+23/1.5+6+3)+45+14, "Nickname:", fcolor);

        componentManager.paint();
        lock.unlock();

    }

    public boolean validateResolution(String resolution){
        String[] split = resolution.split("x");

        boolean error = split.length != 2;
        if (!error){
            int xa = 0;
            for (char c: resolution.toCharArray()){
                if (c == 'x'){
                    xa++;
                }
            }

            if (xa != 1){
                error = true;
            }
        }

        if (!error){
            try {
                Integer.parseInt(split[0]);
                Integer.parseInt(split[1]);
            }
            catch (Exception e){
                error = true;
            }
        }

        return !error;
    }

    public boolean validateSamples(String samples){
        try{
            Integer.parseInt(samples);
            return true;
        }catch (Exception e){
            return false;
        }
    }

    public int getSamples(String samples){
        try {
            return Integer.parseInt(sampleAmount.getText());
        } catch (Exception e){
            return 0;
        }
    }

    private String errorText;
    private boolean showWarning = false;

    public boolean applySettings(){
        if (msaa_ch.isChecked()){
            if (!validateSamples(sampleAmount.getText())){
                errorText = "Incorrect sample amount set";
                return false;
            }
        }

        if (!validateResolution(window_resolution.getText())){
            errorText = "Incorrect window resolution set";
            return false;
        }

        if (fullscreen_ch.isChecked()){
            if (vdmode == -1){
                errorText = "Incorrect fullscreen resolution set";
                return false;
            }
        }

        Grass32ConfigClient.dumpGrass32Config(
                window_resolution.getText(),
                null,nickname_entry.getText(),
                false,
                vdmode != -1 ?videoModes.get(vdmode) : "none",
                fullscreen_ch.isChecked(),
                msaa_ch.isChecked() ? getSamples(sampleAmount.getText()): 0
        );
        showWarning = true;

        if (fullscreen_ch.isChecked()){
            if (vdmode != -1) {
                String[] split = videoModes.get(vdmode).split("x");

                int w = Integer.parseInt(split[0]);
                int h = Integer.parseInt(split[1]);

                frame.setFullscreen(new VideoMode(w, h, Monitors.getPrimaryVideoMode().refreshRate()),
                        Monitors.getPrimaryMonitor());
            }
        }
        else{

            String[] split = window_resolution.getText().split("x");

            int w = Integer.parseInt(split[0]);
            int h = Integer.parseInt(split[1]);

            frame.setWindowed(w, h);
        }
        return true;
    }

    public void clFrame(){
        componentManager.removeComponent(msaa_ch);
        componentManager.removeComponent(sampleAmount);
        componentManager.removeComponent(fullscreen_ch);
        componentManager.removeComponent(window_resolution);
        componentManager.removeComponent(res_btn_1);
        componentManager.removeComponent(res_btn_2);
        componentManager.removeComponent(nickname_entry);
        componentManager.removeComponent(apply);
        componentManager.removeComponent(ok);
        frame.removeWindowResizeListener(resizeListener);

        monogram12.dispose();
        background.dispose();
        information_logo.drop();
    }

    public boolean isOkPressed() {
        return isOkPressed;
    }
}

