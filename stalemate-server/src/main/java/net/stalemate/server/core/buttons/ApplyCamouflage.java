package net.stalemate.server.core.buttons;

import net.stalemate.server.core.Unit;
import net.stalemate.server.core.buff.Camouflage;
import net.stalemate.server.core.controller.Game;

public class ApplyCamouflage implements Unit.IStandardButton {


    @Override
    public String bind() {
        return "C";
    }

    @Override
    public String texture() {
        return "assets/ui/buttons/apply_camo.png";
    }

    @Override
    public String identifier() {
        return "button_apply_camo";
    }

    @Override
    public void action(Unit unit, Game gameController) {
        if (!unit.hasTurnEnded()){
            if (unit.getTeam().getSteel() > 0){
                for (Unit.Buff b: unit.getBuffs()){
                    if (b instanceof Camouflage)
                        return;
                }

                unit.getTeam().setSteel(unit.getTeam().getSteel()-1);
                unit.addBuff(new Camouflage(20));
            }
        }
    }
}
