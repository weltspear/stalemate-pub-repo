/*
 * Stalemate Game
 * Copyright (C) 2022 Weltspear
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package net.stalemate.server.core.gamemode.gamemodes;

import net.stalemate.server.core.Unit;
import net.stalemate.server.core.controller.Game;
import net.stalemate.server.core.gamemode.IGamemode;
import net.stalemate.server.core.units.buildings.MilitaryTent;

import java.util.ArrayList;

public class Versus implements IGamemode {

    private final Game g;

    public Versus(Game g){
        this.g = g;
    }
    @Override
    public void tick() {

    }

    @Override
    public boolean hasGameEnded() {
        return getVictoriousTeam() != null;
    }

    @Override
    public Game.Team getVictoriousTeam() {

        ArrayList<Game.Team> teams_who_have_bases = new ArrayList<>();

        for (Game.Team team : g.getTeams()) {
            if (!(team instanceof Game.NeutralTeam)) {
                if (!team.getTeamUnits().isEmpty() && !team.getIsDisabledTurn())
                    for (Unit u : team.getTeamUnits()) {
                        if (u instanceof MilitaryTent) {
                            teams_who_have_bases.add(team);
                            break;
                        }
                    }
            }
        }

        if (teams_who_have_bases.size() == 1) {
            return teams_who_have_bases.get(0);
        }

        return null;
    }

    @Override
    public String gmName() {
        return "Versus";
    }
}
