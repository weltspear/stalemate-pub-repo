/*
 * Stalemate Game
 * Copyright (C) 2022 Weltspear
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package net.stalemate.client;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.json.JsonMapper;
import com.fasterxml.jackson.databind.jsontype.BasicPolymorphicTypeValidator;
import com.fasterxml.jackson.databind.jsontype.PolymorphicTypeValidator;
import net.stalemate.utils.error.ErrorResult;
import net.stalemate.utils.error.Expect;
import net.stalemate.utils.etable.EntryTable;
import net.stalemate.StVersion;
import net.stalemate.client.config.Grass32ConfigClient;
import net.stalemate.client.ui.InGameUI;
import net.stalemate.client.ui.LobbySelectMenu;
import net.stalemate.client.ui.LobbyWaitMenu;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.net.ConnectException;
import java.net.InetAddress;
import java.net.Socket;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.Semaphore;
import java.util.concurrent.locks.ReentrantLock;


@SuppressWarnings("unchecked")
public class Client {

    private volatile boolean firstPacketDone = false;

    private final String ip;
    private InGameUI inGameUI;

    public void setInGameUI(InGameUI inGameUI) {
        this.inGameUI = inGameUI;
    }

    //private final ClientMenu clientMenu;
    private Socket client;

    private PrintWriter output;
    private BufferedReader input;

    private static final Logger LOGGER = LoggerFactory.getLogger(Client.class.getSimpleName());

    private ClientGame cgame = null;
    private volatile LobbySelectMenu lobbySelectMenu;

    private final ReentrantLock lock = new ReentrantLock();

    private final Semaphore wLobbyWaitMenu = new Semaphore(0);
    private final Semaphore wInGameUI = new Semaphore(0);

    public Semaphore getwInGameUI() {
        return wInGameUI;
    }

    public Semaphore getwLobbyWaitMenu() {
        return wLobbyWaitMenu;
    }

    private LobbyWaitMenu lobbyWaitMenu;

    public void setLobbyWaitMenu(LobbyWaitMenu lobbyWaitMenu) {
        this.lobbyWaitMenu = lobbyWaitMenu;
    }

    public Client(String ip){
        this.inGameUI = null;

        this.ip = ip;
    }

    public static class GameControllerClient{

        private int client_sel_x;
        private int client_sel_y;

        private int sel_x;
        private int sel_y;

        private int cbas_x;
        private int cbas_y;

        private int sbas_x;
        private int sbas_y;

        public ClientGame getClientGame() {
            return clientGame;
        }

        private final InGameUI inGameUI;
        private final ClientGame clientGame;

        HashMap<String, Object> selected_unit = null;

        private boolean isselectorbutton_press = false;

        private boolean first_packet = true;

        public GameControllerClient(/*InGameUI.KeyboardInput input,*/ InGameUI inGameUI, ClientGame clientGame){
            this.inGameUI = inGameUI;
            //in = input;

            this.clientGame = clientGame;
            //this.inGameUI = inGameUI;
        }

        @SuppressWarnings("unchecked")
        public Expect<String, ?> receive_packet(String json){
            try {

                PolymorphicTypeValidator ptv = BasicPolymorphicTypeValidator.builder()
                        .build();
                ObjectMapper objectMapper = JsonMapper.builder().polymorphicTypeValidator(ptv).build();
                Map<String, Object> data_map = (objectMapper).readValue(json, Map.class);

                Expect<String, ?> e = this.clientGame.load(data_map);

                sel_x = (int) data_map.get("sel_x");
                sel_y = (int) data_map.get("sel_y");

                client_sel_x = (int) data_map.get("sel_x");
                client_sel_y = (int) data_map.get("sel_y");

                cbas_x = (int) data_map.get("cbas_x");
                cbas_y = (int) data_map.get("cbas_y");

                sbas_x = (int) data_map.get("sbas_x");
                sbas_y = (int) data_map.get("sbas_y");

                if (first_packet){
                    client_sel_x = sbas_x;
                    client_sel_y = sbas_y;

                    inGameUI.unsafeLock.lock();
                    inGameUI.cam_x = cbas_x;
                    inGameUI.cam_y = cbas_y;
                    inGameUI.unsafeLock.unlock();

                    first_packet = false;
                }

                if (!(data_map.get("selected_unit_data") instanceof Integer) && data_map.get("selected_unit_data") != null){
                    selected_unit = (HashMap<String, Object>) data_map.get("selected_unit_data");

                    isselectorbutton_press = (boolean) selected_unit.get("iselectorbutton_press");
                } else{
                    selected_unit = null;
                    isselectorbutton_press = false;
                }

                return e;
            } catch (JsonProcessingException e) {
                return new Expect<>(() -> "Failed to parse JSON");
            } catch (ClassCastException | NullPointerException e){
                return new Expect<>(() -> "Incorrect packet format");
            }
        }

        private boolean reset_x_offset = false;
        private boolean reset_y_offset = false;

        public boolean[] resetOffsetArn(){
            try {
                return new boolean[]{reset_x_offset, reset_y_offset};
            } finally {
                reset_y_offset = false;
                reset_x_offset = false;
            }
        }

        public String create_json_packet(){
            HashMap<String, Object> packet = new HashMap<>();
            ArrayList<Object> actions = new ArrayList<>();


            while(!inGameUI.in_client.getQueue().isEmpty()) {
                String input = inGameUI.in_client.getQueue().poll();

                if (Objects.equals(input, "SHIFT")){
                    inGameUI.unsafeLock.lock();
                    inGameUI.cam_x = cbas_x;
                    inGameUI.cam_y = cbas_y;
                    inGameUI.unsafeLock.unlock();

                    client_sel_x = sbas_x;
                    client_sel_y = sbas_y;
                }
                else if (Objects.equals(input, "UP")){
                    client_sel_y--;
                }
                else if (Objects.equals(input, "DOWN")){
                    client_sel_y++;
                }
                else if (Objects.equals(input, "LEFT")){
                    client_sel_x--;
                }
                else if (Objects.equals(input, "RIGHT")){
                    client_sel_x++;
                }
                else if (Objects.equals(input, "SPACE")){
                    HashMap<String, Object> action = new HashMap<>();
                    action.put("action", "EndTurn");
                    actions.add(action);
                }
                else if (Objects.equals(input, "ENTER")) {
                    HashMap<String, Object> action = new HashMap<>();
                    if (isselectorbutton_press){
                        action.put("action", "ISBSelect");
                    }
                    else {
                        action.put("action", "SelectUnit");
                    }
                    actions.add(action);
                }
                else if (Objects.equals(input, "ESCAPE")){
                    HashMap<String, Object> action = new HashMap<>();
                    if (isselectorbutton_press)
                        action.put("action", "ISBCancel");
                    else
                        action.put("action", "DeselectUnit");
                    actions.add(action);
                }
                else if (Objects.equals(input, "TAB")){
                    HashMap<String, Object> action = new HashMap<>();
                    action.put("action", "ChangeViewMode");
                    actions.add(action);
                }
                else if ("qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM".contains(String.valueOf(input))){
                    if (selected_unit != null && input != null){
                        HashMap<String, Object> action = new HashMap<>();

                        for (Object button: (ArrayList<Object>)(selected_unit.get("buttons"))){
                            if (!(button instanceof Integer)){
                                HashMap<String, Object> b = (HashMap<String, Object>) button;

                                // Standard button press handling
                                if (((int)b.get("mode")) == 1){
                                    if (((String)b.get("bind")).equalsIgnoreCase(input)){
                                        action.put("action", "IStandardButtonPress");

                                        HashMap<String, Object> params = new HashMap<>();
                                        params.put("id", b.get("id"));
                                        action.put("params", params);

                                        actions.add(action);
                                    }
                                }
                                if (((int)b.get("mode")) == 2){
                                    if (((String)b.get("bind")).equalsIgnoreCase(input)){
                                        action.put("action", "ISelectorButtonPress");

                                        HashMap<String, Object> params = new HashMap<>();
                                        params.put("id", b.get("id"));
                                        action.put("params", params);

                                        actions.add(action);
                                    }
                                }
                            }
                        }
                    }
                }
            }


            inGameUI.unsafeLock.lock();

            packet.put("cam_x", inGameUI.cam_x);
            packet.put("cam_y", inGameUI.cam_y);

            inGameUI.unsafeLock.unlock();

            packet.put("sel_x", client_sel_x);
            packet.put("sel_y", client_sel_y);

            while (!inGameUI.in_client.getChatMSGS().isEmpty()){
                HashMap<String, String> typechat = new HashMap<>();
                typechat.put("action", "TypeChat");
                typechat.put("msg", inGameUI.in_client.getChatMSGS().poll());
                actions.add(typechat);
            }


            packet.put("actions", actions);

            try {
                PolymorphicTypeValidator ptv = BasicPolymorphicTypeValidator.builder()
                        .build();
                ObjectMapper objectMapper = JsonMapper.builder().polymorphicTypeValidator(ptv).build();
                return (objectMapper).writeValueAsString(packet);
            } catch (JsonProcessingException e) {
                e.printStackTrace();
            }
            return null;
        }

        public int getSelX() {
            return sel_x;
        }

        public int getSelY() {
            return sel_y;
        }

    }

    public void setLobbySelectMenu(LobbySelectMenu lobbySelectMenu){
        this.lobbySelectMenu = lobbySelectMenu;
    }

    private boolean hasConnectionStarted = false;

    public boolean hasConnectionStarted() {
        try {
            lock.lock();
            return hasConnectionStarted;
        } finally {
            lock.unlock();
        }
    }

    public Expect<Integer, ?> start_client(){
        try {
            client = new Socket(InetAddress.getByName(ip).getHostAddress(), 59657);

            return handle_connection();
        } catch (ConnectException e){
            return new Expect<>(() -> "Connection refused");
        } catch (IOException e){
            return new Expect<>(() -> "Unknown host");
        }
    }

    private boolean isConnectedToLobby = false;

    public boolean isConnectedToLobby() {
        return isConnectedToLobby;
    }

    private Expect<Integer, ?> handle_connection(){
        try {
            hasConnectionStarted = true;
            // load grass32

            client.setTcpNoDelay(true);
            client.setSoTimeout(Grass32ConfigClient.getTimeout() * 1000);

            client.setSoTimeout(Grass32ConfigClient.getLobbyTimeout() * 1000);

            // Initialize output and input
            output = new PrintWriter(new BufferedWriter(new OutputStreamWriter(client.getOutputStream(), StandardCharsets.UTF_8)), true);
            input = new BufferedReader(new InputStreamReader(client.getInputStream(), StandardCharsets.UTF_8));
            LOGGER.info( "Input output initialized");

            writeSafely(String.valueOf(StVersion.packet_version));
            Expect<String, ?> response_p = readSafely();

            if (response_p.isNone()){
                client.close();
                LOGGER.warn( "Failed to get server response");
                return new Expect<>(() -> "Failed to get server response");
            }

            if (!(response_p.unwrap().equals("ok"))){
                Expect<String, ?> response_p2 = readSafely();

                if (response_p2.isNone()){
                    client.close();
                    LOGGER.warn( "Failed to get server response");
                    return new Expect<>(() -> "Failed to get server response");
                }
                return new Expect<>(response_p2::unwrap);
            }

            Expect<String, ?> srv_desc = readSafely();
            if (srv_desc.isNone()){
                client.close();
                LOGGER.warn( "Failed to get server description");
                return new Expect<>(() -> "Failed to get server description");
            }

            // Make player choose the lobby
            Expect<String, ?> lobby_list = readSafely();
            if (lobby_list.isNone()) {
                client.close();
                LOGGER.warn( "Failed to read lobby list: " + lobby_list.getResult().message());
                String msg = "Failed to read lobby list: " + lobby_list.getResult().message();
                return new Expect<>(() -> msg);
            }
            PolymorphicTypeValidator ptv = BasicPolymorphicTypeValidator.builder()
                    .build();
            ObjectMapper objectMapper = JsonMapper.builder().polymorphicTypeValidator(ptv).build();
            EntryTable lobby_etable = new EntryTable((HashMap<String, Object>) (objectMapper).readValue(lobby_list.unwrap(), HashMap.class));

            writeSafely(Grass32ConfigClient.getNickname());

            Expect<String, ?> response = readSafely();

            if (response.isNone()){
                client.close();
                LOGGER.warn( "Failed to get server response");
                return new Expect<>(() -> "Failed to get server response");
            }

            if (!response.unwrap().equals("ok")){
                Expect<String, ?> resp2 = readSafely();

                if (resp2.isNone()){
                    client.close();
                    LOGGER.warn( "Failed to get server response");
                    return new Expect<>(() -> "Failed to get server response");
                }
                else {
                    client.close();
                    LOGGER.warn( resp2.unwrap());
                    String msg = resp2.unwrap();
                    return new Expect<>(() -> msg);
                }
            }

            // wait for lobbySelectMenu

            while (lobbySelectMenu == null){
                Thread.onSpinWait();
            }

            lobbySelectMenu.setDesc(srv_desc.unwrap());

            boolean has_connected_to_lb = false;
            while (!has_connected_to_lb) {
                Expect<ArrayList<String>, EntryTable.EntryTableGetFailure> lblist = lobby_etable.get("lobbies");
                if (lblist.isNone()){
                    client.close();
                    LOGGER.warn( "Failed to get lobbies");
                    return new Expect<>(() -> "Failed to get lobbies");
                }

                lobbySelectMenu.setLobbies(lblist.unwrap());

                while (lobbySelectMenu.getStatus() == 0) {
                    writeSafely("continue");
                    readSafely();
                }

                if (lobbySelectMenu.getStatus() == 2) {
                    writeSafely("-1");
                    readSafely();

                    // get lobby list
                    lobby_list = readSafely();
                    if (lobby_list.isNone()) {
                        client.close();
                        LOGGER.warn( "Failed to read lobby list: " + lobby_list.getResult().message());
                        String msg = "Failed to read lobby list: " + lobby_list.getResult().message();
                        return new Expect<>(() -> msg);
                    }
                    ptv = BasicPolymorphicTypeValidator.builder()
                            .build();
                    objectMapper = JsonMapper.builder().polymorphicTypeValidator(ptv).build();
                    lobby_etable = new EntryTable((HashMap<String, Object>) (objectMapper).readValue(lobby_list.unwrap(), HashMap.class));
                    lobbySelectMenu.setStatus(0);
                } else if (lobbySelectMenu.getStatus() == 1) {
                    writeSafely("" + (lobbySelectMenu.getIndex() + 1));
                    Expect<String, ?> status = readSafely();
                    if (status.isNone()) {
                        client.close();
                        LOGGER.warn( "Connection lost!");
                        return new Expect<>(() -> "Connection lost!");
                    } else if (status.unwrap().equals("OK")) {
                        has_connected_to_lb = true;
                    } else {
                        lobbySelectMenu.setStatus(0);
                        lobbySelectMenu.setText("Can't connect to lobby because " + (status.unwrap().equals("INCORRECT_LOBBY") ? "incorrect lobby was chosen" :
                                status.unwrap().equals("STARTED") ? "game has already started" : status.unwrap().equals("FULL") ? "lobby is full" : "UNKNOWN"));
                        lobby_list = readSafely();
                        if (lobby_list.isNone()) {
                            client.close();
                            LOGGER.warn( "Failed to read lobby list: " + lobby_list.getResult().message());
                            String msg = "Failed to read lobby list: " + lobby_list.getResult().message();
                            return new Expect<>(() -> msg);
                        }
                    }
                } else if (lobbySelectMenu.getStatus() == 3) {
                    client.close();
                    LOGGER.info( "Disconnecting...");
                    return new Expect<>(1);
                }
            }
            //lobbySelectMenu.clFrame();

            isConnectedToLobby = true;

            LOGGER.info( "Connected to lobby!");

            wLobbyWaitMenu.acquire();

            // waiting in lobby
            while (true) {
                Expect<String, ?> msg = readSafely();
                if (msg.isNone()) {
                    client.close();
                    LOGGER.warn( "Connection lost!");
                    //lobbyMenu.clFrame();
                    return new Expect<>(() -> "Connection lost!");
                } else if (msg.unwrap().equals("start")) {
                    break;
                } else {
                    ptv = BasicPolymorphicTypeValidator.builder()
                            .build();
                    objectMapper = JsonMapper.builder().polymorphicTypeValidator(ptv).build();
                    EntryTable nick_etable = new EntryTable((objectMapper).readValue(msg.unwrap(), HashMap.class));
                    Expect<ArrayList<String>, ?> nicks = nick_etable.get("nicks");

                    if (nicks.isNone()){
                        client.close();
                        LOGGER.warn( "Failed to get nick list");
                        //lobbyMenu.clFrame();
                        return new Expect<>(() -> "Failed to get nick list");
                    }

                    lobbyWaitMenu.setNicks(nicks.unwrap());
                }

                if (lobbyWaitMenu.getStatus() == 1){
                    client.close();
                    return new Expect<>(1);
                }
                writeSafely("ok");
            }

            lobbyWaitMenu.setStatus(3);
            wInGameUI.acquire();

            client.setSoTimeout(Grass32ConfigClient.getTimeout() * 1000);


            cgame = new ClientGame(new ClientMapLoader());
            GameControllerClient controller = new GameControllerClient(inGameUI, cgame);

            int tick = 0;

            LOGGER.info("Game started!");

            while (true) {
                long t1 = System.currentTimeMillis();
                Expect<String, ?> json = readSafely();
                long t2 = System.currentTimeMillis();
                tick++;

                if (tick == 100) {
                    LOGGER.info("ping: {}", (t2 - t1));
                    tick = 0;
                }

                if (json.isNone()) {
                    client.close();
                    LOGGER.warn("Failed to read packet: {}", json.getResult().message());
                    String msg = "Failed to read packet: " + json.getResult().message();
                    return new Expect<>(() -> msg);
                }
                if (json.unwrap().startsWith("endofgame")) {
                    break;
                }

                if (json.unwrap().startsWith("connection_terminated")) {
                    String cause = input.readLine();
                    LOGGER.warn( "Lobby was terminated. Additional information: {}", cause);
                    client.close();
                    return new Expect<>(() -> "Lobby was terminated. Additional information: " + cause);
                }
                Expect<String, ?> expect = controller.receive_packet(json.unwrap());

                inGameUI.getClDataManager().setSelectorData(controller.sel_x, controller.sel_y);

                if (expect.isNone()) {
                    LOGGER.warn( "Failed to read server packet, shutting down client: {}", expect.getResult().message());
                    client.close();
                    String msg = "Failed to read server packet, shutting down client: " + expect.getResult().message();
                    return new Expect<>(() -> msg);
                }


                // Escape menu connection termination
                if (inGameUI.getStatus() == 1){
                    client.close();
                    return new Expect<>(1);
                }
                firstPacketDone = true;

                String packet = controller.create_json_packet();
                writeSafely(packet);

            }

            Expect<String, ?> result = readSafely();
            if (result.isNone()) {
                LOGGER.warn( "Failed to get result {}", result.getResult().message());
                client.close();
                String msg = "Failed to get result " + result.getResult().message();
                return new Expect<>(() -> msg);
            }
            client.close();
            inGameUI.setResults(result.unwrap());

            while (inGameUI.getStatus() != 1) {
                Thread.onSpinWait();
            }

            return new Expect<>(1);


        } catch (Exception e){
            LOGGER.error(e.getMessage());
        }

        return new Expect<>(1);
    }

    public Expect<String, ErrorResult> readSafely(){
        try {
            String data = input.readLine();
            if (data != null)
            return new Expect<>(data);
            else return new Expect<>(() -> "Connection lost!");
        } catch (Exception e){
            return new Expect<>(() -> "Connection lost!");
        }
    }

    public void writeSafely(String a){
        output.println(a);
    }

    public ClientGame getCgame(){
        return cgame;
    }

    public boolean isFirstPacketDone() {
        return firstPacketDone;
    }
}
