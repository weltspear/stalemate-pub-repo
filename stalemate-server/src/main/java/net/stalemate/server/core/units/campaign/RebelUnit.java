/*
 * Stalemate Game
 * Copyright (C) 2022 Weltspear
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package net.stalemate.server.core.units.campaign;

import net.stalemate.server.core.Unit;
import net.stalemate.server.core.animation.Animation;
import net.stalemate.server.core.animation.AnimationController;
import net.stalemate.server.core.buttons.*;
import net.stalemate.server.core.controller.Game;
import net.stalemate.server.core.name.UnitNameGen;
import net.stalemate.server.core.properties.Properties;
import net.stalemate.server.core.units.util.IUnitName;

import java.util.Random;

public class RebelUnit extends Unit implements IUnitName{

    private static final Random rnd = new Random();

    public RebelUnit(int x, int y, Game game) {
        super(x, y, game, new UnitStats(10, 10, 1, 1, 2, 1, 20, 20, 0, 1, 3),
                new AnimationController(), "Rebel");
        Animation idle = new Animation(20);
        idle.addFrame("assets/units/rebel_idle_1.png");
        idle.addFrame("assets/units/rebel_idle_2.png");

        anim.addAnimation("idle", idle);
        anim.setCurrentAnimation("idle");

        Animation attack = new Animation(1);
        attack.addFrame("assets/units/rebel_fire_1.png");
        attack.addFrame("assets/units/rebel_fire_2.png");
        attack.addFrame("assets/units/rebel_fire_3.png");
        attack.addFrame("assets/units/rebel_fire_4.png");
        attack.addFrame("assets/units/rebel_fire_5.png");

        anim.addAnimation("attack", attack);
        anim.addShift("attack", "idle");

        move_amount = 2;
        turn_move_amount = 2;
    }

    @Override
    public IButton[] getButtons() {
        IButton[] buttons = new IButton[9];
        buttons[0] = new AttackButton(attack_range);
        buttons[1] = new MoveButton(movement_range);
        buttons[2] = new ProtectUnitButton(Layer.GROUND);

        if (supply <= 5) {
            buttons[8] = new HPSacrificeSU();
        }

        if (hp < max_hp && supply > 3){
            buttons[7] = new RecoverButton();
        }

        return buttons;
    }

    public static String genRebelName(UnitNameGen unitNameGen){
        int tp = rnd.nextInt(4);
        String nm = unitNameGen.getUnitNumber()+"-"+unitNameGen.numEnd()+" ";
        if (tp == 0){
            return nm+"National Guards Unit";
        }
        else if (tp == 1){
            return nm+"Civic Front Unit";
        }
        else if (tp == 3) {
            return nm+"Marauder Unit";
        }
        else {
            return nm+"Revolutionary Front Unit";
        }
    }

    @Override
    public Properties getProperties() {
        Properties p = super.getProperties();

        if (uname.isEmpty()){
            uname = genRebelName(game.getUnitNameGen());
        }

        IUnitName.addNameProperty(uname, p);
        return p;
    }

    private String uname = "";

    @Override
    public String getUnitName() {
        return uname;
    }

    @Override
    public void setUnitName(String n) {
        uname = n;
    }
}
