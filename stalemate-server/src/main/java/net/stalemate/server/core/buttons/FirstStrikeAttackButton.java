/*
 * Stalemate Game
 * Copyright (C) 2022 Weltspear
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package net.stalemate.server.core.buttons;

import net.stalemate.server.core.Unit;
import net.stalemate.server.core.controller.Game;

public class FirstStrikeAttackButton implements Unit.ISelectorButtonUnit {
    private final AttackButton attackButton;

    private boolean hasDoneFirstStrike = false;

    public FirstStrikeAttackButton(int attack_range){
        attackButton = new AttackButton(attack_range, Unit.Layer.GROUND);
    }

    @Override
    public String bind() {
        return "A";
    }

    @Override
    public String texture() {
        return "assets/ui/buttons/attack_first_strike_button.png";
    }

    @Override
    public String identifier() {
        return "button_attack_first_strike_gun";
    }

    @Override
    public void action(Unit selected_unit, Unit unit, Game gameController) {
        if (unit.hasTurnEnded() || unit.getTurnMoveAmount() == 0 || hasDoneFirstStrike){
            return;
        }

        int old_moves = unit.getMoveAmount();

        attackButton.action(selected_unit, unit, gameController);

        unit.invertTurnStatus();
        unit.setMoveAmount(old_moves-1);

        hasDoneFirstStrike = true;

    }

    @Override
    public int selector_range() {
        return attackButton.selector_range();
    }

    @Override
    public String selector_texture() {
        return "assets/ui/selectors/ui_attack.png";
    }

    @Override
    public boolean isUsedOnOurUnit() {
        return false;
    }

    @Override
    public boolean isUsedOnEnemy() {
        return true;
    }

    @Override
    public boolean isUsedOnAlliedUnit() {
        return false;
    }

    public void turnUpdate(){
        hasDoneFirstStrike = false;
    }
}
