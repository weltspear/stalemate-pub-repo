/*
 * Stalemate Game
 * Copyright (C) 2022 Weltspear
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package net.stalemate.client.ui;

import net.stgl.Utils;
import net.stgl.ui.STGLButton;
import net.stgl.ui.STGLCheckbox;
import net.stgl.ui.STGLEntry;

import java.awt.*;

public class StalemateStyle {
    public static void makeButton(STGLButton button){
        button.setForeground(Utils.colorToVec(new Color(198, 130, 77)));
        button.setBackground(Utils.colorToVec(new Color(60, 38, 22)));
        button.setHoverForegroundColor(Utils.colorToVec(new Color(60, 38, 22)));
        button.setHoverBackgroundColor(Utils.colorToVec(new Color(198, 130, 77)));
        button.setPressedBackgroundColor(Utils.colorToVec(new Color(51, 39, 31)));
        button.setPressedForegroundColor(Utils.colorToVec(Color.BLACK));
        button.setPartiallyDisabledForegroundColor(Utils.colorToVec(new Color(128,128,128)));
        button.setPartiallyDisabledBackgroundColor(Utils.colorToVec(new Color(51, 39, 31)));
    }

    public static void makeCheckbox(STGLCheckbox checkbox){
        checkbox.setForeground(Utils.colorToVec(new Color(198, 130, 77)));
        checkbox.setBackground(Utils.colorToVec(new Color(60, 38, 22)));
        checkbox.setPartiallyDisabledForegroundColor(Utils.colorToVec(new Color(128,128,128)));
        checkbox.setPartiallyDisabledBackgroundColor(Utils.colorToVec(new Color(51, 39, 31)));
    }

    public static void makeEntry(STGLEntry entry){
        entry.setForeground(Utils.colorToVec(new Color(122, 82, 45)));
        entry.setBackground(Utils.colorToVec(new Color(60, 38, 22)));
        entry.setActiveBackgroundColor(Utils.colorToVec(new Color(60, 38, 22)));
        entry.setActiveForegroundColor(Utils.colorToVec(new Color(198, 130, 77)));
        entry.setPartiallyDisabledForegroundColor(Utils.colorToVec(new Color(128,128,128)));
        entry.setPartiallyDisabledBackgroundColor(Utils.colorToVec(new Color(51, 39, 31)));
    }
}
