/*
 * Stalemate Game
 * Copyright (C) 2022 Weltspear
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package net.stalemate.server.core.gamemode.gamemodes;

import net.stalemate.server.core.Entity;
import net.stalemate.server.core.Unit;
import net.stalemate.server.core.ai.BattleGroup;
import net.stalemate.server.core.controller.Game;
import net.stalemate.server.core.gamemode.IGamemode;
import net.stalemate.server.core.gamemode.IGamemodeAI;
import net.stalemate.server.core.gamemode.IGamemodeTextUI;
import net.stalemate.server.core.properties.Properties;
import net.stalemate.server.core.units.*;
import net.stalemate.server.core.units.buildings.MilitaryTent;
import net.stalemate.singleplayer.AITurn;
import net.stalemate.singleplayer.textui.GoalUI;
import net.stalemate.singleplayer.textui.TextUI;
import org.joml.Vector2i;
import org.joml.Vector4i;

import java.util.*;

public class Fortress implements IGamemode, IGamemodeAI, EventListener, IGamemodeTextUI {

    private static final int turn_amount = 55;
    private final Game g;

    private int turns_passed = 0;

    @Override
    public TextUI getTextUI() {
        return new GoalUI(" Survive for " + turn_amount + " turns", 15, new boolean[]{false});
    }

    ArrayList<Vector4i> spawnRects;

    FortressAI ai = null;

    @SuppressWarnings("unchecked")
    public Fortress(Game g) {
        this.g = g;

        if (g.getAparams().containsKey("spawnrects")) {
            spawnRects = new ArrayList<>();
            ArrayList<ArrayList<Integer>> spawnrs = (ArrayList<ArrayList<Integer>>) g.getAparams().get("spawnrects");
            for (ArrayList<Integer> rect : spawnrs) {
                spawnRects.add(new Vector4i(rect.get(0), rect.get(1), rect.get(2), rect.get(3)));
            }
        }
    }


    @Override
    public void tick() {

    }

    @Override
    public boolean hasGameEnded() {
        return getVictoriousTeam()!=null;
    }

    private boolean hasAtLeastOneBase(Game.Team t){
        boolean b = false;

        for (Entity entity : t.getTeamUnits()){
            if (entity instanceof MilitaryTent){
                b = true;
                break;
            }
        }

        return b;
    }

    @Override
    public Game.Team getVictoriousTeam() {
        if (turns_passed >= turn_amount) {
            for (Game.Team team : g.getTeams()) {
                if (!Objects.equals(team.getTeamName(), null) && !Objects.equals(team.getTeamName(), "neutral")
                    && !Objects.equals(team.getTeamName(), "fortressAI")) {
                    return team;
                }
            }
        }
        else{
            for (Game.Team team : g.getTeams()){
                if (!Objects.equals(team.getTeamName(), null) && !Objects.equals(team.getTeamName(), "neutral")
                        && !Objects.equals(team.getTeamName(), "fortressAI") && !hasAtLeastOneBase(team)) {
                    for (Game.Team team2 : g.getTeams()){
                        if (Objects.equals(team2.getTeamName(), "fortressAI")){
                            return team2;
                        }
                    }
                }
            }
        }
        return null;
    }

    @Override
    public String gmName() {
        return "Fortress";
    }

    public static class FortressAI implements AITurn{
        private final Game g;
        private final Game.Team t;
        private final ArrayList<Vector4i> rects;

        private static final Random RND = new Random();

        ArrayList<BattleGroup> battleGroups = new ArrayList<>();

        private int turnsDone = 0;

        private int waveSize = 1;

        private Unit targetBase = null;

        public FortressAI(Game g, Game.Team t, ArrayList<Vector4i> rects){
            this.g = g;
            this.t = t;
            this.rects = rects;

            for (Entity entity : g.getAllEntities()){
                if (entity instanceof MilitaryTent base){
                    if (!t.getTeamUnits().contains(base)){
                        targetBase = base;
                    }
                }
            }

            t.setTeamName("fortressAI");
        }

        private int infantryAmount2(){
            return Math.min((int) Math.ceil(-(waveSize / 3f - 4) * (waveSize / 3f - 4) + 15), 25);
        }

        private int artilleryAmount(){
            return Math.min((int) Math.ceil(((double) (waveSize-3))*1.5f), 25);
        }

        private int tankAmount(){
            return Math.min((int) Math.ceil(((double) (waveSize-5))*2f), 25);
        }

        private int heavyTankAmount(){
            return Math.min((int) (double) (waveSize - 5), 25);
        }

        private int apcAmount(){
            return Math.min(waveSize, 25);
        }

        private ArrayList<Unit> spawnUnits(Vector4i rect, int uamount, ArrayList<Vector2i> alreadySpawnedCoords, int type){
            ArrayList<Unit> units = new ArrayList<>();

            if (t.getTeamUnits().size()+uamount < 100)
                for (int i = 0; i < uamount; i++) {
                    Vector2i sp = new Vector2i(rect.x() + RND.nextInt(rect.z() - rect.x()),
                            rect.y() + RND.nextInt(rect.w() - rect.y()));

                    Unit unit = type == 1 ? new Infantry(sp.x(), sp.y(), g) : type == 2 ? new Artillery(sp.x(), sp.y(), g)
                            : type == 3 ? new LightTank(sp.x(), sp.y(), g) : type == 4 ? new HeavyTank(sp.x(), sp.y(), g):
                            new ArmoredPersonnelCarrier(sp.x(), sp.y(), g);

                    unit.setSupply(unit.getSupply() + 20);

                    t.addUnit(unit);

                    alreadySpawnedCoords.add(sp);
                    g.addEntity(unit);

                    units.add(unit);
                }

            return units;
        }

        @Override
        public void doTurn() {

            if (turnsDone == 5){
                turnsDone = 0;

                Vector4i rect;

                if (rects.size() == 1) {
                    rect = rects.get(0);
                } else {
                    rect = rects.get(RND.nextInt(rects.size()));
                }

                ArrayList<Vector2i> alreadySpawnedCoords = new ArrayList<>();

                ArrayList<Unit> spawnedUnits = new ArrayList<>();

                spawnedUnits.addAll(spawnUnits(rect, infantryAmount2(), alreadySpawnedCoords, 1));
                spawnedUnits.addAll(spawnUnits(rect, artilleryAmount(), alreadySpawnedCoords, 2));
                spawnedUnits.addAll(spawnUnits(rect, tankAmount(), alreadySpawnedCoords, 3));
                spawnedUnits.addAll(spawnUnits(rect, heavyTankAmount(), alreadySpawnedCoords, 4));
                if (waveSize % 3 == 0)
                    spawnedUnits.addAll(spawnUnits(rect, apcAmount(), alreadySpawnedCoords, 5));

                Collections.shuffle(spawnedUnits);

                while (!spawnedUnits.isEmpty()){
                    BattleGroup battleGroup = new BattleGroup(g, t);
                    for (int i = 0; i < 10 && !spawnedUnits.isEmpty(); i++){
                        Unit u = spawnedUnits.get(0);
                        spawnedUnits.remove(0);

                        battleGroup.addUnit(u);
                    }
                    battleGroup.attack(targetBase.getX(), targetBase.getY());
                    battleGroup.makeAutonomous();
                    battleGroups.add(battleGroup);
                }

                waveSize++;
            }

            ArrayList<BattleGroup> to_be_removed = new ArrayList<>();

            for (BattleGroup battleGroup: battleGroups){
                if (battleGroup.getUnitAmount() == 0){
                    to_be_removed.add(battleGroup);
                }
            }

            battleGroups.removeAll(to_be_removed);

            for (BattleGroup battleGroup : battleGroups){
                battleGroup.doTurn();
            }


            turnsDone++;
        }
    }

    @Override
    public AITurn getAI(Game.Team t) {

        ai = new FortressAI(g, t, spawnRects);

        return ai;
    }

    @Override
    public void onTurnEnd() {
        turns_passed++;
    }


    @Override
    public boolean isSingleplayerExclusive() {
        return true;
    }

    public String fortressInfo(){
        return "Turn: " + turns_passed + ' ' + "Wave: " + (ai.waveSize-1);
    }

    @Override
    public Properties getProperties() {
        Properties properties = new Properties();
        properties.put("wave", String.valueOf(ai.waveSize - 1));
        properties.put("turn", String.valueOf(turns_passed));
        return properties;
    }
}
