package net.stalemate.server.core.buff;

import net.stalemate.server.core.Unit;

public class Camouflage extends Unit.Buff {
    public Camouflage(int turnTime) {
        super(turnTime, Unit.UnitStats.ZERO);
    }

    @Override
    public void turnAction(Unit u) {
        u.mechanicMakeInvisible();
    }

    @Override
    public String getName() {
        return "Camouflage";
    }

    @Override
    public String getIndicatorPath() {
        return "assets/ui/buff/camo.png";
    }
}
