/*
 * Stalemate Game
 * Copyright (C) 2022 Weltspear
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package net.stalemate.client.ui;

import net.stalemate.client.AssetManager;
import net.stgl.MainFrame;
import net.stgl.STGLGraphics;
import net.stgl.Utils;
import net.stgl.event.WindowResizeListener;
import net.stgl.font.FontManager;
import net.stgl.font.STGLFont;
import net.stgl.texture.Texture;
import net.stgl.ui.ComponentManager;
import net.stgl.ui.STGLButton;
import net.stgl.ui.STGLMultiList;
import org.joml.Vector4f;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.locks.ReentrantLock;

import static net.stalemate.client.Utils.setXY;

public class LobbySelectMenu {
    private final MainFrame frame;
    private STGLMultiList multiList;
    private final int title_width_img;
    private final int title_height_img;
    private final Texture background;
    private final Texture title;

    private final ReentrantLock lock = new ReentrantLock();

    private int width;
    private int height;
    private final ComponentManager componentManager;

    private STGLButton connect;
    private STGLButton refresh;
    private STGLButton disconnect;

    private int status = 0;
    private String desc = "";

    public int getStatus() {
        try {
            lock.lock();
            return status;
        }
        finally {
            lock.unlock();
        }
    }

    private final STGLFont monogram;

    public void dispose(){
        background.dispose();
        title.dispose();
        monogram.dispose();
    }

    private WindowResizeListener resizeListener = (nwidth, nheight) -> {
        width = nwidth;
        height = nheight;
        int x_l = (width-400-50)/2;
        int y_l = (height-300)/2;
        setXY(multiList, x_l, y_l);
        setXY(connect, multiList.getX()+375+25,multiList.getY());
        setXY(refresh, multiList.getX()+375+25,multiList.getY()+25);
        setXY(disconnect, multiList.getX()+375+25,multiList.getY()+50);
    };

    public LobbySelectMenu(MainFrame frame, ComponentManager componentManager, FontManager fontManager){
        this.frame = frame;
        frame.addWindowResizeListener(resizeListener);

        width = frame.getWidth();
        height = frame.getHeight();
        this.componentManager = componentManager;

        monogram = fontManager.createFont("font/monogram-extended.ttf", 12, false, false);

        background = new Texture(AssetManager.loadImage("assets/background.png"));
        BufferedImage title_img = AssetManager.loadImage("assets/stalemate.png");
        title = new Texture(title_img.getWidth(), title_img.getHeight()+2, new Texture.TextureOptions().setMagFiltering(Texture.Filtering.NEAREST).setMinFiltering(Texture.Filtering.NEAREST));
        title.writeImage(0,0, title_img);
        title_width_img = title_img.getWidth();
        title_height_img = title_img.getHeight();

        int x_l = (width-400-50)/2;
        int y_l = (height-300)/2;


        multiList = new STGLMultiList(
                List.of(new STGLMultiList.DefaultColumn("Map", List.of(), 150),
                        new STGLMultiList.DefaultColumn("Gamemode", List.of(), 150),
                        new STGLMultiList.DefaultColumn("Player Count", List.of(), 75)),
                x_l, y_l, 400, 300, monogram.getMaxHeight()+4, 30);
        multiList.setForeground(Utils.colorToVec(new Color(198, 130, 77)));
        multiList.setBackground(Utils.colorToVec(new Color(60, 38, 22)));
        multiList.setSelectedBackground(Utils.colorToVec(new Color(51, 39, 31)));
        multiList.setSelectedForeground(Utils.colorToVec(Color.BLACK));
        multiList.setFont(monogram);
        componentManager.addComponent(multiList);


        connect = new STGLButton(150, 25, multiList.getX()+375+25,multiList.getY(), "Connect");
        StalemateStyle.makeButton(connect);
        connect.setFont(monogram);
        connect.setActionListener(()-> status = 1);
        componentManager.addComponent(connect);

        refresh = new STGLButton(150, 25, multiList.getX()+375+25,multiList.getY()+25, "Refresh");
        StalemateStyle.makeButton(refresh);
        refresh.setFont(monogram);
        refresh.setActionListener(()-> status = 2);
        componentManager.addComponent(refresh);

        disconnect = new STGLButton(150, 25, multiList.getX()+375+25,multiList.getY()+50, "Disconnect");
        StalemateStyle.makeButton(disconnect);
        disconnect.setActionListener(()-> status = 3);
        disconnect.setFont(monogram);
        componentManager.addComponent(disconnect);
    }

    public void paint(STGLGraphics g) {
        g.drawTextureScaled(0, 0, background, new Vector4f(1, 1, 1, 1), width, height);
        g.fillRect(multiList.getX()+375+25, multiList.getY()+75, 150, 225, Utils.colorToVec(new Color(60, 38, 22)));
        g.drawRect(multiList.getX()+375+25, multiList.getY()+75, 150, 225, Utils.colorToVec(new Color(198, 130, 77)));
        g.renderFormattedText(monogram, monogram, monogram, monogram, desc, multiList.getX()+375+25+monogram.getFontMetrics().getMaxCharWidth()/2, multiList.getY()+75+monogram.getMaxHeight());
        componentManager.paint();

        if (ertext != null)
            g.renderText(monogram, multiList.getX(), multiList.getY()+300+monogram.getMaxHeight(), ertext, new Vector4f(1,0,0,1));

        if (title != null)
            g.drawTextureRegionScaled((width - 364) / 2, (int) (0.43f * height) - 194, title.getSubTexture(0, 0, title_width_img, title_height_img), new Vector4f(1, 1, 1, 1), 364, 64);
    }

    public void setLobbies(List<String> lobbies){
        try {
            lock.lock();

            ArrayList<String> maps = new ArrayList<>();
            ArrayList<String> gamemode = new ArrayList<>();
            ArrayList<String> playercounts = new ArrayList<>();

            for (String lb: lobbies){
                String[] lbs = lb.split(",");
                gamemode.add(lbs[0]);
                maps.add(lbs[1]);
                playercounts.add(lbs[2]);
            }


            multiList.setColumns(
                    List.of(new STGLMultiList.DefaultColumn("Map", maps, 150),
                            new STGLMultiList.DefaultColumn("Gamemode", gamemode, 150),
                            new STGLMultiList.DefaultColumn("Player Count", playercounts, 75))
            );

        }finally {
            lock.unlock();
        }
    }

    public void setStatus(int i) {
        try {
            lock.lock();
            status = i;
        }
        finally {
            lock.unlock();
        }
    }

    public int getIndex(){
        try {
            lock.lock();
            return multiList.getSelectedIdx();
        }
        finally {
            lock.unlock();
        }
    }

    private String ertext = null;

    public void setText(String s) {
        lock.lock();
        ertext = s;
        lock.unlock();
    }

    public void clFrame() {
        componentManager.removeComponent(connect);
        componentManager.removeComponent(refresh);
        componentManager.removeComponent(disconnect);
        componentManager.removeComponent(multiList);
        frame.removeWindowResizeListener(resizeListener);
    }

    public void setDesc(String desc){
        lock.lock();
        this.desc = desc.replace("<br>", "\n");
        lock.unlock();
    }
}
