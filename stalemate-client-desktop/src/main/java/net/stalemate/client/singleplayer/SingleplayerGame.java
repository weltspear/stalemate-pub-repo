/*
 * Stalemate Game
 * Copyright (C) 2022 Weltspear
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package net.stalemate.client.singleplayer;

import net.stalemate.utils.error.Expect;
import net.stalemate.client.Client;
import net.stalemate.client.ClientGame;
import net.stalemate.client.ClientMapLoader;
import net.stalemate.client.config.Grass32ConfigClient;
import net.stalemate.client.ui.InGameUI;
import net.stalemate.server.lobby_management.Lobby;
import net.stgl.MainFrame;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SingleplayerGame {

    private static final Logger LOGGER = LoggerFactory.getLogger(SingleplayerGame.class.getSimpleName());

    private final SingleplayerLobby lobby;
    private final Thread lobbyThr;

    private final Lobby.Player player;

    private final InGameUI inGameUI;
    private final Client.GameControllerClient gameController;

    private final ClientMapLoader clientMapLoader;

    private final ClientGame cgame;
    private final MainFrame frame;

    public SingleplayerGame(String map, int width, int height, MainFrame frame){
        this.frame = frame;
        this.player = new Lobby.Player();
        this.player.setNickname("Player");

        lobby = new SingleplayerLobby(map, player);
        clientMapLoader = new ClientMapLoader();
        clientMapLoader.load(map);
        lobbyThr = new Thread(lobby, "LobbyThread");

        /*stolen directly from main menu*/

        Grass32ConfigClient.loadGrass32();

        cgame = new ClientGame(clientMapLoader);
        inGameUI = new InGameUI(frame);
        gameController = new Client.GameControllerClient(inGameUI,cgame);

        inGameUI.getClDataManager().getMapLoader().loadFromOther(clientMapLoader);
    }

    public Expect<Integer, ?> startGame(){
        LOGGER.info("Starting internal lobby");
        lobbyThr.start();

        while(!lobby.isReady()){
            Thread.onSpinWait();
            if (lobby.isInvalidMap()){
                return new Expect<>(() -> "Selected map doesn't support single-player");
            }
        }

        while (!frame.shouldClose()){
            long t1 = System.currentTimeMillis();
            frame.getGraphics().clear();
            player.pushCommand(gameController.create_json_packet());
            gameController.receive_packet(player.createJsonPacket());

            inGameUI.unsafeLock.lock();
            Object[] ef = cgame.buildView(inGameUI.cam_x, inGameUI.cam_y, inGameUI.scale, (int) Math.ceil((inGameUI.tr_width - 832f) / 64f), (int) Math.ceil((inGameUI.tr_height - 576f) / 64f));

            inGameUI.getClDataManager().updateData(cgame.getChat(),
                    (ClientGame.ClientEntity[][]) ef[0], (boolean[][]) ef[1], (boolean[][]) ef[2], cgame.getSelectedUnit(), cgame.getSteel(), cgame.getAluminium(),
                    cgame.isIsItYourTurn(), cgame.getClMapLoader(), cgame.drawMinimap(inGameUI.cam_x, inGameUI.cam_y), cgame.getTeamDoingTurnColor(), cgame.getTeamDoingTurnNick(),
                    cgame.getGamemodeProperties(), cgame.getManpower());
            inGameUI.unsafeLock.unlock();

            inGameUI.getClDataManager().setSelectorData(gameController.getSelX(), gameController.getSelY());

            inGameUI.paint(frame.getGraphics());
            inGameUI.inGameUIUpdate();

            long t2 = System.currentTimeMillis() - t1;
            if (6 - t2 > 0){
                try {
                    Thread.sleep(6-t2);
                } catch (InterruptedException e) {
                    LOGGER.error(e.getMessage());
                }
            }

            inGameUI.textUI = lobby.getTextUI();

            if (player.getEndOfAGameMessage() != null){
                break;
            }

            if (inGameUI.getStatus() == 1){
                lobby.terminateLobby();
                inGameUI.dispose();
                inGameUI.clFrame();
                return new Expect<>(1);
            }
            frame.update();
        }

        inGameUI.setResults(player.getEndOfAGameMessage());

        while (!frame.shouldClose()) {
            try {
                Thread.sleep(6);
            } catch (InterruptedException e) {
                LOGGER.error(e.getMessage());
            }

            if (inGameUI.getStatus() == 1){
                break;
            }

            inGameUI.paint(frame.getGraphics());
            inGameUI.inGameUIUpdate();
            frame.update();
        }

        inGameUI.dispose();
        inGameUI.clFrame();

        return new Expect<>(1);
    }


}
