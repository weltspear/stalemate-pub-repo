/*
 * Stalemate Game
 * Copyright (C) 2022 Weltspear
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package net.stalemate.server.core.fx

import org.joml.Vector4f
import java.util.HashMap

class FxTextEffect(private var x: Int,
                   private var y: Int,
                   private var direction: FxTextDirection = FxTextDirection.UP,
                   private var color: Vector4f,
                   private var text: String,
                   private var requiresVision: Boolean = true) : FxGraphicalEffect {
    enum class FxTextDirection{
        UP,
        DOWN,
        NONE,
    }

    override fun serializedData(): HashMap<String, Any> {
        val data : HashMap<String, Any> = HashMap()

        val dir = when (direction){
            FxTextDirection.UP -> "up"
            FxTextDirection.DOWN -> "down"
            FxTextDirection.NONE -> "none"
        }

        data["color"] = listOf(color.x, color.y, color.z, color.w)
        data["text"] = text
        data["direction"] = dir

        return data
    }

    override fun requiresVision(): Boolean {
        return requiresVision
    }

    override fun fxId(): String = "fxText"
    override fun getX() = x
    override fun getY() = y
}