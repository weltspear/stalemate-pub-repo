/*
 * Stalemate Game
 * Copyright (C) 2022 Weltspear
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package net.stalemate.client.config;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.HashMap;

public class Grass32ConfigClient {
    private static String nickname = "unnamed";
    private static int timeout = 30;
    private static int lobby_timeout = 120;
    private static boolean steel_button_overlay = false;

    private static int msaa_samples = 0;

    private static String resolution = "832x576";

    private static String fullscreen_resolution = "none";
    private static boolean isFullscreenEnabled = false;

    @SuppressWarnings("unchecked")
    public static void loadGrass32(){
        try {

            ObjectMapper objectMapper = new ObjectMapper(new YAMLFactory());

            HashMap<String, Object> config_map = objectMapper.readValue(new File("grass32"), HashMap.class);

            HashMap<String, Object> client_config =
                    (HashMap<String, Object>) ((HashMap<String, Object>) (config_map.get("config"))).get("client");

            nickname = (String) client_config.get("nickname");
            timeout = (int) client_config.get("timeout");
            lobby_timeout = (int) client_config.get("lobby_timeout");
            steel_button_overlay = (boolean) client_config.get("steel_button_overlay");
            resolution = (String) client_config.get("resolution");

            if ( client_config.containsKey("msaa_samples"))
                msaa_samples = (int) client_config.get("msaa_samples");

            if (client_config.containsKey("do_fullscreen")){
                fullscreen_resolution = (String)client_config.get("fullscreen_resolution");
                isFullscreenEnabled = (boolean) client_config.get("do_fullscreen");
            }

            //HashMap<String, HashMap<String, String>> controls = (HashMap<String, HashMap<String, String>>) client_config.get("controls");

            //KeyboardBindMapper.makeBinds(controls.get("keyb"));

        } catch (Exception e){
            System.err.println("Failed to load grass32.");
            e.printStackTrace();
        }
    }

    public static void dumpGrass32Config(String new_resolution, HashMap<String, String> controls_dump, String nickname
                                         , boolean steel_button_overlay, String fullscreen_resolution,
                                         boolean doFullScreen, int msaa_samples){

        HashMap<String, Object> grass32 = new HashMap<>();

        HashMap<String, Object> config = new HashMap<>();

        HashMap<String, Object> client = new HashMap<>();
        client.put("nickname", nickname);
        client.put("timeout", 30);
        client.put("lobby_timeout", 120);
        client.put("resolution", new_resolution);

        client.put("fullscreen_resolution", fullscreen_resolution);
        client.put("do_fullscreen", doFullScreen);
        client.put("msaa_samples", msaa_samples);

        client.put("steel_button_overlay", steel_button_overlay);

        HashMap<String, Object> controls = new HashMap<>();
        controls.put("keyb", controls_dump);

        //client.put("controls", controls);

        config.put("client", client);

        grass32.put("config", config);

        String prestart_text = """
                # It is highly recommended to change your client config in the options menu
                # List of nice resolutions:
                # 832x576
                # 1024x768
                # 1152x768
                """;


        try {
            try (PrintWriter out = new PrintWriter("grass32")) {
                out.println(prestart_text+new ObjectMapper(new YAMLFactory()).writeValueAsString(grass32));
            }
        } catch (JsonProcessingException | FileNotFoundException e) {
            throw new RuntimeException(e);
        }

        Grass32ConfigClient.loadGrass32();
    }

    public static String getNickname() {
        return nickname;
    }

    public static int getTimeout() {
        return timeout;
    }

    public static int getLobbyTimeout() {
        return lobby_timeout;
    }

    public static boolean doSteelButtonOverlay() {
        return steel_button_overlay;
    }

    public static String getResolution(){return resolution;}

    public static int getMSAASamples() {
        return msaa_samples;
    }

    public static String getFullscreenResolution() {
        return fullscreen_resolution;
    }

    public static boolean isFullscreenEnabled() {
        return isFullscreenEnabled;
    }
}
