/*
 * Stalemate Game
 * Copyright (C) 2022 Weltspear
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package net.stalemate.client.ui;

import net.stalemate.StVersion;
import net.stalemate.client.AssetManager;
import net.stalemate.client.ClientGame;
import net.stalemate.client.ClientMapLoader;
import net.stalemate.client.SpecialTeamReprReg;
import net.stalemate.client.config.ButtonTooltips;
import net.stalemate.client.config.Grass32ConfigClient;
import net.stalemate.client.config.PropertiesMatcher;
import net.stalemate.client.property.ClientSideProperty;
import net.stalemate.singleplayer.textui.GoalUI;
import net.stalemate.singleplayer.textui.TextUI;
import net.stgl.MainFrame;
import net.stgl.STGLGraphics;
import net.stgl.Utils;
import net.stgl.debug.ErrorCheck;
import net.stgl.event.*;
import net.stgl.font.FontManager;
import net.stgl.font.STGLFont;
import net.stgl.font.FontManager;
import net.stgl.image.STGLImage;
import net.stgl.text.STGLTextFormatter;
import net.stgl.texture.*;
import net.stgl.ui.ComponentManager;
import net.stgl.ui.STGLButton;
import net.stgl.ui.STGLComponent;
import org.joml.Vector2i;
import org.joml.Vector4f;

import java.awt.*;
import java.awt.geom.AffineTransform;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.nio.BufferOverflowException;
import java.util.List;
import java.util.*;
import java.util.List;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.locks.ReentrantLock;

import static net.stalemate.client.Utils.setXY;

public class InGameUI {
    private final FontManager fontManager = new FontManager();
    private final ClientDataManager clDataManager;
    public final KeyboardInput in_client;
    @SuppressWarnings("FieldCanBeLocal") private final STGLFont monogram;

    private final Texture minimap_t;
    private final MainFrame frame;

    private ArrayList<BufferedImage> buffsTextures = null;
    private ArrayList<Integer> buffsTimes = null;
    private ArrayList<String> buffsNames = null;

    private final DynamicTextureAtlas textureAtlas = new DynamicTextureAtlas(250, 64, 64, new Texture.TextureOptions().setMinFiltering(Texture.Filtering.LINEAR_MIPMAPS_NEAREST).setMagFiltering(Texture.Filtering.NEAREST), 1);
    private final DynamicTextureAtlas selectedUnitAtlas = new DynamicTextureAtlas(20, 32, 32, new Texture.TextureOptions().setMinFiltering(Texture.Filtering.LINEAR).setMagFiltering(Texture.Filtering.NEAREST), 1);
    private final DynamicTextureAtlas selectorTextureAtlas = new DynamicTextureAtlas(30, 32, 32, new Texture.TextureOptions().setMagFiltering(Texture.Filtering.NEAREST).setMinFiltering(Texture.Filtering.NEAREST), 1);
    private final STGLFont monogram_button;
    private final STGLFont monogram_32;
    private final STGLFont monogram_32_bold;

    private final STGLFont monogram_19;

    private final STGLFont monogram_15;

    private final STGLFont monogram_18;

    private final STGLFont monogram_23;
    private final STGLFont monogram_button_small;
    private final STGLFont uname_monogram;
    private final MListener m;
    private final boolean focus_desktop_pane = false;

    private final STGLImage information_logo;
    private HashMap<String, String> gamemodeProperties = new HashMap<>();
    private BufferedImage minimap = null;

    public float scale = 1f;
    private boolean do_offset = false;

    private boolean hasFirstPackedBeenReceived = false;
    private Color teamDoingTurnColor = Color.WHITE;
    private String teamDoingTurnNick = "";

    public int tr_width;
    public int tr_height;

    public int[][] highlights = new int[9][];

    public int[] cur_highlight = null;

    private TextureBatch gameBatch;
    private TextureBatch uiBatch;

    private final ComponentManager escapeMenuCompManager;
    private final ArrayList<STGLComponent> escapeMenuComponents = new ArrayList<>();

    private Vector2i escapeMenuOff;

    private final Texture ttextUI;

    private STGLFont textUIFont = null;

    // camera movement related fields
    private boolean isCamMovementInProgress = false;
    private int origin_m_x = -1;
    private int origin_m_y = -1;

    /***
     * Ids of buttons to be rendered
     */
    String[][] id_array = {{null, null, null},
            {null, null, null},
            {null, null, null}};


    public ClientDataManager getClDataManager() {
        return clDataManager;
    }

    public KeyboardInput getInput() {
        return in_client;
    }

    public int getStatus() {
        try {
            unsafeLock.lock();
            return status;
        } finally {
            unsafeLock.unlock();
        }
    }

    public record PropertiesToRender(ArrayList<ClientSideProperty> properties){}

    private PropertiesToRender propertiesToRender = null;

    public final ReentrantLock unsafeLock = new ReentrantLock();

    private int offset_x = 0;
    private int offset_y = 0;
    private boolean dis_offset = false; // if true camera is locked

    private int camera_speed = 1;

    ArrayList<ArrayList<BufferedImage>> map_to_render = new ArrayList<>();
    ArrayList<ArrayList<BufferedImage>> fog_of_war = new ArrayList<>();
    // selector range
    ArrayList<ArrayList<BufferedImage>> _selr = new ArrayList<>();
    ArrayList<ArrayList<BufferedImage>> entity_render = new ArrayList<>();
    ArrayList<ArrayList<BufferedImage>> unit_data_ar = new ArrayList<>();
    ArrayList<BufferedImage> buttons = new ArrayList<>();
    ArrayList<BufferedImage> queue = null;
    ArrayList<String> binds = new ArrayList<>();
    ArrayList<String> unit_times = new ArrayList<>();
    ArrayList<String> chat = new ArrayList<>();
    BufferedImage unit_img = null;
    float steel = 0;
    float aluminium = 0;
    boolean is_it_your_turn = false;

    BufferedImage placeholder_ui;
    BufferedImage placeholder_ui_2;
    Texture panel;
    Texture steel_icon;
    Texture aluminium_icon;

    Texture manpower_img;

    final Texture bhighlight;

    BufferedImage selector;

    int sel_x_frame;
    int sel_y_frame;

    int x_prev = 0;
    int y_prev = 0;
    // do render preview
    boolean do_render_prev = false;

    private int smallify_button = -1;
    private int smallify_button_renders = 3;

    public int cam_x;
    public int cam_y;

    public TextUI textUI = null;

    public class KeyboardInput implements KeyboardListener {

        private final ConcurrentLinkedQueue<String> keysInQueue = new ConcurrentLinkedQueue<>();
        private final ConcurrentLinkedQueue<String> chatMSGS = new ConcurrentLinkedQueue<>();
        private String currentMSG = "";
        private boolean isTypingChatMessage = false;
        private final ReentrantLock lock = new ReentrantLock();

        public String getCurrentMSG() {
            return currentMSG;
        }

        public boolean isTypingChatMessage() {
            return isTypingChatMessage;
        }

        public ConcurrentLinkedQueue<String> getChatMSGS() {
            return chatMSGS;
        }

        private boolean isDisabled = false;

        public void disable(){
            isDisabled = true;
        }

        public void enable(){
            isDisabled = false;
        }

        public synchronized ConcurrentLinkedQueue<String> getQueue() {
            lock.lock();
            try {
                return keysInQueue;
            } finally {
                lock.unlock();
            }
        }

        @Override
        public void keyPressed(KeyPress key, int glfw_scancode, int glfw_modifiers) {
            lock.lock();

            if (isDisabled) {
                lock.unlock();
                return;
            }

            if (!focus_desktop_pane) {
                if (!isTypingChatMessage) {
                    if (/*e.getKeyCode() == KeyboardBindMapper.move_up*/key == KeyPress.KEY_UP) {
                        keysInQueue.add("UP");
                    } else if (key == KeyPress.KEY_DOWN/*e.getKeyCode() == KeyboardBindMapper.move_down*/) {
                        keysInQueue.add("DOWN");
                    } else if (key == KeyPress.KEY_LEFT/*e.getKeyCode() == KeyboardBindMapper.move_left*/) {
                        keysInQueue.add("LEFT");
                    } else if (key == KeyPress.KEY_RIGHT/*e.getKeyCode() == KeyboardBindMapper.move_right*/) {
                        keysInQueue.add("RIGHT");
                    } else if (key == KeyPress.KEY_ENTER/*e.getKeyCode() == KeyboardBindMapper.confirm*/) {
                        keysInQueue.add("ENTER");
                    } else if (key == KeyPress.KEY_ESCAPE/*e.getKeyCode() == KeyboardBindMapper.escape*/) {
                        keysInQueue.add("ESCAPE");
                    } else if (key == KeyPress.KEY_SPACE/*e.getKeyCode() == KeyboardBindMapper.finish_turn*/) {
                        keysInQueue.add("SPACE");
                        InGameUI.this.m.mouseMoved(lm_x, lm_y);
                    } else if (key == KeyPress.KEY_LEFT_SHIFT) {
                        keysInQueue.add("SHIFT");
                    } else if (key == KeyPress.KEY_SLASH) {
                        isTypingChatMessage = true;
                    } else if (key == KeyPress.KEY_F1) {
                        keysInQueue.add("TAB");
                    } else if (key == KeyPress.KEY_F10 && !isTypingChatMessage) {
                        spawnEscapeMenu();
                    } else if (key == KeyPress.KEY_PAGE_UP){
                        if (camera_speed * 2 != 8) {
                            camera_speed *= 2;
                        }
                    } else if (key == KeyPress.KEY_PAGE_DOWN){
                        if (camera_speed != 1) {
                            camera_speed /= 2;
                        }
                    } else if (key == KeyPress.KEY_F3){
                        unsafeLock.lock();
                        dbg_text = !dbg_text;
                        unsafeLock.unlock();
                    }
                } else {
                    if (key == KeyPress.KEY_ESCAPE) {
                        currentMSG = "";
                        isTypingChatMessage = false;
                    } else if (key == KeyPress.KEY_ENTER) {
                        isTypingChatMessage = false;
                        if (!currentMSG.isEmpty())
                            chatMSGS.add(currentMSG);
                        currentMSG = "";
                    } else if (key == KeyPress.KEY_BACKSPACE) {
                        if (currentMSG.length() - 1 >= 0)
                            currentMSG = currentMSG.substring(0, currentMSG.length() - 1);
                    } /*else if (e.isControlDown() && e.getKeyCode() == KeyEvent.VK_V){
                        try {
                            currentMSG += Toolkit.getDefaultToolkit().getSystemClipboard().getData(DataFlavor.stringFlavor);
                        } catch (UnsupportedFlavorException | IOException ex) {
                            ex.printStackTrace();
                        }
                    }*/ //fixme unimplemented
                }
            }
            lock.unlock();
        }

        @Override
        public void keyReleased(KeyPress keyPress, int glfw_scancode, int glfw_modifiers) {

        }

        @Override
        public void keyPressedRepeat(KeyPress key, int glfw_scancode, int glfw_modifiers) {
            if (isDisabled)
                return;

            if (isTypingChatMessage){
                if (key == KeyPress.KEY_BACKSPACE)
                    if (currentMSG.length() - 1 >= 0)
                        currentMSG = currentMSG.substring(0, currentMSG.length() - 1);
            }
        }

        @Override
        public void charTyped(int codepoint) {
            if (isDisabled)
                return;

            if (!isTypingChatMessage) {
                if ("qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM".contains(Utils.unicodeCodepointToString(codepoint))) {
                    keysInQueue.add(Utils.unicodeCodepointToString(codepoint));
                    if (binds != null) {
                        int btn_idx = 0;
                        for (String bind : binds) {
                            if (Objects.equals(bind != null ? bind.toLowerCase(Locale.ROOT) : "", Utils.unicodeCodepointToString(codepoint).toLowerCase(Locale.ROOT))) {
                                smallify_button = btn_idx;
                                smallify_button_renders = 3;
                            }
                            btn_idx++;
                        }
                    }
                }
            }
            else{
                if (" qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM<>=-()[]{}\"';:.,1234567890@#$%^&*/\\?_".contains(Utils.unicodeCodepointToString(codepoint))) {
                    currentMSG += Utils.unicodeCodepointToString(codepoint);
                }
            }
        }
    }

    public static class ClientDataManager {
        private final InGameUI interface_;
        private final BufferedImage fog_of_war;
        private final ClientMapLoader mapLoader = new ClientMapLoader();
        private final BufferedImage skull;
        private final BufferedImage shovel;
        private final BufferedImage motorized;
        private final BufferedImage plane;

        private final HashMap<String, BufferedImage> entity_scaled_cache = new HashMap<>();
        private final HashMap<String, BufferedImage> map_scaled_cache = new HashMap<>();
        private final HashMap<String, BufferedImage> button_scaled_cache = new HashMap<>();
        private final HashMap<BufferedImage, BufferedImage> queue_scaled_cache = new HashMap<>();
        private final HashMap<BufferedImage, BufferedImage> big_unit_texture_cache = new HashMap<>();

        private final HashMap<Image, BufferedImage> entity_cache = new HashMap<>();

        // true selector x
        private int tsel_x;
        // true selector y
        private int tsel_y;

        static class CachedBufferedImage{
            public BufferedImage image;

            public CachedBufferedImage(BufferedImage i){
                image = i;
            }
        }

        public ClientMapLoader getMapLoader(){
            return mapLoader;
        }

        private final HashMap<Integer, CachedBufferedImage> cachedUnitDataArImgs = new HashMap<>();

        /*
        {
            "type" : "RenderRequest",

            "x" : 0,
            "y" : 0,

            "map_path" : path,

            "sel_x" : 0,
            "sel_y" : 0,

            Those are in packet in order to teleport camera to player's first base
            c = cam
            s = selector
            "cbas_x" : 0,
            "cbas_y" : 0,
            "sbas_x" : 0,
            "sbas_y" : 0,

            "entity_data" : [
                {
                    "type" : "unit" | "entity",
                    "rgb" : int,
                    "stats" : [...],
                    "transparent" : bool,
                    "texture" : str
                    "flip" : bool
                    "x" : int,
                    "y" : int
                }

            ]
            "minimap_data" : {
                "fog_fo_war" : [[]], 1/0 2d array
                "units" : [[[0, 0, 0]]], Unit's team rgb
                "tiles" : [[true, true]], Whether tile is passable or not
            }

            "selected_unit_data" : { <- If 0 it means that there is no unit selected
                   "buttons" : [ 0, 0, 0, {"id" : "id_here" , "texture" : "texture_here", "mode" : 1 -> StandardButton, "bind" : "A"}, ... ,
                                                                                                       2 -> SelectorButton
                   "queue" : [0, {"texture" : "", "turn_time" : 0}],

                   NOTE: If buttons are -1 it means unit doesn't have buttons
                   "properties" : <Properties>
                   "texture" : "texture_here.png"

                   "iselectorbutton_press" : true,
                   "iselectorbutton_data_selector_texture" : "texture_here",

                   NOTE: -1 means hidden statistic
            }

            "chat" : []
        }


         */

        public ClientDataManager(InGameUI interface_) {
            fog_of_war = new BufferedImage(64, 64, BufferedImage.TYPE_INT_ARGB);
            Graphics2D graphics2D = fog_of_war.createGraphics();
            graphics2D.setColor(new Color(0,0,0, 0.5F));
            graphics2D.fillRect(0, 0, 64, 64);
            graphics2D.dispose();
            this.interface_ = interface_;

            shovel = AssetManager.loadImage("assets/shovel.png");
            skull = AssetManager.loadImage("assets/skull.png");
            motorized = AssetManager.loadImage("assets/motorized.png");
            plane = AssetManager.loadImage("assets/plane.png");
        }

        private BufferedImage createARGBClone(Image image){
            BufferedImage clone = new BufferedImage(32, 32, BufferedImage.TYPE_INT_ARGB_PRE);
            Graphics2D graphics = clone.createGraphics();

            graphics.setBackground(new Color(0x00FFFFFF, true));
            graphics.clearRect(0, 0, clone.getWidth(), clone.getHeight());

            graphics.drawImage(image, 0, 0, null);

            graphics.dispose();

            return clone;
        }

        public BufferedImage unit2image(ClientGame.ClientUnit e){
            BufferedImage image;

            String cached = e.getTextureLoc();

            if (e.isFlip()){
                cached = cached+"+flip";
            }

            if (SpecialTeamReprReg.getTeamRepr(e.getTextureLoc()) != null){
                cached = cached+"+rgb"+e.getTeamColor();
            }

            if (e.isTransparent()){
                cached = cached+"+transparent";
            }

            if (e.hasTurnEnded() && e.getAnimationState().equals("idle")){
                cached = cached+"+turnended";
            }

            if (entity_scaled_cache.containsKey(cached)){
                return entity_scaled_cache.get(cached);
            }
            else {
                if (SpecialTeamReprReg.getTeamRepr(e.getTextureLoc()) != null) {
                    image = new BufferedImage(e.getTexture().getWidth(), e.getTexture().getHeight(), e.getTexture().getType());
                    Graphics g = image.getGraphics();
                    g.drawImage(e.getTexture(), 0, 0, null);
                    g.dispose();

                    SpecialTeamReprReg.TeamRepr teamRepr = SpecialTeamReprReg.getTeamRepr(e.getTextureLoc());
                    for (int[] c : teamRepr.getCoords()) {
                        image.setRGB(c[0], c[1], e.getTeamColor().getRGB());
                    }
                }
                else {
                    image = e.getTexture();
                }

                if (e.isFlip()) {
                    // Flip the image
                    AffineTransform tx = AffineTransform.getScaleInstance(-1, 1);
                    tx.translate(-image.getWidth(), 0);
                    AffineTransformOp op = new AffineTransformOp(tx, AffineTransformOp.TYPE_NEAREST_NEIGHBOR);
                    image = op.filter(image, null);
                }

                if (e.isTransparent()) {
                    BufferedImage clone = createARGBClone(image);

                    for (int y_ = 0; y_ < 32; y_++) {
                        for (int x_ = 0; x_ < 32; x_++) {

                            Color original = new Color((new Color(clone.getRGB(x_, y_))).getRed(),
                                    (new Color(clone.getRGB(x_, y_))).getGreen(),
                                    (new Color(clone.getRGB(x_, y_))).getBlue(),
                                    (new Color(clone.getRGB(x_, y_), true)).getAlpha());

                            clone.setRGB(x_, y_, (new Color(original.getRed(),
                                    original.getGreen(),
                                    original.getBlue(),
                                    (int) (original.getAlpha() * 0.5)).getRGB()));

                        }
                    }

                    image = clone;
                }

                if (e.hasTurnEnded() && e.getAnimationState().equals("idle")){
                    BufferedImage clone = createARGBClone(image);

                    for (int y_ = 0; y_ < 32; y_++) {
                        for (int x_ = 0; x_ < 32; x_++) {

                            Color original = new Color((new Color(clone.getRGB(x_, y_))).getRed(),
                                    (new Color(clone.getRGB(x_, y_))).getGreen(),
                                    (new Color(clone.getRGB(x_, y_))).getBlue(),
                                    (new Color(clone.getRGB(x_, y_), true)).getAlpha());

                            int r = (int) Math.floor(original.getRed()*0.9);
                            int g = (int) Math.floor(original.getRed()*0.9);
                            int b = (int) Math.floor(original.getRed()*0.9);

                            clone.setRGB(x_, y_, (new Color(r < 256 ? r: 255,
                                    g < 256 ? r: 255,
                                    b < 256 ? r: 255,
                                    original.getAlpha())).getRGB());

                        }
                    }

                    image = clone;

                }

                Image img_d = image.getScaledInstance(64, 64, Image.SCALE_FAST);

                BufferedImage bufferedImage = new BufferedImage(64, 64, BufferedImage.TYPE_INT_ARGB);
                Graphics g = bufferedImage.getGraphics();
                g.drawImage(img_d, 0, 0, null);
                g.dispose();

                entity_scaled_cache.put(cached, bufferedImage);
                return bufferedImage;
            }
        }

        public void updateData(ArrayList<String> chat, ClientGame.ClientEntity[][] _entities,
                               boolean[][] fog_of_war, boolean[][] selr, ClientGame.ClientSelectedUnit selectedUnit,
                               float steel, float aluminium,
                               boolean is_it_your_turn, ClientMapLoader clMapLoader, BufferedImage minimap, Color teamDoingTurnColor,
                               String teamDoingTurnNick, HashMap<String, String> gamemodeProperties, float manpower) {
            if (!clMapLoader.isMapLoaded()){
                return;
            }
            try {
                this.interface_.unsafeLock.lock();

                this.interface_.hasFirstPackedBeenReceived = true;

                this.interface_.minimap = minimap;

                if (!mapLoader.isMapLoaded()){
                    mapLoader.loadFromOther(clMapLoader);
                }

                ArrayList<ArrayList<String>> map_textures = mapLoader.getMap(this.interface_.cam_x, this.interface_.cam_y, this.interface_.scale, (interface_.tr_width-832)/64, (interface_.tr_height-576)/64);

                ArrayList<BufferedImage> buttons = new ArrayList<>();
                ArrayList<String> binds = new ArrayList<>();
                int[][] highlights = new int[9][];

                BufferedImage selected_unit_image = null;
                PropertiesToRender propertiesToRender = null;
                BufferedImage selector = null;

                ArrayList<BufferedImage> unit_queue = null;
                ArrayList<String> unit_queue_turn_time = new ArrayList<>();

                String[][] button_ids = {{null, null, null}, {null, null, null}, {null, null, null}};

                if (selectedUnit != null) {
                    selected_unit_image = selectedUnit.getTexture();
                    propertiesToRender = new PropertiesToRender(selectedUnit.getProperties());
                    selector = selectedUnit.getISelectorButtonPress();

                    // Deal with unit queue

                    if (selectedUnit.getQueue() != null) {
                        unit_queue = new ArrayList<>();
                        for (ClientGame.ClientSelectedUnit.ClientUnitQueueElement queueElement : selectedUnit.getQueue()) {
                            if (queue_scaled_cache.containsKey(queueElement.getTexture()))
                                unit_queue.add(queue_scaled_cache.get(queueElement.getTexture()));
                            else{
                                Image f = queueElement.getTexture().getScaledInstance(64, 64, Image.SCALE_FAST);
                                BufferedImage bufferedImage = new BufferedImage(64, 64, BufferedImage.TYPE_INT_ARGB);
                                Graphics g = bufferedImage.createGraphics();
                                g.drawImage(f, 0,0, null);
                                g.dispose();
                                queue_scaled_cache.put(queueElement.getTexture(), bufferedImage);
                                unit_queue.add(bufferedImage);

                            }
                            unit_queue_turn_time.add(String.valueOf(queueElement.getTurn_time()));
                        }
                    }

                    // Deal with the buttons

                    int by = 0;
                    int bx = 0;
                    int bi = 0;
                    for (ClientGame.ClientSelectedUnit.ClientButton button : selectedUnit.getButtons()) {
                        if (bx == 3) {
                            bx = 0;
                            by++;
                        }

                        if (button != null) {
                            button_ids[by][bx] = button.getId();

                            if (button_scaled_cache.containsKey(button.getId())){
                                buttons.add(button_scaled_cache.get(button.getId()));
                            }
                            else{
                                Image f = button.getImage().getScaledInstance(64,64,Image.SCALE_FAST);
                                BufferedImage bufferedImage = new BufferedImage(64, 64, BufferedImage.TYPE_INT_ARGB);
                                Graphics g = bufferedImage.createGraphics();
                                g.drawImage(f, 0,0, null);
                                g.dispose();
                                button_scaled_cache.put(button.getId(), bufferedImage);
                                buttons.add(bufferedImage);
                            }

                            if (button.isHighlightEnabled()){
                                highlights[bi] = button.getHighlight();
                            }

                            binds.add(button.getBind());
                        } else {
                            buttons.add(null);
                            binds.add(null);
                        }

                        bx++;
                        bi++;
                    }

                    // Buffs

                    if (selectedUnit.getBuffsTextures() != null){
                        interface_.buffsTextures = selectedUnit.getBuffsTextures();
                        interface_.buffsNames = selectedUnit.getBuffNames();
                        interface_.buffsTimes = selectedUnit.getBuffTimes();
                    }
                    else{
                        interface_.buffsTextures = null;
                        interface_.buffsNames = null;
                        interface_.buffsTimes = null;
                    }

                }

                if (selector == null)
                    selector = AssetManager.loadImage("assets/ui/selectors/ui_select.png");

                // deal with map
                ArrayList<ArrayList<BufferedImage>> map = new ArrayList<>();
                int y = 0;
                for (ArrayList<String> x_row : map_textures) {
                    map.add(new ArrayList<>());
                    for (String texture : x_row) {
                        if (texture == null) {
                            map.get(y).add(null);
                        }
                        else {
                            if (map_scaled_cache.containsKey(texture)){
                                map.get(y).add(map_scaled_cache.get(texture));
                            }
                            else{
                                Image f = AssetManager.loadImage(texture).getScaledInstance(64, 64, Image.SCALE_FAST);

                                BufferedImage bufferedImage = new BufferedImage(64, 64, BufferedImage.TYPE_INT_ARGB);
                                Graphics g = bufferedImage.createGraphics();
                                g.drawImage(f, 0, 0, null);
                                g.dispose();

                                map_scaled_cache.put(texture, bufferedImage);
                                map.get(y).add(bufferedImage);
                            }

                        }
                    }
                    y++;
                }

                // Deal with entities
                ArrayList<ArrayList<BufferedImage>> entities = new ArrayList<>();
                y = 0;
                for (ClientGame.ClientEntity[] x_row : _entities) {
                    entities.add(new ArrayList<>());
                    for (ClientGame.ClientEntity centity: x_row) {
                        if (centity == null) {
                            entities.get(y).add(null);
                        } else {
                            if (centity instanceof ClientGame.ClientUnit ucentity) {
                                BufferedImage image = unit2image(ucentity);

                                entities.get(y).add(image);
                            } else {
                                if (!entity_cache.containsKey(centity.getTexture())) {
                                    Image image = centity.getTexture().getScaledInstance(64, 64, Image.SCALE_FAST);

                                    BufferedImage bufferedImage = new BufferedImage(64, 64, BufferedImage.TYPE_INT_ARGB);
                                    Graphics g = bufferedImage.createGraphics();
                                    g.drawImage(image, 0, 0, null);
                                    g.dispose();
                                    entity_cache.put(centity.getTexture(), bufferedImage);

                                    entities.get(y).add(bufferedImage);
                                }
                                else{
                                    entities.get(y).add(entity_cache.get(centity.getTexture()));
                                }
                            }
                        }

                    }
                    y++;
                }

                // Fog of war
                ArrayList<ArrayList<BufferedImage>> fog_of_war_ = new ArrayList<>();
                y = 0;
                for (boolean[] x_row : fog_of_war) {
                    fog_of_war_.add(new ArrayList<>());
                    for (boolean texture : x_row) {
                        if (!texture) {
                            fog_of_war_.get(y).add(this.fog_of_war);
                        } else {
                            fog_of_war_.get(y).add(null);
                        }
                    }
                    y++;
                }

                // sel_r

                BufferedImage slr_img = AssetManager.loadImage("selr");

                ArrayList<ArrayList<BufferedImage>> sel_r = new ArrayList<>();
                y = 0;
                for (boolean[] x_row : selr) {
                    sel_r.add(new ArrayList<>());
                    for (boolean texture : x_row) {
                        if (texture) {
                            sel_r.get(y).add(slr_img);
                        } else {
                            sel_r.get(y).add(null);
                        }
                    }
                    y++;
                }

                // Create those team showing thingies
                ArrayList<ArrayList<BufferedImage>> unit_data_ar = new ArrayList<>();
                y = 0;
                for (ClientGame.ClientEntity[] row : _entities){
                    unit_data_ar.add(new ArrayList<>());
                    for (ClientGame.ClientEntity centity: row) {
                        if (centity instanceof ClientGame.ClientUnit ucentity) {

                            // Calculate hash
                            Color rgb_team = ucentity.getTeamColor();
                            boolean has_unit_su_enabled = ucentity.getSu() != -1 && ucentity.getMaxSu() != -1 && ucentity.getSu() != 0 && ucentity.getMaxSu() != 0;
                            Object[] hash_ar = new Object[]{rgb_team,
                                    ((float) ucentity.getHp()) / ((float) ucentity.getMaxHp()),
                                    (has_unit_su_enabled) ? ((float) ucentity.getSu()) / ((float) ucentity.getMaxSu()) : -1,
                                    0, ucentity.getOther(), ucentity.getOtherTeamRGB()};

                            // takes indicators into account
                            if (ucentity.getSu() < ucentity.getMaxSu() * 0.4 && has_unit_su_enabled) {
                                hash_ar[3] = -1;
                            } else if (ucentity.getEt() > 0 && !(ucentity.getSu() < ucentity.getMaxSu() * 0.4 && has_unit_su_enabled)) {
                                hash_ar[3] = ucentity.getEt();
                            }

                            if (!cachedUnitDataArImgs.containsKey(Arrays.hashCode(hash_ar))) {

                                BufferedImage clone = new BufferedImage(32, 32, BufferedImage.TYPE_INT_ARGB_PRE);
                                Graphics2D graphics = clone.createGraphics();

                                graphics.setBackground(new Color(0x00FFFFFF, true));
                                graphics.clearRect(0, 0, clone.getWidth(), clone.getHeight());

                                graphics.setColor(rgb_team);

                                graphics.drawLine(23, 0, 31, 0);
                                graphics.drawLine(31, 0, 31, 8);

                                graphics.drawLine(0, 31, 0, 23);
                                graphics.drawLine(8, 31, 0, 31);

                                graphics.setColor(new Color(148, 0, 21));

                                int hp = ucentity.getHp();
                                int max_hp = ucentity.getMaxHp();

                                if (hp != -1 && max_hp != -1 && hp != 0 && max_hp != 0) {
                                    graphics.drawLine(22, 30, 22 + (int) (9f * ((float) (hp) / (float) (max_hp))), 30);
                                }

                                graphics.setColor(Color.YELLOW);
                                int su = ucentity.getSu();
                                int max_su = ucentity.getMaxSu();

                                if (has_unit_su_enabled) {
                                    graphics.drawLine(22, 31, 22 + (int) (9f * ((float) (su) / (float) (max_su))), 31);
                                }

                                graphics.dispose();

                                int et = ucentity.getEt();
                                // Draw shovel
                                BufferedImage shovel_col = new BufferedImage(16, 16, BufferedImage.TYPE_INT_ARGB_PRE);
                                if (et > 0) {
                                    Graphics2D shovel_graphics = shovel_col.createGraphics();
                                    graphics.setBackground(new Color(0x00FFFFFF, true));
                                    shovel_graphics.drawImage(shovel, 0, 0, null);

                                    for (int y_ = 0; y_ < 16; y_++) {
                                        for (int x_ = 0; x_ < 16; x_++) {
                                            if (shovel_col.getRGB(x_, y_) == Color.BLACK.getRGB()) {
                                                shovel_col.setRGB(x_, y_, et == 1 ? Color.RED.getRGB() :
                                                        et == 2 ? Color.YELLOW.getRGB() : et == 3 ? Color.GREEN.getRGB() : 0)
                                                ;
                                            }
                                        }
                                    }
                                    shovel_graphics.dispose();
                                }

                                Image scaled = clone.getScaledInstance(64, 64, Image.SCALE_FAST);
                                clone = new BufferedImage(64, 64, BufferedImage.TYPE_INT_ARGB_PRE);
                                graphics = clone.createGraphics();
                                graphics.setBackground(new Color(0x00FFFFFF, true));
                                graphics.drawImage(scaled, 0, 0, null);
                                // Draw skull to indicate that unit is under supplied
                                if (su < max_su * 0.4 && has_unit_su_enabled) {
                                    BufferedImage skull_col = new BufferedImage(16, 16, BufferedImage.TYPE_INT_ARGB_PRE);
                                    Graphics2D skull_graphics = skull_col.createGraphics();
                                    skull_graphics.setBackground(new Color(0x00FFFFFF, true));
                                    skull_graphics.drawImage(skull, 0, 0, null);

                                    for (int y_ = 0; y_ < 16; y_++) {
                                        for (int x_ = 0; x_ < 16; x_++) {
                                            if (skull_col.getRGB(x_, y_) == Color.BLACK.getRGB()) {
                                                skull_col.setRGB(x_, y_, Color.RED.getRGB());
                                            }
                                        }
                                    }
                                    skull_graphics.dispose();
                                    graphics.drawImage(skull_col, 0, 0, null);
                                }

                                if (et > 0 && !(su < max_su * 0.4 && has_unit_su_enabled))
                                    graphics.drawImage(shovel_col, 0, 0, null);

                                // Draw "other"
                                if (ucentity.getOther() != -1){
                                    BufferedImage other = new BufferedImage(16, 16, BufferedImage.TYPE_INT_ARGB_PRE);
                                    Graphics2D other_graphics = other.createGraphics();
                                    other_graphics.setBackground(new Color(0x00FFFFFF, true));
                                    other_graphics.drawImage(ucentity.getOther() == 1 ? plane : motorized, 0, 0, null);

                                    for (int y_ = 0; y_ < 16; y_++) {
                                        for (int x_ = 0; x_ < 16; x_++) {
                                            if (other.getRGB(x_, y_) == Color.BLACK.getRGB()) {
                                                other.setRGB(x_, y_, ucentity.getOtherTeamRGB().getRGB());
                                            }
                                        }
                                    }

                                    graphics.drawImage(other,46, 1, null);
                                }

                                graphics.dispose();

                                unit_data_ar.get(y).add(clone);
                                cachedUnitDataArImgs.put(Arrays.hashCode(hash_ar), new CachedBufferedImage(clone));
                            } else
                                unit_data_ar.get(y).add(cachedUnitDataArImgs.get(Arrays.hashCode(hash_ar)).image);
                        }
                        else {
                            if (centity != null) {
                                if (centity.getOther() != -1) {

                                    // hash for just centity
                                    Object[] hash_ar = new Object[]{0,
                                            0,
                                            -1,
                                            0, centity.getOther(), centity.getOtherTeamRGB()};

                                    if (!cachedUnitDataArImgs.containsKey(Arrays.hashCode(hash_ar))) {

                                        BufferedImage clone = new BufferedImage(64, 64, BufferedImage.TYPE_INT_ARGB_PRE);
                                        Graphics2D graphics = clone.createGraphics();

                                        graphics.setBackground(new Color(0x00FFFFFF, true));
                                        graphics.clearRect(0, 0, clone.getWidth(), clone.getHeight());

                                        BufferedImage other = new BufferedImage(16, 16, BufferedImage.TYPE_INT_ARGB_PRE);
                                        Graphics2D other_graphics = other.createGraphics();
                                        other_graphics.setBackground(new Color(0x00FFFFFF, true));
                                        other_graphics.drawImage(centity.getOther() == 1 ? plane : motorized, 0, 0, null);

                                        for (int y_ = 0; y_ < 16; y_++) {
                                            for (int x_ = 0; x_ < 16; x_++) {
                                                if (other.getRGB(x_, y_) == Color.BLACK.getRGB()) {
                                                    other.setRGB(x_, y_, centity.getOtherTeamRGB().getRGB());
                                                }
                                            }
                                        }
                                        other_graphics.dispose();

                                        graphics.drawImage(other, 46, 1, null);
                                        graphics.dispose();

                                        cachedUnitDataArImgs.put(Arrays.hashCode(hash_ar), new CachedBufferedImage(clone));
                                        unit_data_ar.get(y).add(clone);
                                    } else {
                                        unit_data_ar.get(y).add(cachedUnitDataArImgs.get(Arrays.hashCode(hash_ar)).image);
                                    }
                                } else
                                    unit_data_ar.get(y).add(null);
                            } else
                                unit_data_ar.get(y).add(null);
                        }
                    }
                    y++;
                }

                // if no new sel_x and sel_y have been received in a long time fall back to previous
                setSelectorData(tsel_x, tsel_y);

                this.interface_.unsafeLock.lock();
                this.interface_.map_to_render = map;
                this.interface_.fog_of_war = fog_of_war_;
                this.interface_.entity_render = entities;
                this.interface_.buttons = buttons;
                this.interface_.binds = binds;
                this.interface_.unit_img = selected_unit_image;
                this.interface_.propertiesToRender = propertiesToRender;
                this.interface_.selector = selector;
                this.interface_.unit_data_ar = unit_data_ar;
                this.interface_.queue = unit_queue;
                this.interface_.unit_times = unit_queue_turn_time;
                this.interface_.steel = steel;
                this.interface_.aluminium = aluminium;
                this.interface_.is_it_your_turn = is_it_your_turn;
                this.interface_.id_array = button_ids;
                this.interface_.chat = chat;
                this.interface_._selr = sel_r;
                this.interface_.gamemodeProperties = gamemodeProperties;
                this.interface_.highlights = highlights;
                this.interface_.manpower = manpower;
                if (!teamDoingTurnColor.equals(Color.WHITE))
                    this.interface_.teamDoingTurnColor = teamDoingTurnColor;
                if (!Objects.equals(teamDoingTurnNick, "neutralteam")){
                    this.interface_.teamDoingTurnNick = teamDoingTurnNick;
                }
                this.interface_.unsafeLock.unlock();

                if (cachedUnitDataArImgs.size() > 100){
                    cachedUnitDataArImgs.clear();
                }

                interface_.unsafeLock.unlock();
                return;
            } catch (Exception e) {
                e.printStackTrace();
            }

            interface_.unsafeLock.unlock();
        }

        public void setSelectorData(int sel_x, int sel_y){
            interface_.unsafeLock.lock();
            tsel_x = sel_x;
            tsel_y = sel_y;

            int x_c = tsel_x-interface_.cam_x;
            int y_c = tsel_y-interface_.cam_y;

            int sel_x_frame = interface_.createCorrectXFromXC(x_c);
            int sel_y_frame = interface_.createCorrectYFromYC(y_c);

            this.interface_.sel_x_frame = sel_x_frame;
            this.interface_.sel_y_frame = sel_y_frame;
            interface_.unsafeLock.unlock();
        }

    }

    class MListener implements MouseListener {

        private int last_move_x = 0;
        private int last_move_y = 0;

        private boolean isDisabled = false;

        public void disable(){
            isDisabled = true;
        }

        public void enable(){
            isDisabled = false;
        }

        @Override
        public void mousePressed(MousePress button, int glfw_mods, double _x, double _y) {
            unsafeLock.lock();

            if (isDisabled) {
                unsafeLock.unlock();
                return;
            }

            if (!focus_desktop_pane) {
                if (button == MousePress.MOUSE_BUTTON_1) {
                    int y = 0;
                    int x = 0;
                    int button_idx = 0;
                    for (String bind : binds) {
                        if (_x >= rightPanelX() + (x * 64) && _y >= rightPanelY() + (y * 64)
                                && _x <= rightPanelX() + 64 + (x * 64) && _y <= rightPanelY() + 64 + (y * 64)) {
                            if (bind != null) {
                                InGameUI.this.smallify_button = button_idx;
                                InGameUI.this.smallify_button_renders = 3;
                                in_client.keysInQueue.add(bind);
                                break;
                            }
                        }
                        x++;
                        if (x == 3) {
                            x = 0;
                            y++;
                        }
                        button_idx++;
                    }

                    // Move selector
                    //if (offset_direction == OffsetDirection.None) {
                        if ((_x >= 0 && _x <= tr_width) &&
                                (_y >= 64 && _y <= rightPanelY())) {

                            int x_c = calculateXOnCamera((int) _x);
                            int y_c = calculateYOnCamera((int) _y);

                            int right_mv = clDataManager.tsel_x-(cam_x+x_c);
                            int down_mv = clDataManager.tsel_y-(cam_y+y_c);

                            for (int m1 = 0; m1 < Math.abs(right_mv); m1++) {
                                if (right_mv > 0) {
                                    in_client.keysInQueue.add("LEFT");
                                } else if (right_mv < 0) {
                                    in_client.keysInQueue.add("RIGHT");
                                }
                            }

                            for (int m2 = 0; m2 < Math.abs(down_mv); m2++) {
                                if (down_mv > 0) {
                                    in_client.keysInQueue.add("UP");
                                } else if (down_mv < 0) {
                                    in_client.keysInQueue.add("DOWN");
                                }
                            }

                            in_client.keysInQueue.add("ENTER");
                        }
                    //}
                }
                else if (button == MousePress.MOUSE_BUTTON_2) {
                    in_client.keysInQueue.add("ESCAPE");
                }
                else if (button == MousePress.MOUSE_BUTTON_3){
                    isCamMovementInProgress = true;
                    origin_m_x = (int) _x;
                    origin_m_y = (int) _y;
                }
                else {
                    in_client.keysInQueue.add("ENTER");
                }
            }
            unsafeLock.unlock();
        }

        public int calculateXOnCamera(int x_m){
            return (int)(Math.ceil(Math.ceil((x_m+Math.ceil(offset_x/scale))/Math.ceil(64/scale))))-1;
        }

        public int calculateYOnCamera(int y_m){
            return (int)(Math.ceil(Math.ceil((y_m-64+Math.ceil(offset_y/scale))/Math.ceil(64/scale))))-1;
        }

        @Override
        public void mouseMoved(double _x, double _y) {
            unsafeLock.lock();

            if (isCamMovementInProgress){

                int x_diff = (int) (_x - origin_m_x);
                int y_diff = (int) (_y - origin_m_y);

                origin_m_y = (int) _y;
                origin_m_x = (int) _x;

                offset_x -= x_diff*camera_speed;
                offset_y -= y_diff*camera_speed;

                if (offset_x <= -64) {
                    if (cam_x + 5 >= 0) {
                        cam_x--;
                        offset_x = 0;
                    }
                }

                if (offset_x >= 64) {
                    if (cam_x + 7 < clDataManager.getMapLoader().getWidth()) {
                        cam_x++;
                        offset_x = 0;
                    }
                }

                if (offset_y <= -64){
                    if (cam_y + 1 >= 0) {
                        cam_y--;
                        offset_y = 0;
                    }
                }

                if (offset_y >= 64){
                    if (cam_y + 3 < clDataManager.getMapLoader().getHeight()) {
                        cam_y++;
                        offset_y = 0;
                    }
                }
            }

            lm_x = (int) _x;
            lm_y = (int) _y;

            if (isDisabled) {
                unsafeLock.unlock();
                return;
            }
            cur_highlight = null;
            last_move_x = (int) _x;
            last_move_y = (int) _y;
            if (!focus_desktop_pane) {
                boolean clearTooltip = false;
                int y = 0;
                for (String[] row : id_array) {
                    int x = 0;
                    for (String point : row) {
                        if (_x >= rightPanelX() + (x * 64) && _y >= rightPanelY() + (y * 64)
                                && _x <= rightPanelX() + 64 + (x * 64) && _y <= rightPanelY() + 64 + (y * 64)) {
                            if (ButtonTooltips.getTooltip(point) != null)
                                InGameUI.this.setTooltipText(ButtonTooltips.getTooltip(point));
                            else
                                InGameUI.this.setTooltipText(null);
                            clearTooltip = true;
                        }
                        x++;
                    }
                    y++;
                }


                if (buffsNames != null)
                    if (!buffsNames.isEmpty()) {
                        boolean setTooltip = false;
                        int off = 0;
                        for (int bidx = 0; bidx < buffsNames.size(); bidx++) {
                            if (transitionX() + 32 + off <= _x && _x < transitionX() + 32 + off + 16) {
                                if (rightPanelY() + 43 + 13 * 11 - uname_monogram.getHeight() - 16 <= _y
                                        && _y <= rightPanelY() + 43 + 13 * 11 - uname_monogram.getHeight()) {
                                    InGameUI.this.setTooltipText(buffsNames.get(bidx) + "\nTurn time: "
                                            + buffsTimes.get(bidx));
                                    setTooltip = true;
                                }
                            }
                            off += 16;
                        }
                        if (!clearTooltip)
                            clearTooltip = setTooltip;
                    }

                if (!isCamMovementInProgress) {
                    if ((((64 < _y) && (_y < rightPanelY())) && ((0 < _x) && (_x < tr_width))) || (((rightPanelX() < _x) && (_x < tr_width)) && ((rightPanelY() < _y) && (_y < tr_height)))) {
                        if (((64 < _y) && (_y < rightPanelY())) && ((0 < _x) && (_x < tr_width))) {
                            int x_c = calculateXOnCamera((int) _x);
                            int y_c = calculateYOnCamera((int) _y);

                            pr_x = x_c + cam_x;
                            pr_y = y_c + cam_y;

                            if (x_c + cam_x >= 0 && y_c + cam_y >= 0 && clDataManager.getMapLoader() != null) {
                                if (clDataManager.getMapLoader().isMapLoaded())
                                    if (clDataManager.getMapLoader().getWidth() > x_c + cam_x
                                            && clDataManager.getMapLoader().getHeight() > y_c + cam_y) {
                                        InGameUI.this.x_prev = createCorrectXFromXC(x_c);
                                        InGameUI.this.y_prev = createCorrectYFromYC(y_c);

                                        InGameUI.this.do_render_prev = true;
                                        InGameUI.this.do_offset = true;
                                    } else {
                                        InGameUI.this.do_render_prev = false;
                                    }
                            } else {
                                InGameUI.this.do_render_prev = false;
                            }
                        } else {
                            InGameUI.this.x_prev = (int) ((_x - rightPanelX()) / 64);
                            InGameUI.this.y_prev = (int) ((_y - rightPanelY()) / 64);
                            InGameUI.this.do_render_prev = true;
                            InGameUI.this.do_offset = false;

                            int hxb = (int) ((_x - rightPanelX()) / 64);
                            int hyb = (int) ((_y - rightPanelY()) / 64);
                            int hidx = (hyb * 3) + hxb;

                            if (hidx < 9) {
                                if (highlights[hidx] != null) {
                                    cur_highlight = new int[]{
                                            createCorrectXFromXC(highlights[hidx][0] - cam_x),
                                            createCorrectYFromYC(highlights[hidx][1] - cam_y),
                                            highlights[hidx][2]
                                    };
                                } else {
                                    cur_highlight = null;
                                }
                            }
                        }
                    }
                    else {
                        InGameUI.this.do_render_prev = false;
                    }
                } else {
                    InGameUI.this.do_render_prev = false;
                }

                if (!clearTooltip) {
                    InGameUI.this.setTooltipText(null);
                }
            }
            unsafeLock.unlock();
        }

        @Override
        public void mouseReleased(MousePress button, int glfw_mods, double x, double y) {
            if (isCamMovementInProgress){
                isCamMovementInProgress = false;
            }
        }

        @Override
        public void mouseWheelMoved(double xoffset, double yoffset) {
            unsafeLock.lock();

            if (isDisabled)
            {
                unsafeLock.unlock();
                return;
            }

            if (yoffset == -1){
                scale -= 0.1f;
                if (scale < 0.5){
                    scale = 0.5f;
                }
            } else{
                scale += 0.1f;
                if (scale > 1.42){
                    scale = 1.4f;
                }
            }

            // trick to fix prev selector when changing scale
            mouseMoved(last_move_x, last_move_y);
            unsafeLock.unlock();
        }
    }

    /***
     * @param x_c x on camera space
     */
    public int createCorrectXFromXC(int x_c) {
        return (x_c * (int) Math.ceil(64 / scale)) + (int) Math.ceil(-offset_x / scale);
    }

    /***
     * @param y_c y on camera space
     */
    public int createCorrectYFromYC(int y_c) {
        return (y_c * (int) Math.ceil(64 / scale)) + (int) Math.ceil(-(offset_y) / scale);
    }

    private final WindowResizeListener resizeListener = (nwidth, nheight) -> {
        tr_width = nwidth;
        tr_height = nheight;
        escapeMenuOff = new Vector2i((nwidth-150)/2, (nheight-100)/2);
        int offy = 0;
        for (STGLComponent comp: escapeMenuComponents){
            setXY(comp, escapeMenuOff.x, escapeMenuOff.y+offy);
            offy+=25;
            if (offy == 25){
                offy+=25;
            }
        }
    };

    public InGameUI( MainFrame frame){
        this.frame = frame;
        ButtonTooltips.init();
        PropertiesMatcher.loadPropertyMatcher();
        clDataManager = new ClientDataManager(this);
        frame.addWindowResizeListener(resizeListener);

        tr_width = frame.getWidth();
        tr_height = frame.getHeight();

        this.m = new MListener();

        in_client = new KeyboardInput();

        monogram = fontManager.createFont("font/monogram-extended.ttf", 14, false, false);
        monogram_32 = fontManager.createFont("font/monogram-extended.ttf", 32, false, false);
        monogram_32_bold = fontManager.createFont("font/monogram-extended.ttf", 32, true, false);
        monogram_19 = fontManager.createFont("font/monogram-extended.ttf", 19, false, false);
        monogram_18 = fontManager.createFont("font/monogram-extended.ttf", 18, false, false);
        monogram_15 = fontManager.createFont("font/monogram-extended.ttf", 15, false, false);
        monogram_23 = fontManager.createFont("font/monogram-extended.ttf", 23, false, false);
        monogram_button = fontManager.createFont("font/monogram-extended.ttf", 16, false, false);
        monogram_button_small = fontManager.createFont("font/monogram-extended.ttf", 15, false, false);
        uname_monogram = fontManager.createFont("font/monogram-extended.ttf", 16, false, false);

        placeholder_ui = AssetManager.loadImage("assets/placeholder_ui.png");
        placeholder_ui_2 = AssetManager.loadImage("assets/placeholder_ui_2.png");
        panel = new Texture(AssetManager.loadImage("assets/panel.png"));
        selector = AssetManager.loadImage("assets/ui/selectors/ui_select.png");
        steel_icon = new Texture(AssetManager.loadImage("assets/steel_icon.png"));
        aluminium_icon = new Texture(AssetManager.loadImage("assets/aluminium_icon.png"));

        ttextUI = new Texture(AssetManager.loadImage("assets/text_ui.png"));
        //military_points = military_points.getScaledInstance(18, 18, Image.SCALE_SMOOTH);

        minimap_t = new Texture(30, 30, new Texture.TextureOptions().setMinFiltering(Texture.Filtering.NEAREST).setMagFiltering(Texture.Filtering.NEAREST));

        manpower_img = new Texture(AssetManager.loadImage("assets/manpower_indicator.png"));
        bhighlight = new Texture(AssetManager.loadImage("bhighlight"));

        try {
            information_logo = new STGLImage(TextureManager.loadBufferedImage("assets/information_logo.png"), new Texture.TextureOptions().setMinFiltering(Texture.Filtering.NEAREST).setMagFiltering(Texture.Filtering.NEAREST));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        // escape menu
        escapeMenuCompManager = new ComponentManager(frame);
        escapeMenuCompManager.enableDirectDrawing();

        escapeMenuOff = new Vector2i((frame.getWidth()-150)/2, (frame.getHeight()-100)/2);

        STGLButton returnToGame = new STGLButton(150, 25, escapeMenuOff.x, escapeMenuOff.y, "Return to game");
        returnToGame.setFont(monogram);
        returnToGame.setActionListener(() -> closeEscapeMenu = true);
        StalemateStyle.makeButton(returnToGame);
        escapeMenuComponents.add(returnToGame);

        STGLButton disconnect = new STGLButton(150, 25, escapeMenuOff.x, escapeMenuOff.y+50, "Disconnect");
        //ButtonHover disconnect = new ButtonHover("Disconnect");
        disconnect.setActionListener(()-> status = 1);
        disconnect.setFont(monogram);
        StalemateStyle.makeButton(disconnect);
        escapeMenuComponents.add(disconnect);

        STGLButton exit = new STGLButton(150, 25, escapeMenuOff.x, escapeMenuOff.y+75,"Exit");
        exit.setFont(monogram);
        exit.setActionListener(() -> {
            System.exit(0);
            frame.dispose();
        });
        StalemateStyle.makeButton(exit);
        escapeMenuComponents.add(exit);

        escapeMenuCompManager.addComponent(returnToGame);
        escapeMenuCompManager.addComponent(disconnect);
        escapeMenuCompManager.addComponent(exit);

        for (STGLComponent component: escapeMenuComponents){
            component.disable();
        }

        frame.addMouseListener(m);
        frame.addKeyboardListener(in_client);
    }

    public void clFrame(){
        unsafeLock.lock();
        for (STGLComponent component: escapeMenuComponents){
            component.disable();
        }
        frame.removeWindowResizeListener(resizeListener);
        unsafeLock.unlock();
    }
    private volatile boolean isEscapeMenuActive = false;

    private volatile boolean closeEscapeMenu = false;
    private int status = 0;

    public void spawnEscapeMenu(){
        unsafeLock.lock();
        isEscapeMenuActive = true;
        closeEscapeMenu = false;
        for (STGLComponent component: escapeMenuComponents){
            component.enable();
        }

        in_client.disable();
        m.disable();

        unsafeLock.unlock();
    }
    public volatile boolean showResults = false;
    private String result = null;

    public void setResults(String res){
        unsafeLock.lock();
        showResults = true;
        result = res;
        m.disable();
        in_client.disable();

        int x_m = (tr_width - 300) / 2;
        int y_m = (tr_height - 300) / 2;

        // reuse escape menu comp manager
        STGLButton ok = new STGLButton(150, 30, x_m+(300-150)/2, y_m+270, "OK");
        ok.setFont(monogram);
        StalemateStyle.makeButton(ok);
        escapeMenuCompManager.addComponent(ok);
        ok.setActionListener(() -> status = 1);

        unsafeLock.unlock();
    }

    public void inGameUIUpdate(){
        unsafeLock.lock();

        if (closeEscapeMenu){
            closeEscapeMenu();
        }

        unsafeLock.unlock();
    }

    public void closeEscapeMenu(){
        isEscapeMenuActive = false;
        closeEscapeMenu = false;
        for (STGLComponent component: escapeMenuComponents){
            component.disable();
        }

        in_client.enable();
        m.enable();
    }

    public void drawUITiles(STGLGraphics g){
        // Top panel
        BufferedImage top_1 = AssetManager.loadImage("assets/ui_tile/top_1.png");
        BufferedImage top_2 = AssetManager.loadImage("assets/ui_tile/top_2.png");
        BufferedImage top_3 = AssetManager.loadImage("assets/ui_tile/top_3.png");
        // Bottom panel
        BufferedImage bot_top_fill_1 = AssetManager.loadImage("assets/ui_tile/bottom_top_fill.png");
        BufferedImage bot_fill_2 = AssetManager.loadImage("assets/ui_tile/bottom_fill_2.png");
        BufferedImage bot_left_end = AssetManager.loadImage("assets/ui_tile/bottom_left_end.png");
        BufferedImage bot_top_transition =  AssetManager.loadImage("assets/ui_tile/bottom_top_transition.png");
        BufferedImage bot_fill_1 =  AssetManager.loadImage("assets/ui_tile/bottom_fill_1.png");
        BufferedImage bot_right_top_corner =  AssetManager.loadImage("assets/ui_tile/bottom_right_top_corner.png");
        BufferedImage bot_transition =  AssetManager.loadImage("assets/ui_tile/bottom_transition.png");
        BufferedImage bot_top_fill_2 =  AssetManager.loadImage("assets/ui_tile/bottom_top_fill_2.png");
        BufferedImage bot_right_end =  AssetManager.loadImage("assets/ui_tile/bottom_right_end.png");
        BufferedImage bot_top_left_corner =  AssetManager.loadImage("assets/ui_tile/bottom_top_left_corner.png");

        int twidth = (int)Math.ceil(tr_width/64f);

        if (uiBatch == null)
            uiBatch = new TextureBatch(g, textureAtlas, (int)(200*(tr_width*tr_height/(832f*576f))));

        uiBatch.begin();

        // Handle top panel

        //BufferedImage top2_sub = top_2.getSubimage(0,0,1,64);
        TextureRegion _top2 = textureAtlas.getTextureRegion(top_2);
        for (int x = 1; x < twidth; x++){
            uiBatch.drawTextureRegion(x*64, 0, _top2);
        }

        uiBatch.drawTextureRegion(0,0, textureAtlas.getTextureRegion(top_1));
        uiBatch.drawTextureRegion(tr_width-64-1, 0, textureAtlas.getTextureRegion(top_3));

        // Render bottom panel

        int y = tr_height-3*64; // first y index

        int transition_x = twidth-7;

        // pre-transition
        //BufferedImage bot_fill_sub = bot_fill_1.getSubimage(0, 0, 1, 64);
        //BufferedImage bot_top_sub = bot_top_fill_1.getSubimage(0, 0, 1, 64);

        TextureRegion _bot_fill_sub = textureAtlas.getTextureRegion(bot_fill_1);
        TextureRegion _bot_top_sub = textureAtlas.getTextureRegion(bot_top_fill_1);
        for (int x = 1; x < transition_x; x++){
            uiBatch.drawTextureRegion(x*64, y,_bot_top_sub);
            uiBatch.drawTextureRegion(x*64, y+64,_bot_fill_sub);
            uiBatch.drawTextureRegion(x*64, y+128,_bot_fill_sub);
        }

        // post-transition
        //bot_fill_sub = bot_fill_2.getSubimage(0, 0, 1, 64);
        //bot_top_sub = bot_top_fill_2.getSubimage(0, 0, 1, 64);

        _bot_fill_sub = textureAtlas.getTextureRegion(bot_fill_2);
        _bot_top_sub = textureAtlas.getTextureRegion(bot_top_fill_2);
        for (int x = transition_x+1; x < twidth-1; x++){
            uiBatch.drawTextureRegion(x*64, y,_bot_top_sub);
            uiBatch.drawTextureRegion(x*64, y+64,_bot_fill_sub);
            uiBatch.drawTextureRegion(x*64, y+128,_bot_fill_sub);
        }

        // left corner
        uiBatch.drawTextureRegion(0,y,textureAtlas.getTextureRegion(bot_top_left_corner));
        uiBatch.drawTextureRegion(0,y+64,textureAtlas.getTextureRegion(bot_left_end));
        uiBatch.drawTextureRegion(0,y+128,textureAtlas.getTextureRegion(bot_left_end));

        // right
        uiBatch.drawTextureRegion(tr_width-64,y,textureAtlas.getTextureRegion(bot_right_top_corner));
        uiBatch.drawTextureRegion(tr_width-64,y+64,textureAtlas.getTextureRegion(bot_right_end));
        uiBatch.drawTextureRegion(tr_width-64,y+128,textureAtlas.getTextureRegion(bot_right_end));

        // transition
        uiBatch.drawTextureRegion(transition_x*64,y,textureAtlas.getTextureRegion(bot_top_transition));
        uiBatch.drawTextureRegion(transition_x*64,y+64,textureAtlas.getTextureRegion(bot_transition));
        uiBatch.drawTextureRegion(transition_x*64,y+128,textureAtlas.getTextureRegion(bot_transition));

        uiBatch.flush();

        // Draw panels
        g.drawTexture(tr_width-(3*64), y, panel, new Vector4f(1,1,1,1));
        g.drawTexture(0, y, panel, new Vector4f(1,1,1,1));
    }

    public int rightPanelX(){
        return tr_width-3*64;
    }

    public int rightPanelY(){
        return tr_height-3*64;
    }

    public int rightPanelXEnd(){
        return rightPanelX() + 64*3;
    }

    public int rightPanelYEnd(){
        return rightPanelY() + 64*3;
    }

    public int transitionX(){
        return ((int)Math.ceil(tr_width/64f)-7)*64;
    }

    private static final Color brown1 = new Color(96, 39, 2);
    private static final Color lbrown1 = new Color(198, 130, 77);
    private static final Color brown2 = new Color(51, 39, 31);
    private static final Color transparent_black = new Color(0, 0, 0, 0.5F);

    // max 40
    // curently unused
    private float manpower = 0;

    private boolean dbg_text = false;

    private int pr_x = 0;
    private int pr_y = 0;

    // adds ... and shortens property value if too long
    public String dealWithPropertyValue(String s){
        if (s.length() >= 22){
            return String.format("%s...",s.substring(0, 22));
        }
        return s;
    }

    public void paint(STGLGraphics g)
    {

        //g.clear();
        unsafeLock.lock();
        long t1 = System.currentTimeMillis();

        try {
            drawUITiles(g);

            if (hasFirstPackedBeenReceived) {

                int y;

                STGLGraphics g2 = g.childGraphics();
                g2.setTranslationVector(new Vector2i(0, 64));
                g2.defineScissorBox(0, 64, tr_width, tr_height-4*64);

                if (gameBatch == null)
                    gameBatch = new TextureBatch(g2, textureAtlas, (int)(1000*(tr_width*tr_height/(832f*576f))));
                else gameBatch.setGraphics(g2);

                try {
                    gameBatch.begin();
                    renderImagesScale(map_to_render, offset_x, offset_y, scale, g2, gameBatch);
                    renderImagesScale(entity_render, offset_x, offset_y, scale, g2, gameBatch);
                    renderImagesScale(fog_of_war, offset_x, offset_y, scale, g2, gameBatch);
                    renderImagesScale(unit_data_ar, offset_x, offset_y, scale, g2, gameBatch);
                    renderImagesScale(_selr, offset_x, offset_y, scale, g2, gameBatch);
                    gameBatch.flush();
                } catch (BufferOverflowException overflow){
                    // we should resize the buffer accordingly
                    gameBatch = new TextureBatch(g2, textureAtlas, (int)(1000*(tr_width*tr_height/(832f*576f))));
                }

                if (cur_highlight != null){
                    g2.fillRect(cur_highlight[0], cur_highlight[1], (int)Math.ceil(64/scale), (int)Math.ceil(64/scale), Utils.colorToVec(new Color(cur_highlight[2], true)));
                }

                // Preview selector
                if (selector != null && do_render_prev && do_offset) {
                    g2.drawTextureRegionScaled(x_prev, y_prev, selectorTextureAtlas.getTextureRegion(selector), new Vector4f(1,1,1,1), (int)Math.ceil(64/scale), (int)Math.ceil(64/scale));
                }

                // Normal Selector
                if (selector != null) {
                    g2.drawTextureRegionScaled(sel_x_frame, sel_y_frame, selectorTextureAtlas.getTextureRegion(selector), new Vector4f(1,1,1,1), (int)Math.ceil(64/scale), (int)Math.ceil(64/scale));
                }

                g2.dispose();

                // Render the buttons
                int i = 0;
                int x = 0;
                y = 0;
                int btn_idx = 0;
                for (BufferedImage button : buttons) {
                    if (button != null) {
                        if (btn_idx == smallify_button){
                            //g.drawImage(button.getScaledInstance(57, 57, Image.SCALE_FAST), rightPanelX() + x + 3, rightPanelY() + y + 3, null);
                            g.drawTextureRegionScaled(rightPanelX() + x + 3, rightPanelY() + y + 3, textureAtlas.getTextureRegion(button), new Vector4f(1,1,1,1), 57, 57);
                            if (Grass32ConfigClient.doSteelButtonOverlay()) {
                                g.drawTextureRegionScaled(rightPanelX() + x + 3, rightPanelY() + y + 3, textureAtlas.getTextureRegion(AssetManager.loadImage("assets/ui/buttons/steel_button_overlay_2.png")), new Vector4f(1,1,1,1), 57, 57);
                            }
                        }
                        else {
                            g.drawTextureRegion( rightPanelX() + x, rightPanelY() + y, textureAtlas.getTextureRegion(button), new Vector4f(1,1,1,1));
                            if (Grass32ConfigClient.doSteelButtonOverlay()) {
                                g.drawTextureRegion( rightPanelX() + x, rightPanelY() + y, textureAtlas.getTextureRegion(AssetManager.loadImage("assets/ui/buttons/steel_button_overlay_2.png")), new Vector4f(1,1,1,1));
                            }
                        }
                    }
                    i++;
                    x += 64;
                    if (i == 3) {
                        i = 0;
                        x = 0;
                        y += 64;
                    }
                    btn_idx++;
                }

                // Render minimap
                if (minimap != null && queue == null) {
                    // fixme don't use texture
                    minimap_t.writeImage(0,0, minimap);
                    g.drawTextureScaled(16, rightPanelY() + 16, minimap_t, new Vector4f(1,1,1,1), 160, 160);
                }

                // Render the queue
                if (queue != null) {
                    i = 0;
                    x = 0;
                    y = 0;
                    for (BufferedImage m : queue) {
                        if (m != null) {
                            //g.drawImage(m, x, rightPanelY() + y, null);
                            g.drawTextureRegion(x, rightPanelY() + y, textureAtlas.getTextureRegion(m), new Vector4f(1,1,1,1));
                        }
                        i++;
                        x += 64;
                        if (i == 3) {
                            i = 0;
                            x = 0;
                            y += 64;
                        }
                    }
                }

                // Render the binds
                /*if (!Grass32ConfigClient.doSteelButtonOverlay())
                    g.setColor(Color.BLACK);
                else
                    g.setColor(Color.WHITE);
                g.setFont(monogram_button);*/

                i = 0;
                x = 0;
                y = 0;
                btn_idx = 0;
                for (String bind : binds) {
                    if (bind != null) {
                        if (btn_idx == smallify_button) {
                            g.renderText(monogram_button_small,
                                    rightPanelX() + x + 8 + 3,
                                    rightPanelY() - 1 + y + 11 + 3,
                                    bind,
                                    !Grass32ConfigClient.doSteelButtonOverlay() ? new Vector4f(0,0,0,1): new Vector4f(1,1,1,1)
                                    );
                            smallify_button_renders--;
                            if (smallify_button_renders == 0)
                                smallify_button = -1;
                        }
                        else{
                            g.renderText(monogram_button,
                                    rightPanelX() + x + 8,
                                    rightPanelY() - 1 + y + 11,
                                    bind,
                                    !Grass32ConfigClient.doSteelButtonOverlay() ? new Vector4f(0,0,0,1): new Vector4f(1,1,1,1));

                        }
                    }
                    i++;
                    x += 64;
                    if (i == 3) {
                        i = 0;
                        x = 0;
                        y += 64;
                    }
                    btn_idx++;
                }

                // Render the time of production
                //g.setColor(Color.BLACK);
                //g.setFont(monogram_button);

                i = 0;
                x = 0;
                y = 0;
                for (String bind : unit_times) {
                    if (bind != null) {
                       // g.drawString(bind, x + 8, rightPanelY() - 1 + y + 11);
                        g.renderText(monogram_button, x + 8, rightPanelY() - 1 + y + 11, bind, new Vector4f(0,0,0,1));
                    }
                    i++;
                    x += 64;
                    if (i == 3) {
                        i = 0;
                        x = 0;
                        y += 64;
                    }
                }

                // Render the unit (big unit texture)
                if (unit_img != null) {
                    g.drawTextureRegionScaled(transitionX() - 2 * 64, rightPanelY() + 32, selectedUnitAtlas.getTextureRegion(unit_img), new Vector4f(1,1,1,1), 128, 128);
                    //g.drawImage(unit_img, transitionX() - 2 * 64, rightPanelY() + 32, null);
                }

                // Render the stats
                if (propertiesToRender != null && monogram != null) {
                    // Find name of a unit
                    ClientSideProperty name = null;
                    for (ClientSideProperty property : propertiesToRender.properties) {
                        if (property.key().equals("name")) {
                            name = property;
                        }
                    }
                    @SuppressWarnings("unchecked") ArrayList<ClientSideProperty> properties = (ArrayList<ClientSideProperty>) propertiesToRender.properties.clone();
                    properties.remove(name);
                    PropertiesToRender propertiesToRender2 = new PropertiesToRender(properties);

                    //g.setColor(Color.BLACK);
                    //g.setFont(AssetManager.getBoldFontWithSize((32)));

                    // Get font char size
                    STGLFont.FontMetrics metrics = monogram_32_bold.getFontMetrics();
                    int width = metrics.getCharWidth('A');

                    if (name == null) {
                        name = new ClientSideProperty("name", "");
                    }

                    int h = ((rightPanelX()- transitionX()-32 - (name.value().length() * width)) / 2);

                    //g.drawString(name.value(), transitionX()+32+h, rightPanelY() + 30); // 416-640
                    g.renderText(monogram_32_bold, transitionX()+32+h, rightPanelY() + 30, name.value(), new Vector4f(0,0,0,1));

                    int y__ = 1;
                    for (ClientSideProperty clientSideProperty : propertiesToRender2.properties) {
                        if (clientSideProperty.key().equals("uname")){
                            if (clientSideProperty.value().isEmpty()){
                                continue;
                            }
                            //g.setFont(uname_monogram);

                            STGLFont.FontMetrics metrics_16 = uname_monogram.getFontMetrics();
                            int width_16 = metrics_16.getCharWidth('A');

                            int h2 = ((rightPanelX()- transitionX()-32 - ((clientSideProperty.value().length()+4) * width_16)) / 2);

                            g.renderText(uname_monogram, transitionX()+32+ h2, rightPanelY() + 43 + 13 * 11, "<" + clientSideProperty.value() + ">", new Vector4f(0,0,0,1));

                            //g.drawString("<" + clientSideProperty.value() + ">", transitionX()+32+ h2, rightPanelY() + 43 + 13 * 11);
                            //g.setFont(monogram23);

                            continue;
                        }

                        if (PropertiesMatcher.matchKeyToString(clientSideProperty.key()) != null) {
                            if (!Objects.equals("true", clientSideProperty.value())) {
                                g.renderText(monogram_23, transitionX() + 32, rightPanelY() + 43 + 13 * y__,
                                        dealWithPropertyValue(PropertiesMatcher.matchKeyToString(clientSideProperty.key()) + ": " + clientSideProperty.value()),
                                        new Vector4f(0,0,0,1));
                            }
                            else {
                                g.renderText(monogram_23, transitionX() + 32,
                                        rightPanelY() + 43 + 13 * y__,
                                        "(" + PropertiesMatcher.matchKeyToString(clientSideProperty.key()) + ")",
                                        new Vector4f(0,0,0,1));
                            }
                            y__++;
                        }
                    }

                    if (buffsTextures != null){
                        int off = 0;
                        for (BufferedImage buff_: buffsTextures){
                            g.drawTextureRegionScaled(transitionX()+32+off, rightPanelY() + 43 + 13 * 11-uname_monogram.getHeight()-16, selectedUnitAtlas.getTextureRegion(buff_), new Vector4f(1,1,1,1), 16, 16);
                            off+=16;
                        }
                    }
                }

                g.drawTexture(20, 10, steel_icon, new Vector4f(1,1,1,1));
                g.drawTexture(20+47+10, 10, aluminium_icon, new Vector4f(1,1,1,1));

                g.drawTextureScaled(67+47+10-15+20, 10, manpower_img, new Vector4f(1,1,1,1), 18, 18);

                g.renderText(monogram_19, 40+2, 22, String.valueOf(steel), new Vector4f(0,0,0,1));
                g.renderText(monogram_19, 20+47+20+2+10, 22, String.valueOf(aluminium), new Vector4f(0,0,0,1));
                g.renderText(monogram_19, 67+47+10+17-15+20, 22, manpower + "U", new Vector4f(0,0,0,1));
                g.renderText(monogram_19, 20, 40, "Turn: ", new Vector4f(0,0,0,1));
                g.fillRect(55, 33, 8, 8, Utils.colorToVec(teamDoingTurnColor));
                g.renderText(monogram_19,67, 40, Objects.requireNonNullElse(teamDoingTurnNick, "BOT"), new Vector4f(0,0,0,1));

                g.renderText(monogram_19, rightPanelX() + 50, 22, "Camera speed: " + camera_speed, new Vector4f(0,0,0,1));
                //g.setColor(brown1);
                g.fillRect(rightPanelX() - 156, 6, 162,36, Utils.colorToVec(brown1));
                //g.fillRoundRect(rightPanelX() - 156, 6, 162,36, 5, 5);
                //g.setColor(lbrown1);

                int y_count = 0;
                int x_count = 0;
                for (Map.Entry<String, String> entry: gamemodeProperties.entrySet()){
                    g.renderText(monogram_19, rightPanelX()-150+x_count*50, 22+y_count*10, PropertiesMatcher.matchKeyToString(entry.getKey()) + ": " + entry.getValue(), Utils.colorToVec(lbrown1));
                    y_count++;
                    if (y_count == 2){
                        x_count++;
                        y_count = 0;
                    }
                }

                //g.setColor(Color.BLACK);

                if (selector != null && do_render_prev) {
                    if (!do_offset) {
                        g.drawTexture(rightPanelX() + (x_prev * 64), rightPanelY() + (y_prev * 64), bhighlight, new Vector4f(0,0,0,1));
                    }
                }

                // Render currently written chat message
                if (in_client != null)
                    if (in_client.isTypingChatMessage()) {
                        /*if (monogram != null) {
                            g.setColor(Color.WHITE);
                            g.setFont(AssetManager.getBoldFontWithSize(15));
                        }*/
                        String m = "[Chat]: " + in_client.getCurrentMSG();

                        int offset_rect = monogram_15.getFontMetrics().getStringWidth(m);
                        g.renderText(monogram_15,rightPanelX()-140, rightPanelY() - 1 - 40, m, new Vector4f(1,1,1,1));

                        g.fillRect(rightPanelX()-140+offset_rect, rightPanelY() - 1 - 40 -monogram_15.getFontMetrics().getMaxCharHeight()+2,
                                monogram_15.getFontMetrics().getStringWidth(" "), monogram_15.getFontMetrics().getMaxCharHeight(), new Vector4f(1,1,1,1));
                    }

                if (textUI != null){
                    BufferedImage img = AssetManager.loadImage("assets/text_ui.png");

                    g.drawTexture(rightPanelX()-76, 76, ttextUI, new Vector4f(1,1,1,1));

                    if (textUIFont == null)
                        textUIFont = fontManager.createFont("font/monogram-extended.ttf",textUI.getFontSize(),false, false);
                    //g.setFont(AssetManager.getFontWithSize(textUI.getFontSize()));

                    int y_ = 0;
                    int b = 0;
                    for (String s: textUI.getText().split("\n")) {
                        if (textUI instanceof GoalUI gp){
                            //g.setColor(brown2);
                            g.fillRect(rightPanelX() - 76 + 16, 76 + 22 + y_- 7, 9, 9, Utils.colorToVec(brown2));
                            //g.setColor(lbrown1);
                            if (gp.getDone()[b])
                                g.fillRect(rightPanelX() - 76 + 18, 76 + 22 + y_- 5, 5, 5, Utils.colorToVec(lbrown1));
                            g.renderText(textUIFont, rightPanelX() - 76 + 26, 76 + 22 + y_ + 1, s, new Vector4f(0,0,0,1));
                        } else
                            g.renderText(textUIFont, rightPanelX() - 76 + 16, 76 + 22 + y_ + 1, s, new Vector4f(0,0,0,1));
                        y_+=textUIFont.getFontMetrics().getMaxCharHeight();
                        b++;
                    }
                }

                // Render chat
                y = 0;
                if (chat != null)
                    for (String msg : chat) {
                        g.renderText(monogram_15, rightPanelX() - 140, rightPanelY() - 151 + (y * 10), msg.substring(0, msg.length()-1), new Vector4f(1,1,1,1));
                        y++;
                    }

                // Render debug stuff
                if (dbg_text) {
                    int ___x = tr_width - 164 - 125;
                    long t2 = System.currentTimeMillis();
                    g.renderText(monogram, ___x, 68, String.format("Stalemate Client. Packet version: %d", StVersion.packet_version), new Vector4f(1, 1, 1, 1));
                    g.renderText(monogram, ___x, 68 + 15, String.format("Game version: %s", StVersion.version), new Vector4f(1, 1, 1, 1));
                    g.renderText(monogram, ___x, 68 + 30, String.format("pr_x: %d, pr_y: %d", pr_x, pr_y), new Vector4f(1, 1, 1, 1));
                    g.renderText(monogram, ___x, 68 + 45, String.format("cam_x: %d, cam_y: %d", cam_x, cam_y), new Vector4f(1, 1, 1, 1));
                    g.renderText(monogram, ___x, 68 + 45+15, String.format("TFPS: %f", 1/(float)(t2-t1)), new Vector4f(1, 1, 1, 1));
                }

                if (isEscapeMenuActive) {
                    g.fillRect(0, 0, tr_width, tr_height, Utils.colorToVec(transparent_black));
                    g.fillRect(escapeMenuOff.x, escapeMenuOff.y, 150, 100, Utils.colorToVec(new Color(60, 38, 22)));
                    escapeMenuCompManager.paint();
                }

                if (showResults) {
                    g.fillRect(0, 0, tr_width, tr_height, Utils.colorToVec(transparent_black));

                    int x_m = (tr_width - 300) / 2;
                    int y_m = (tr_height - 300) / 2;

                    g.fillRect(x_m, y_m, 300, 300, Utils.colorToVec(new Color(60, 38, 22)));
                    g.fillRect(x_m, y_m, 300, 45, Utils.colorToVec(new Color(131, 71, 37)));
                    g.drawRect(x_m, y_m, 300, 300, 4, Utils.colorToVec(new Color(52, 33, 19)));
                    int _y_m = y_m + 45 + 25;

                    g.renderText(monogram, x_m + 20, _y_m, result, Utils.colorToVec(new Color(198, 130, 77)));

                    g.drawRect(x_m, y_m, 300, 45, 4, Utils.colorToVec(new Color(52, 33, 19)));
                    g.renderText(monogram_15,
                            x_m + (300 - monogram_15.getFontMetrics().getStringWidth("Game result")) / 2,
                            y_m + (45 - monogram_15.getFontMetrics().getMaxCharHeight()) / 2 + monogram_15.getFontMetrics().getMaxCharHeight(), "Game result", Utils.colorToVec(new Color(198, 130, 77)));
                    g.drawImage(x_m + 5, y_m + (45 - 32) / 2 + 2, information_logo);
                    escapeMenuCompManager.paint();

                }

                if (tooltip_text != null){
                    int fx = lm_x-4;
                    if (lm_x-4+tooltip_box_w > tr_width){
                        fx = tr_width-tooltip_box_w-8;
                    }

                    int fy = lm_y-4;
                    if (lm_y-4+tooltip_box_h > tr_height){
                        fy = tr_width-tooltip_box_w-8;
                    }

                    g.fillRect(fx, fy, tooltip_box_w+8, tooltip_box_h+8, new Vector4f(0,0,0,1));
                    g.renderFormattedText(monogram, monogram, monogram, monogram, tooltip_text, fx+4, fy+4+monogram.getMaxHeight());
                }

            }
            else {
                g.drawRect(0,64, 64*13, 64*5, new Vector4f(0,0,0,0));
            }

            //g.dispose();
        } catch (Exception e){
            e.printStackTrace();
        }
        unsafeLock.unlock();
    }

    private void renderImagesScale(ArrayList<ArrayList<BufferedImage>> buffered_images, int offset_x, int offset_y, float scale, STGLGraphics g2, TextureBatch textureBatch) {
        int y;
        y = 0;
        for (ArrayList<BufferedImage> row_x: buffered_images){
            int x_count = 0;

            for (BufferedImage x : row_x){
                if (x != null) {
                    int wh = (int) Math.ceil((1 / scale)*(64));

                    textureBatch.drawTextureRegionScaled(
                            (x_count - 1) * (int) Math.ceil(64 / scale) + (int) Math.ceil(-offset_x / scale), (y - 1) * (int) Math.ceil(64 / scale) + (int) Math.ceil(-offset_y / scale),
                            textureAtlas.getTextureRegion(x), wh, wh

                    );
                }
                x_count++;
            }
            y++;
        }
    }

    // tooltip related
    private int lm_x;
    private int lm_y;

    private int tooltip_box_w;
    private int tooltip_box_h;

    private String tooltip_text;

    private void setTooltipText(String text){
        if (text == null){
            tooltip_text = null;
            return;
        }

        List<STGLTextFormatter.Token> tokens = STGLTextFormatter.formattedTokens(text);

        int prv_x = 0;
        int cur_x = 0;
        int y = 0;

        for (STGLTextFormatter.Token t: tokens){
            if (t.tt() == STGLTextFormatter.TokenType.TEXT){
                String[] split = t.t().split("\n");
                for (String s: split){
                    prv_x = Math.max(prv_x, monogram.getFontMetrics().getStringWidth(s));
                    y+=monogram.getMaxHeight();
                }
            }
        }


        tooltip_box_h = y;
        tooltip_box_w = Math.max(prv_x, cur_x);

        tooltip_text = text;
    }

    public void dispose(){
        this.monogram_15.dispose();
        this.monogram.dispose();
        this.monogram_32_bold.dispose();
        this.monogram_32.dispose();
        this.minimap_t.dispose();
        this.bhighlight.dispose();
        this.textureAtlas.getAtlas().dispose();
        this.selectorTextureAtlas.getAtlas().dispose();
        this.information_logo.drop();
        this.manpower_img.dispose();
        this.steel_icon.dispose();
        this.monogram_18.dispose();
        this.monogram_23.dispose();
        this.monogram_19.dispose();
        this.monogram_button.dispose();
        this.monogram_button_small.dispose();
        this.selectedUnitAtlas.getAtlas().dispose();
        this.uname_monogram.dispose();
        ErrorCheck.checkErrorGL();
    }
}
