package net.stalemate.client.ui;

import net.stalemate.StVersion;
import net.stalemate.client.AssetManager;
import net.stgl.MainFrame;
import net.stgl.STGLGraphics;
import net.stgl.Utils;
import net.stgl.event.WindowResizeListener;
import net.stgl.font.FontManager;
import net.stgl.font.STGLFont;
import net.stgl.image.STGLImage;
import net.stgl.texture.Texture;
import net.stgl.texture.TextureManager;
import net.stgl.ui.ComponentManager;
import net.stgl.ui.STGLButton;
import org.joml.Vector4f;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.locks.ReentrantLock;

public class MainMenu {
    private STGLButton play = null;
    private STGLButton singleplayer = null;
    private STGLButton settings = null;
    private STGLButton exit = null;

    private STGLButton ok = null;
    private final ReentrantLock lock = new ReentrantLock();
    private final MainFrame frame;
    private final ComponentManager componentManager;

    private final static Color StalemateGreen = new Color(35, 115, 0);

    private final Texture background;
    private final Texture title;

    private int width;
    private int height;

    private STGLFont monogram12;

    private final int title_width_img;
    private final int title_height_img;

    private final STGLImage information_logo;

    private boolean playPressed = false;

    private boolean settingsPressed = false;
    private boolean singleplayerPressed = false;
    private boolean reorganizeButtons = false;

    private final FontManager fontManager;

    private final WindowResizeListener resizeListener = (nwidth, nheight) -> {
        width = nwidth;
        height = nheight;

        play.setX((width-125)/2);
        play.setY((int) (0.43*height)+3);

        singleplayer.setX((width-125)/2);
        singleplayer.setY((int) (0.43*height)+30+6);

        settings.setX((width-125)/2);
        settings.setY((int) (0.43*height)+60+9);

        exit.setX((width-125)/2);
        exit.setY((int) (0.43*height)+90+12);

        if (ok != null){
            int x_m = (width - 300) / 2;
            int y_m = (height - 300) / 2;

            ok.setX(x_m + (300 - 150) / 2);
            ok.setY(y_m + 270);
        }
    };

    public MainMenu(MainFrame frame, ComponentManager componentManager, int width, int height, FontManager fontManager){
        this.frame = frame;
        this.componentManager = componentManager;

        this.fontManager = fontManager;

        monogram12 = fontManager.createFont("font/monogram-extended.ttf", 12, false, false);

        this.width = width;
        this.height = height;

        frame.addWindowResizeListener(resizeListener);

        background = new Texture(AssetManager.loadImage("assets/background.png"));
        BufferedImage title_img = AssetManager.loadImage("assets/stalemate.png");
        title = new Texture(title_img.getWidth(), title_img.getHeight()+2, new Texture.TextureOptions().setMagFiltering(Texture.Filtering.NEAREST).setMinFiltering(Texture.Filtering.NEAREST));
        title.writeImage(0,0, title_img);
        title_width_img = title_img.getWidth();
        title_height_img = title_img.getHeight();

        play = new STGLButton(125, 30, (width-125)/2, (int) (0.43*height)+3,"Multiplayer");
        singleplayer = new STGLButton(125, 30, (width-125)/2, (int) (0.43*height)+30+6,"Singleplayer");
        settings = new STGLButton(125, 30, (width-125)/2, (int) (0.43*height)+60+9,"Settings");
        exit = new STGLButton(125, 30, (width-125)/2, (int) (0.43*height)+90+12,"Exit");

        StalemateStyle.makeButton(play);
        StalemateStyle.makeButton(singleplayer);
        StalemateStyle.makeButton(settings);
        StalemateStyle.makeButton(exit);

        exit.setActionListener(() -> System.exit(0));
        play.setActionListener(() -> playPressed = true);
        singleplayer.setActionListener(() -> singleplayerPressed = true);
        settings.setActionListener(() -> settingsPressed = true);

        //singleplayer.partiallyDisable();
        //options.partiallyDisable();

        play.setFont(monogram12);
        singleplayer.setFont(monogram12);
        settings.setFont(monogram12);
        exit.setFont(monogram12);

        componentManager.addComponent(play);
        componentManager.addComponent(singleplayer);
        componentManager.addComponent(settings);
        componentManager.addComponent(exit);

        int x_m = (width - 300) / 2;
        int y_m = (height - 300) / 2;

        ok = new STGLButton(150, 30, x_m+(300-150)/2, y_m+270, "OK");
        ok.setFont(monogram12);
        StalemateStyle.makeButton(ok);
        componentManager.addComponent(ok);
        ok.disable();
        ok.setActionListener(() ->{
            isMessage = false;
            reorganizeButtons = true;
        });

        try {
            information_logo = new STGLImage(TextureManager.loadBufferedImage("assets/information_logo.png"), new Texture.TextureOptions().setMinFiltering(Texture.Filtering.NEAREST).setMagFiltering(Texture.Filtering.NEAREST));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private boolean isMessage = false;
    private ArrayList<String> message = null;

    public void paint(STGLGraphics g) {
        lock.lock();
        g.drawTextureScaled(0, 0, background, new Vector4f(1,1,1,1), width, height);

        g.renderText(monogram12, 3, height - 6, "Version " + StVersion.version, new Vector4f(1,1,1,1));
        g.renderText(monogram12, width - monogram12.getFontMetrics().getStringWidth("See NOTICE.md and LICENSE.md for m"), height - 6 - 30,"Made by Weltspear and SP7",new Vector4f(1,1,1,1));
        g.renderText(monogram12, width - monogram12.getFontMetrics().getStringWidth("See NOTICE.md and LICENSE.md for m"), height - 6 - 20, "Licensed under terms of GNU AGPLv3", new Vector4f(1,1,1,1));
        g.renderText(monogram12,width - monogram12.getFontMetrics().getStringWidth("See NOTICE.md and LICENSE.md for m"), height - 6 - 10, "See NOTICE.md and LICENSE.md for m", new Vector4f(1,1,1,1));
        g.renderText(monogram12,width - monogram12.getFontMetrics().getStringWidth("See NOTICE.md and LICENSE.md for m"), height - 6, "ore information", new Vector4f(1,1,1,1));

        if (title != null)
            g.drawTextureRegionScaled((width - 364) / 2, (int) (0.43f * height) - 94,  title.getSubTexture(0,0,title_width_img,title_height_img), new Vector4f(1,1,1,1),364,64);

        if (reorganizeButtons){
            play.enable();
            singleplayer.enable();
            settings.enable();
            exit.enable();
            ok.disable();
            reorganizeButtons = false;
        }

        //
        if (isMessage) {
            int x_m = (width - 300) / 2;
            int y_m = (height - 300) / 2;

            g.fillRect(x_m, y_m, 300, 300, Utils.colorToVec(new Color(60, 38, 22)));
            g.fillRect(x_m, y_m, 300, 45, Utils.colorToVec(new Color(131, 71, 37)));
            g.drawRect(x_m, y_m, 300, 300, 4, Utils.colorToVec(new Color(52, 33, 19)));
            int _y_m = y_m + 45 + 25;
            for (String s: message) {
                g.renderText(monogram12, x_m + 20, _y_m, s, Utils.colorToVec(new Color(198, 130, 77)));
                _y_m+=20;
            }
            g.drawRect(x_m, y_m, 300, 45, 4, Utils.colorToVec(new Color(52, 33, 19)));
            g.renderText(monogram12,
                    x_m + (300 - monogram12.getFontMetrics().getStringWidth("Information"))/2,
                       y_m + (45 - monogram12.getFontMetrics().getMaxCharHeight())/2+monogram12.getFontMetrics().getMaxCharHeight(), "Information", Utils.colorToVec(new Color(198, 130, 77)));
            g.drawImage(x_m+5, y_m+(45-32)/2+2, information_logo);
        }

        componentManager.paint();
        lock.unlock();

    }

    public boolean isPlayPressed() {
        return playPressed;
    }

    public boolean isSingleplayerPressed() {
        return singleplayerPressed;
    }

    public void cleanup(){
        componentManager.removeComponent(play);
        componentManager.removeComponent(singleplayer);
        componentManager.removeComponent(settings);
        componentManager.removeComponent(exit);
        frame.removeWindowResizeListener(resizeListener);
    }

    public void reinit(){
        width = frame.getWidth();
        height = frame.getHeight();
        resizeListener.windowResize(width, height);
        componentManager.addComponent(play);
        componentManager.addComponent(singleplayer);
        componentManager.addComponent(settings);
        componentManager.addComponent(exit);
        frame.addWindowResizeListener(resizeListener);
        playPressed = false;
        singleplayerPressed = false;
        settingsPressed = false;
    }

    public void setMessage(String s){
        message = new ArrayList<>();
        isMessage = true;
        StringBuilder text = new StringBuilder();
        for (int i = 0; i < s.toCharArray().length; i++){
            char c = s.toCharArray()[i];
            if (c == '\n' || !(i+1 < s.toCharArray().length)){
                if (!(i+1 < s.toCharArray().length)){
                    text.append(c);
                }
                message.add(text.toString());
                text = new StringBuilder();
            }
            text.append(c);
        }
        play.disable();
        singleplayer.disable();
        settings.disable();
        exit.disable();
        ok.enable();
    }

    public void rebuildFont(){
        monogram12.dispose();
        monogram12 = fontManager.createFont("font/monogram-extended.ttf", 12, false, false);
        play.setFont(monogram12);
        singleplayer.setFont(monogram12);
        settings.setFont(monogram12);
        exit.setFont(monogram12);
        ok.setFont(monogram12);
    }

    public boolean isSettingsPressed() {
        return settingsPressed;
    }
}

