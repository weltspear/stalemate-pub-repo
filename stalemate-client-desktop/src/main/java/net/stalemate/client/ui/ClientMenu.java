/*
 * Stalemate Game
 * Copyright (C) 2022 Weltspear
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package net.stalemate.client.ui;

import net.stalemate.client.AssetManager;
import net.stgl.MainFrame;
import net.stgl.STGLGraphics;
import net.stgl.event.WindowResizeListener;
import net.stgl.font.FontManager;
import net.stgl.font.STGLFont;
import net.stgl.texture.Texture;
import net.stgl.ui.ComponentManager;
import net.stgl.ui.STGLButton;
import net.stgl.ui.STGLEntry;
import org.joml.Vector4f;

import java.awt.image.BufferedImage;

import static net.stalemate.client.Utils.setXY;

public class ClientMenu {

    private final int title_width_img;
    private final int title_height_img;
    private final Texture background;
    private final Texture title;

    private STGLEntry entry;
    private STGLButton connect;

    private final MainFrame frame;
    private int width;
    private int height;

    private final ComponentManager componentManager;

    private String data = null;

    public String getData() {
        return data;
    }

    private WindowResizeListener resizeListener = (nwidth, nheight) -> {
        width = nwidth;
        height = nheight;
        setXY(entry, (width-240)/2, (int)(0.43f*height));
        setXY(connect, (width-125)/2, (int)(0.43f*height)+45);
    };

    public ClientMenu(MainFrame frame, ComponentManager componentManager, FontManager fontManager){
        this.frame = frame;
        this.width = frame.getWidth();
        this.height = frame.getHeight();

        frame.addWindowResizeListener(resizeListener);

        STGLFont monogram_12 = fontManager.createFont("font/monogram-extended.ttf", 12, false, false);

        entry = new STGLEntry((width-240)/2, (int)(0.43f*height), 240, 40,"Enter IP");
        entry.setFont(monogram_12);
        StalemateStyle.makeEntry(entry);
        componentManager.addComponent(entry);

        connect = new STGLButton(125, 30, (width-125)/2, (int)(0.43f*height)+45,"Connect");
        StalemateStyle.makeButton(connect);
        connect.setFont(monogram_12);
        componentManager.addComponent(connect);
        connect.setActionListener(() -> data = entry.getText());

        background = new Texture(AssetManager.loadImage("assets/background.png"));
        BufferedImage title_img = AssetManager.loadImage("assets/stalemate.png");

        title = new Texture(title_img.getWidth(), title_img.getHeight()+2, new Texture.TextureOptions().setMagFiltering(Texture.Filtering.NEAREST).setMinFiltering(Texture.Filtering.NEAREST));
        title.writeImage(0,0, title_img);
        title_width_img = title_img.getWidth();
        title_height_img = title_img.getHeight();
        this.componentManager = componentManager;

    }

    public void paint(STGLGraphics g) {

        g.drawTextureScaled(0, 0, background, new Vector4f(1,1,1,1), width, height);

        if (title != null)
            g.drawTextureRegionScaled((width - 364) / 2, (int) (0.43f * height) - 94,  title.getSubTexture(0,0,title_width_img,title_height_img), new Vector4f(1,1,1,1),364,64);

        componentManager.paint();

    }

    public void cleanup(){
        componentManager.removeComponent(connect);
        componentManager.removeComponent(entry);
        frame.removeWindowResizeListener(resizeListener);
    }
}
