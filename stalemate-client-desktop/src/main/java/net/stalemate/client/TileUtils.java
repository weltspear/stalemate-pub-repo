/*
 * Stalemate Game
 * Copyright (C) 2022 Weltspear
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package net.stalemate.client;

import java.awt.*;
import java.awt.image.BufferedImage;

public class TileUtils {

    public static int calcAvgRGB(BufferedImage tile){
        float r = 0;
        float g = 0;
        float b = 0;

        float area = tile.getHeight()*tile.getWidth();

        for (int y = 0; y < tile.getHeight(); y++){
            for (int x = 0; x < tile.getWidth(); x++){
                Color c = new Color(tile.getRGB(x, y));
                r+=c.getRed();
                g+=c.getGreen();
                b+=c.getBlue();
            }
        }
        return new Color((int) Math.floor(r/area), (int) Math.floor(g/area), (int) Math.floor(b/area)).getRGB();
    }
}
