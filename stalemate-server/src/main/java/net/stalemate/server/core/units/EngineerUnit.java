/*
 * Stalemate Game
 * Copyright (C) 2022 Weltspear
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package net.stalemate.server.core.units;

import net.stalemate.server.core.AirUnit;
import net.stalemate.server.core.Entity;
import net.stalemate.server.core.Unit;
import net.stalemate.server.core.animation.Animation;
import net.stalemate.server.core.animation.AnimationController;
import net.stalemate.server.core.buttons.AttackButton;
import net.stalemate.server.core.buttons.MotorizeButton;
import net.stalemate.server.core.buttons.MoveButton;
import net.stalemate.server.core.buttons.RecoverButton;
import net.stalemate.server.core.controller.Game;
import net.stalemate.server.core.minerals.IHarvestableEntity;
import net.stalemate.server.core.properties.Properties;
import net.stalemate.server.core.units.buildings.*;
import net.stalemate.server.core.units.util.IBuilding;
import net.stalemate.server.core.units.util.IConstructableBuilding;
import net.stalemate.server.core.units.util.IMechanized;
import net.stalemate.server.core.units.util.IUnitName;

import java.lang.reflect.InvocationTargetException;

public class EngineerUnit extends Unit implements IUnitName{

    MotorizeButton motbutton = new MotorizeButton();

    public static class RepairButton implements ISelectorButtonUnit{

        private final Layer layer;

        public RepairButton(Layer layer){
            this.layer = layer;
        }
        @Override
        public String bind() {
            return layer == Layer.GROUND ? "R" : "E";
        }

        @Override
        public String texture() {
            return "assets/ui/buttons/repair_button.png";
        }

        @Override
        public String identifier() {
            return layer == Layer.GROUND ? "button_repair" : "button_repair_air";
        }

        @Override
        public void action(Unit selected_unit, Unit unit, Game gameController) {
            if (unit.unitStats().getSupply() >= 14 && !unit.hasTurnEnded() && selected_unit != unit){
                if ((selected_unit instanceof IMechanized || selected_unit instanceof IBuilding) && selected_unit.unitStats().getMaxHp() != selected_unit.unitStats().getHp()){
                    selected_unit.setHp(selected_unit.getHp() + 3);
                    if (selected_unit.unitStats().getHp() > selected_unit.unitStats().getMaxHp()){
                        selected_unit.setHp(selected_unit.unitStats().getMaxHp());
                    }
                    unit.endTurn();
                    unit.consumeSupply(4);
                }
            }
        }

        @Override
        public int selector_range() {
            return 1;
        }

        @Override
        public String selector_texture() {
            return "assets/ui/selectors/ui_repair.png";
        }

        @Override
        public boolean isUsedOnOurUnit() {
            return true;
        }

        @Override
        public boolean isUsedOnEnemy() {
            return false;
        }

        @Override
        public boolean isUsedOnAlliedUnit() {
            return true;
        }

        @Override
        public Layer getLayer() {
            return layer;
        }
    }

    // Build menu things

    public static class BuildMenuButton implements IStandardButton{
        @Override
        public String bind() {
            return "B";
        }

        @Override
        public String texture() {
            return "assets/ui/buttons/build_yes.png";
        }

        @Override
        public String identifier() {
            return "button_engineer_unit_build_menu";
        }

        @Override
        public void action(Unit unit, Game gameController) {
            if (unit instanceof EngineerUnit eu){
                eu.isInBuildingMode = true;
            }
        }

        @Override
        public boolean canBeUsedWhenOtherTeamsTurn(){
            return true;
        }

    }

    public static class ExitBuildMenuButton implements IStandardButton{
        @Override
        public String bind() {
            return "B";
        }

        @Override
        public String texture() {
            return "assets/ui/buttons/build_no.png";
        }

        @Override
        public String identifier() {
            return "button_engineer_unit_build_menu_exit";
        }

        @Override
        public void action(Unit unit, Game gameController) {
            if (unit instanceof EngineerUnit eu){
                eu.isInBuildingMode = false;
            }
        }

        @Override
        public boolean canBeUsedWhenOtherTeamsTurn(){
            return true;
        }
    }

    public abstract static class ConstructBuildingButton implements ISelectorButton{
        private final Class<? extends Unit> b;
        private final int constructionTime;
        private final int steelCost;
        private final int aluminiumCost;
        private final boolean isNeutral;

        public Class<? extends Unit> getUnitBuilt() {
            return b;
        }

        public int getConstructionTime() {
            return constructionTime;
        }

        public int getSteelCost() {
            return steelCost;
        }

        public ConstructBuildingButton(Class<? extends Unit> building, int constructionTime, int steelCost, int aluminiumCost, boolean isNeutral){
            b = building;
            this.constructionTime = constructionTime;
            this.steelCost = steelCost;
            this.aluminiumCost = aluminiumCost;
            this.isNeutral = isNeutral;
        }

        @Override
        public void action(int x, int y, Unit unit, Game gameController) {
            if (!unit.hasTurnEnded() && unit.getTurnMoveAmount() > 0) {

                if (!gameController.isPassable(Layer.GROUND, x, y))
                    return;

                if (!(unit.getTeam().getSteel() - steelCost >= 0)){
                    return;
                }

                if (!(unit.getTeam().getAluminium() - aluminiumCost >= 0)){
                    return;
                }

                Unit building = null;

                try {
                    building = b.getConstructor(int.class, int.class, Game.class).newInstance(x, y, gameController);
                } catch (InstantiationException | IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
                    e.printStackTrace();
                }
                assert building != null;
                if (!isNeutral)
                    unit.getTeam().addUnit(building);
                else
                    gameController.getNeutralTeam().addUnit(building);

                if (!(building instanceof IConstructableBuilding)){
                    throw new IllegalArgumentException("Building must be constructable!");
                }

                UnderConstructionBuilding ucb = new UnderConstructionBuilding(x, y, gameController, building, constructionTime, ((IConstructableBuilding) building).underConstructionAC());
                if (!isNeutral)
                    unit.getTeam().addUnit(ucb);
                else
                    gameController.getNeutralTeam().addUnit(ucb);
                gameController.addEntity(ucb);

                unit.getTeam().setSteel((unit.getTeam().getSteel()*10 - steelCost *10)/10f);
                unit.getTeam().setAluminium((unit.getTeam().getAluminium()*10 - aluminiumCost *10)/10f);

                unit.setMoveAmount(unit.getMoveAmount()-1);
                if (unit.getMoveAmount() == 0){
                    unit.endTurn();
                }

            }
        }

        @Override
        public int selector_range() {
            return 1;
        }
    }


    public static class ConstructMetallurgy implements ISelectorButton{
        private final int constructionTime;
        private final int cost;

        public int getConstructionTime() {
            return constructionTime;
        }

        public int getCost() {
            return cost;
        }

        public ConstructMetallurgy(int constructionTime, int cost){
            this.constructionTime = constructionTime;
            this.cost = cost;
        }

        private boolean isPassableBuildMetallurgy(Game g, int x, int y) {
            for (Entity entity : g.getEntities(x, y)) {
                if (entity instanceof IHarvestableEntity)
                    continue;
                if (!(entity instanceof AirUnit)) {
                    if (!entity.isPassable())
                        return false;
                }
            }
            return true;
        }

        @Override
        public void action(int x, int y, Unit unit, Game gameController) {
            if (!unit.hasTurnEnded() && unit.getTurnMoveAmount() > 0) {

                boolean harvestable = false;
                for (Entity entity: gameController.getEntities(x, y)){
                    if (entity instanceof IHarvestableEntity){
                        harvestable = true;
                        break;
                    }
                }
                if (!harvestable)
                    return;

                if (!isPassableBuildMetallurgy(gameController, x, y))
                    return;

                if (!(unit.getTeam().getSteel()-cost >= 0)){
                    return;
                }

                Unit building;

                building = new Metallurgy(x, y, gameController);
                unit.getTeam().addUnit(building);

                UnderConstructionBuilding ucb = new UnderConstructionBuilding(x, y, gameController, building, constructionTime, ((IConstructableBuilding) building).underConstructionAC());
                unit.getTeam().addUnit(ucb);
                gameController.addEntity(ucb);

                unit.getTeam().setSteel((unit.getTeam().getSteel()*10 - cost*10)/10f);

                unit.setMoveAmount(unit.getMoveAmount()-1);
                if (unit.getMoveAmount() == 0){
                    unit.endTurn();
                }

            }
        }

        @Override
        public int selector_range() {
            return 1;
        }

        @Override
        public String selector_texture() {
            return "assets/ui/selectors/ui_repair.png";
        }

        @Override
        public String bind() {
            return "E";
        }

        @Override
        public String texture() {
            return "assets/ui/buttons/build_factory2.png";
        }

        @Override
        public String identifier() {
            return "button_engineer_unit_build_menu_build_factory";
        }
    }

    public class NextBuildMenuPage implements IStandardButton{

        @Override
        public String bind() {
            return "N";
        }

        @Override
        public String texture() {
            return "assets/ui/buttons/next_page_button.png";
        }

        @Override
        public String identifier() {
            return "build_next_page";
        }

        @Override
        public boolean canBeUsedWhenOtherTeamsTurn() {
            return true;
        }

        @Override
        public void action(Unit unit, Game gameController) {
            build_page = 1;
        }
    }

    public class PreviousBuildMenuPage implements IStandardButton{

        @Override
        public String bind() {
            return "P";
        }

        @Override
        public String texture() {
            return "assets/ui/buttons/previous_page_button.png";
        }

        @Override
        public String identifier() {
            return "build_previous_page";
        }

        @Override
        public boolean canBeUsedWhenOtherTeamsTurn() {
            return true;
        }

        @Override
        public void action(Unit unit, Game gameController) {
            build_page = 0;
        }
    }

    private boolean isInBuildingMode = false;
    public int build_page = 0;

    public EngineerUnit(int x, int y, Game game) {
        super(x, y, game, new UnitStats(10, 10, 1, 1, 1, 0, 20, 20, 0, 0, 0), new AnimationController(), "Engineer");

        Animation idle = new Animation(20);
        idle.addFrame("assets/units/engineer_idle_1.png");
        idle.addFrame("assets/units/engineer_idle_2.png");

        Animation attack = new Animation(1);
        attack.addFrame("assets/units/engineer_fire_1.png");
        attack.addFrame("assets/units/engineer_fire_2.png");
        attack.addFrame("assets/units/engineer_fire_3.png");
        attack.addFrame("assets/units/engineer_fire_4.png");
        attack.addFrame("assets/units/engineer_fire_5.png");

        anim.addAnimation("idle", idle);
        anim.addAnimation("attack", attack);
        anim.addShift("attack", "idle");
        anim.setCurrentAnimation("idle");

        move_amount = 2;
        turn_move_amount = 2;
    }

    public void makeBuildingMode(IButton[] buttons){
        if (build_page == 0) {
            buttons[0] = (new ConstructBuildingButton(MilitaryTent.class, 1, 3, 0, false) {
                @Override
                public String selector_texture() {
                    return "assets/ui/selectors/ui_repair.png";
                }

                @Override
                public String bind() {
                    return "M";
                }

                @Override
                public String texture() {
                    return "assets/ui/buttons/build_base.png";
                }

                @Override
                public String identifier() {
                    return "button_engineer_unit_build_menu_build_military_tent";
                }
            });
            buttons[1] = (new ConstructBuildingButton(TankFactory.class, 2, 2, 1, false) {
                @Override
                public String selector_texture() {
                    return "assets/ui/selectors/ui_repair.png";
                }

                @Override
                public String bind() {
                    return "T";
                }

                @Override
                public String texture() {
                    return "assets/ui/buttons/build_factory.png";
                }

                @Override
                public String identifier() {
                    return "button_engineer_unit_build_menu_build_tank_factory";
                }
            });
            buttons[2] = (new ConstructBuildingButton(SupplyStation.class, 2, 3, 0, false) {
                @Override
                public String selector_texture() {
                    return "assets/ui/selectors/ui_repair.png";
                }

                @Override
                public String bind() {
                    return "S";
                }

                @Override
                public String texture() {
                    return "assets/ui/buttons/build_supply_station.png";
                }

                @Override
                public String identifier() {
                    return "button_engineer_unit_build_menu_build_supply_station";
                }
            });
            buttons[3] = (new ConstructBuildingButton(FortificationEmpty.class, 2, 3, 0, true) {
                @Override
                public String selector_texture() {
                    return "assets/ui/selectors/ui_repair.png";
                }

                @Override
                public String bind() {
                    return "F";
                }

                @Override
                public String texture() {
                    return "assets/ui/buttons/build_fortification.png";
                }

                @Override
                public String identifier() {
                    return "button_engineer_unit_build_menu_build_fortification";
                }
            });
            buttons[4] = (new ConstructBuildingButton(Radar.class, 1, 1, 1, false) {
                @Override
                public String selector_texture() {
                    return "assets/ui/selectors/ui_repair.png";
                }

                @Override
                public String bind() {
                    return "A";
                }

                @Override
                public String texture() {
                    return "assets/ui/buttons/build_radar.png";
                }

                @Override
                public String identifier() {
                    return "button_engineer_unit_build_menu_build_radar";
                }
            });
            buttons[5] = (new ConstructMetallurgy(1, 2));
            buttons[6] = (new ConstructBuildingButton(Airport.class, 3, 1, 2, false) {
                @Override
                public String selector_texture() {
                    return "assets/ui/selectors/ui_repair.png";
                }

                @Override
                public String bind() {
                    return "P";
                }

                @Override
                public String texture() {
                    return "assets/ui/buttons/build_airport.png";
                }

                @Override
                public String identifier() {
                    return "button_engineer_unit_build_menu_build_airport";
                }
            });
            buttons[7] = new NextBuildMenuPage();

        }
        else{
            buttons[0] = (new ConstructBuildingButton(AntiAirBuilding.class, 2, 2, 1, false) {
                @Override
                public String selector_texture() {
                    return "assets/ui/selectors/ui_repair.png";
                }

                @Override
                public String bind() {
                    return "R";
                }

                @Override
                public String texture() {
                    return "assets/ui/buttons/build_anti_air.png";
                }

                @Override
                public String identifier() {
                    return "button_engineer_unit_build_menu_build_antiair";
                }
            });
            buttons[1] = (new ConstructBuildingButton(Barracks.class, 3, 5, 0, false) {
                @Override
                public String selector_texture() {
                    return "assets/ui/selectors/ui_repair.png";
                }

                @Override
                public String bind() {
                    return "C";
                }

                @Override
                public String texture() {
                    return "texture_missing";
                }

                @Override
                public String identifier() {
                    return "button_engineer_unit_build_menu_build_barracks";
                }
            });
            buttons[7] = new PreviousBuildMenuPage();

        }
        buttons[8] = new ExitBuildMenuButton();
    }

    @Override
    public IButton[] getButtons() {
        IButton[] buttons = new IButton[9];
        if (!isInBuildingMode) {
            buttons[0] = new MoveButton(movement_range);
            buttons[1] = new AttackButton(attack_range);
            buttons[2] = motbutton;
            buttons[3] = new RepairButton(Layer.GROUND);
            buttons[4] = new RepairButton(Layer.AIR);

            buttons[8] = new BuildMenuButton();

            if (hp < max_hp && supply > 3){
                buttons[7] = new RecoverButton();
            }
        }
        else{
            makeBuildingMode(buttons);
        }

        return buttons;
    }

    @Override
    public Properties getProperties() {
        Properties p = super.getProperties();

        if (uname.isEmpty()){
            uname = game.getUnitNameGen().genName(name);
        }

        IUnitName.addNameProperty(uname, p);
        return p;
    }

    private String uname = "";

    @Override
    public String getUnitName() {
        return uname;
    }

    @Override
    public void setUnitName(String n) {
        uname = n;
    }

    @Override
    public Unit shiftSelectionOnRemoval() {
        if (hp > 0)
            return motbutton.getShift();
        else
            return null;
    }
}
