import edu.sc.seis.launch4j.tasks.Launch4jLibraryTask

plugins {
    id 'java'
    id 'edu.sc.seis.launch4j' version '2.5.3'
    id 'com.github.johnrengelman.shadow' version '7.1.2'
}

shadowJar{
    manifest {
        attributes 'Main-Class': 'net.stalemate.Main'
    }
    archiveBaseName.set('stalemate-universal')
    archiveClassifier.set('')
}

group 'net.stalemate'
version 'v0.4a'

tasks.register('prepareDirsRelease') {
    group("stalemate-release")
    mustRunAfter clean
    doFirst {
        mkdir "$buildDir/release"
    }
}
project.ext.lwjglVersion = "3.3.2"

task createServerExe(type: Launch4jLibraryTask) {
    group("stalemate-release")
    mustRunAfter prepareDirsRelease
    headerType "console"
    mainClassName "net.stalemate.Main"
    icon "${projectDir}/icons/stalemate_srv.ico"
    jreMinVersion "17"
    productName "Stalemate Server"
    copyright "GNU AGPLv3, CC-BY-SA"
    outfile "stalemate_server.exe"
    cmdLine "--serv"
}

task createClientExe(type: Launch4jLibraryTask) {
    group("stalemate-release")
    mustRunAfter prepareDirsRelease
    headerType "gui"
    icon "${projectDir}/icons/stalemate_icon.ico"
    jreMinVersion "17"
    mainClassName "net.stalemate.Main"
    productName "Stalemate"
    copyright "GNU AGPLv3, CC-BY-SA"
    outfile "stalemate_client.exe"
}

tasks.register('cpExes', Copy) {
    group("stalemate-release")
    mustRunAfter createServerExe
    mustRunAfter createClientExe
    from "$buildDir/launch4j"
    into "$buildDir/release"
}

tasks.register('prepareFiles', Copy) {
    group("stalemate-release")
    mustRunAfter cpExes
    from "."
    into "$buildDir/release"
    include "grass32"
    include "config/**"
    include "maps/**"
    include "font/**"
}

tasks.register('copyFatJar', Copy) {
    group("stalemate-release")
    mustRunAfter shadowJar
    mustRunAfter prepareDirsRelease
    from "$buildDir/libs"
    into "$buildDir/release"
}

tasks.register('clearJarsLibs') {
    group("stalemate-release")
    mustRunAfter copyFatJar
    delete fileTree("$buildDir/libs").include('**/*')
}

tasks.register('releaseBuild') {
    group("stalemate-release")
    dependsOn prepareDirsRelease
    dependsOn createClientExe
    dependsOn createServerExe
    dependsOn cpExes
    dependsOn prepareFiles
    dependsOn shadowJar
    dependsOn copyFatJar
    dependsOn clearJarsLibs
}

tasks.register('releaseBuildUniversalOnly') {
    group("stalemate-release")
    dependsOn prepareDirsRelease
    dependsOn prepareFiles
    dependsOn shadowJar
    dependsOn copyFatJar
    dependsOn clearJarsLibs
}

tasks.register('clearRelease') {
    group("stalemate-release")
    delete fileTree("$buildDir/release").include('**/*')
}

tasks.register('runClientDesktop', JavaExec) {
    dependsOn(build)
    group("run-stalemate")
    classpath sourceSets.main.runtimeClasspath
    mainClass = "net.stalemate.Main"
}

tasks.register('runClientDbgDesktop', JavaExec) {
    dependsOn(build)
    group("run-stalemate")
    classpath sourceSets.main.runtimeClasspath
    mainClass = "net.stalemate.Main"
    args("--dbg")
}

tasks.register('runServer', JavaExec) {
    dependsOn(build)
    group("run-stalemate")
    classpath sourceSets.main.runtimeClasspath
    mainClass = "net.stalemate.Main"
    args("--serv")
}

tasks.register('runSingleplayer', JavaExec) {
    dependsOn(build)
    group("run-stalemate")
    classpath sourceSets.main.runtimeClasspath
    mainClass = "net.stalemate.Main"
    args("--singleplayer")
}

tasks.register('runServerDbg', JavaExec) {
    dependsOn(build)
    group("run-stalemate")
    classpath sourceSets.main.runtimeClasspath
    mainClass = "net.stalemate.Main"
    args("--serv", "--dbg")
}

repositories {
    mavenCentral()

    maven {
        url 'https://gitlab.com/api/v4/projects/47870888/packages/maven'
    }
}

dependencies {

    implementation project(":stalemate-client-desktop")
}

test {
    useJUnitPlatform()
}