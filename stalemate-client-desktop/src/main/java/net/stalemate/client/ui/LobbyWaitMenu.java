/*
 * Stalemate Game
 * Copyright (C) 2022 Weltspear
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package net.stalemate.client.ui;

import net.stalemate.client.AssetManager;
import net.stgl.MainFrame;
import net.stgl.STGLGraphics;
import net.stgl.Utils;
import net.stgl.event.WindowResizeListener;
import net.stgl.font.FontManager;
import net.stgl.font.STGLFont;
import net.stgl.texture.Texture;
import net.stgl.ui.ComponentManager;
import net.stgl.ui.STGLButton;
import net.stgl.ui.STGLMultiList;
import org.joml.Vector4f;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.locks.ReentrantLock;

import static net.stalemate.client.Utils.setXY;

public class LobbyWaitMenu {
    private final MainFrame frame;
    private STGLMultiList multiList;
    private final int title_width_img;
    private final int title_height_img;
    private final Texture background;
    private final Texture title;

    private final ReentrantLock lock = new ReentrantLock();

    private int width;
    private int height;
    private final ComponentManager componentManager;
    private STGLButton disconnect;

    private int status = 0;

    private WindowResizeListener resizeListener = (nwidth, nheight) -> {
        width = nwidth;
        height = nheight;

        setXY(multiList, (width-400-50)/2, (height-300)/2);
        setXY(disconnect, multiList.getX()+375+25,multiList.getY());
    };

    public void dispose(){
        background.dispose();
        title.dispose();
    }

    public LobbyWaitMenu(MainFrame frame, ComponentManager componentManager, String srv_description){
        this.frame = frame;
        frame.addWindowResizeListener(resizeListener);

        width = frame.getWidth();
        height = frame.getHeight();
        this.componentManager = componentManager;

        FontManager fontManager = new FontManager();
        STGLFont monogram = fontManager.createFont("font/monogram-extended.ttf", 12, false, false);

        background = new Texture(AssetManager.loadImage("assets/background.png"));
        BufferedImage title_img = AssetManager.loadImage("assets/stalemate.png");
        title = new Texture(title_img.getWidth(), title_img.getHeight()+2, new Texture.TextureOptions().setMagFiltering(Texture.Filtering.NEAREST).setMinFiltering(Texture.Filtering.NEAREST));
        title.writeImage(0,0, title_img);
        title_width_img = title_img.getWidth();
        title_height_img = title_img.getHeight();


        multiList = new STGLMultiList(List.of(new STGLMultiList.DefaultColumn("Player", List.of(), 150)), (width-400-50)/2, (height-300)/2, 400, 300, monogram.getMaxHeight()+4, 30);
        multiList.setForeground(Utils.colorToVec(new Color(198, 130, 77)));
        multiList.setBackground(Utils.colorToVec(new Color(60, 38, 22)));
        multiList.setSelectedBackground(Utils.colorToVec(new Color(51, 39, 31)));
        multiList.setSelectedForeground(Utils.colorToVec(Color.BLACK));
        multiList.setFont(monogram);
        componentManager.addComponent(multiList);


        disconnect = new STGLButton(150, 25, multiList.getX()+375+25,multiList.getY(), "Disconnect");
        disconnect.setActionListener(() -> {status = 1;});
        StalemateStyle.makeButton(disconnect);
        disconnect.setFont(monogram);
        componentManager.addComponent(disconnect);

    }

    public void paint(STGLGraphics g) {
        lock.lock();
        g.drawTextureScaled(0, 0, background, new Vector4f(1, 1, 1, 1), width, height);
        componentManager.paint();

        if (title != null)
            g.drawTextureRegionScaled((width - 364) / 2, (int) (0.43f * height) - 194, title.getSubTexture(0, 0, title_width_img, title_height_img), new Vector4f(1, 1, 1, 1), 364, 64);
        lock.unlock();
    }

    public void setNicks(ArrayList<String> unwrap) {
        lock.lock();
        multiList.setColumns(List.of(new STGLMultiList.DefaultColumn("Player", unwrap, 150)));
        lock.unlock();
    }

    public int getStatus() {
        return status;
    }

    public void clFrame() {
        componentManager.removeComponent(multiList);
        componentManager.removeComponent(disconnect);
        frame.removeWindowResizeListener(resizeListener);
    }

    public void setStatus(int s) {
        try {
            lock.lock();
            status = s;
        }
        finally {
            lock.unlock();
        }
    }
}
